<?php

/* FOSUserBundle:ChangePassword:change_password.html.twig */
class __TwigTemplate_126071e8eb4adb587cae0210c07d4420db5f52a8037dcd76ed8eda03ed7fa307 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:ChangePassword:change_password.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9825bb3786c28ce003208de92caa1df97aa2c0d2f673ac2b66291c6bbdad93f8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9825bb3786c28ce003208de92caa1df97aa2c0d2f673ac2b66291c6bbdad93f8->enter($__internal_9825bb3786c28ce003208de92caa1df97aa2c0d2f673ac2b66291c6bbdad93f8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:ChangePassword:change_password.html.twig"));

        $__internal_2bd9abdcbc96084d540a85e0b30d3852d9f62030c8dbac136bee31d3ea016038 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2bd9abdcbc96084d540a85e0b30d3852d9f62030c8dbac136bee31d3ea016038->enter($__internal_2bd9abdcbc96084d540a85e0b30d3852d9f62030c8dbac136bee31d3ea016038_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:ChangePassword:change_password.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9825bb3786c28ce003208de92caa1df97aa2c0d2f673ac2b66291c6bbdad93f8->leave($__internal_9825bb3786c28ce003208de92caa1df97aa2c0d2f673ac2b66291c6bbdad93f8_prof);

        
        $__internal_2bd9abdcbc96084d540a85e0b30d3852d9f62030c8dbac136bee31d3ea016038->leave($__internal_2bd9abdcbc96084d540a85e0b30d3852d9f62030c8dbac136bee31d3ea016038_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_055f7e2dfe2fe51f39a052729416745378cef0fe917761ad39331c06aaf56173 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_055f7e2dfe2fe51f39a052729416745378cef0fe917761ad39331c06aaf56173->enter($__internal_055f7e2dfe2fe51f39a052729416745378cef0fe917761ad39331c06aaf56173_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_47e4fde0cd4ee4f57785d60ce596c69cf2f82240dfcbdd0380b0bf8177e1cc25 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_47e4fde0cd4ee4f57785d60ce596c69cf2f82240dfcbdd0380b0bf8177e1cc25->enter($__internal_47e4fde0cd4ee4f57785d60ce596c69cf2f82240dfcbdd0380b0bf8177e1cc25_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/ChangePassword/change_password_content.html.twig", "FOSUserBundle:ChangePassword:change_password.html.twig", 4)->display($context);
        
        $__internal_47e4fde0cd4ee4f57785d60ce596c69cf2f82240dfcbdd0380b0bf8177e1cc25->leave($__internal_47e4fde0cd4ee4f57785d60ce596c69cf2f82240dfcbdd0380b0bf8177e1cc25_prof);

        
        $__internal_055f7e2dfe2fe51f39a052729416745378cef0fe917761ad39331c06aaf56173->leave($__internal_055f7e2dfe2fe51f39a052729416745378cef0fe917761ad39331c06aaf56173_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:ChangePassword:change_password.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/ChangePassword/change_password_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:ChangePassword:change_password.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\app/Resources/FOSUserBundle/views/ChangePassword/change_password.html.twig");
    }
}
