<?php

/* @FOSUser/Group/show.html.twig */
class __TwigTemplate_6be926763d39c2573da78bd0bc989da38356f9b6b8b4757d98fa980bbe3820b7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Group/show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3fc71965742ea8e5d080dc6a240160b6b93715ae5b9c2c4776392a08d85db13e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3fc71965742ea8e5d080dc6a240160b6b93715ae5b9c2c4776392a08d85db13e->enter($__internal_3fc71965742ea8e5d080dc6a240160b6b93715ae5b9c2c4776392a08d85db13e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Group/show.html.twig"));

        $__internal_8c7bda3618bbb3b410784b3100e05178ca04da6f918436d59b7f444055319f91 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8c7bda3618bbb3b410784b3100e05178ca04da6f918436d59b7f444055319f91->enter($__internal_8c7bda3618bbb3b410784b3100e05178ca04da6f918436d59b7f444055319f91_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Group/show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3fc71965742ea8e5d080dc6a240160b6b93715ae5b9c2c4776392a08d85db13e->leave($__internal_3fc71965742ea8e5d080dc6a240160b6b93715ae5b9c2c4776392a08d85db13e_prof);

        
        $__internal_8c7bda3618bbb3b410784b3100e05178ca04da6f918436d59b7f444055319f91->leave($__internal_8c7bda3618bbb3b410784b3100e05178ca04da6f918436d59b7f444055319f91_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_e0a7210e171a2b886bf94745660d85d596df3cb3bc78731df38b8956f7aad87c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e0a7210e171a2b886bf94745660d85d596df3cb3bc78731df38b8956f7aad87c->enter($__internal_e0a7210e171a2b886bf94745660d85d596df3cb3bc78731df38b8956f7aad87c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_b3c16d64b229d7b77f939cc31c6efc87b14a48b0685e1b421734b1d19f65d2f0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b3c16d64b229d7b77f939cc31c6efc87b14a48b0685e1b421734b1d19f65d2f0->enter($__internal_b3c16d64b229d7b77f939cc31c6efc87b14a48b0685e1b421734b1d19f65d2f0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/show_content.html.twig", "@FOSUser/Group/show.html.twig", 4)->display($context);
        
        $__internal_b3c16d64b229d7b77f939cc31c6efc87b14a48b0685e1b421734b1d19f65d2f0->leave($__internal_b3c16d64b229d7b77f939cc31c6efc87b14a48b0685e1b421734b1d19f65d2f0_prof);

        
        $__internal_e0a7210e171a2b886bf94745660d85d596df3cb3bc78731df38b8956f7aad87c->leave($__internal_e0a7210e171a2b886bf94745660d85d596df3cb3bc78731df38b8956f7aad87c_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Group/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/show_content.html.twig\" %}
{% endblock fos_user_content %}
", "@FOSUser/Group/show.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\app\\Resources\\FOSUserBundle\\views\\Group\\show.html.twig");
    }
}
