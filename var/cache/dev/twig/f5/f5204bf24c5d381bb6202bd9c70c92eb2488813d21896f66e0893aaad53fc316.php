<?php

/* @Framework/Form/password_widget.html.php */
class __TwigTemplate_34511abbf5bb8676f8ba8b0b98e2fa3507e90d4c46868d8967d90403c8bdfc0e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d625ec03c0ee553bbad765caf1ee4c70ea1bbfbae3842ea8d1cd4cfbce3c0d23 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d625ec03c0ee553bbad765caf1ee4c70ea1bbfbae3842ea8d1cd4cfbce3c0d23->enter($__internal_d625ec03c0ee553bbad765caf1ee4c70ea1bbfbae3842ea8d1cd4cfbce3c0d23_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        $__internal_744b899ad644b8228ec42a6d417d121a3d8b5e91e72a5160b3ccd948e3788c3c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_744b899ad644b8228ec42a6d417d121a3d8b5e91e72a5160b3ccd948e3788c3c->enter($__internal_744b899ad644b8228ec42a6d417d121a3d8b5e91e72a5160b3ccd948e3788c3c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'password')) ?>
";
        
        $__internal_d625ec03c0ee553bbad765caf1ee4c70ea1bbfbae3842ea8d1cd4cfbce3c0d23->leave($__internal_d625ec03c0ee553bbad765caf1ee4c70ea1bbfbae3842ea8d1cd4cfbce3c0d23_prof);

        
        $__internal_744b899ad644b8228ec42a6d417d121a3d8b5e91e72a5160b3ccd948e3788c3c->leave($__internal_744b899ad644b8228ec42a6d417d121a3d8b5e91e72a5160b3ccd948e3788c3c_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/password_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'password')) ?>
", "@Framework/Form/password_widget.html.php", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\password_widget.html.php");
    }
}
