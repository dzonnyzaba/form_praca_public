<?php

/* @FOSUser/Resetting/check_email.html.twig */
class __TwigTemplate_74bb0f4a81e996c5da683467d74cb1861a7079244961a426ee8851ebe0654cbd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Resetting/check_email.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2dc75efbb0e910b814439459ac50b3a41b8b4545759e72b041a8a0f0c04539a8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2dc75efbb0e910b814439459ac50b3a41b8b4545759e72b041a8a0f0c04539a8->enter($__internal_2dc75efbb0e910b814439459ac50b3a41b8b4545759e72b041a8a0f0c04539a8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Resetting/check_email.html.twig"));

        $__internal_b4919a36227ff430737231029a0edd230c5d05e0ec4d52717b4bf8bfdb1a2a6e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b4919a36227ff430737231029a0edd230c5d05e0ec4d52717b4bf8bfdb1a2a6e->enter($__internal_b4919a36227ff430737231029a0edd230c5d05e0ec4d52717b4bf8bfdb1a2a6e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Resetting/check_email.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2dc75efbb0e910b814439459ac50b3a41b8b4545759e72b041a8a0f0c04539a8->leave($__internal_2dc75efbb0e910b814439459ac50b3a41b8b4545759e72b041a8a0f0c04539a8_prof);

        
        $__internal_b4919a36227ff430737231029a0edd230c5d05e0ec4d52717b4bf8bfdb1a2a6e->leave($__internal_b4919a36227ff430737231029a0edd230c5d05e0ec4d52717b4bf8bfdb1a2a6e_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_194e827865608da901337d8c7a5fd9f2a357e90436bcbcd4772995f00036e760 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_194e827865608da901337d8c7a5fd9f2a357e90436bcbcd4772995f00036e760->enter($__internal_194e827865608da901337d8c7a5fd9f2a357e90436bcbcd4772995f00036e760_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_138493f9b4c16ea65de07cd12587c845f2b5256d75621cffe8c4d93881774aea = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_138493f9b4c16ea65de07cd12587c845f2b5256d75621cffe8c4d93881774aea->enter($__internal_138493f9b4c16ea65de07cd12587c845f2b5256d75621cffe8c4d93881774aea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "<p>
";
        // line 7
        echo nl2br(twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.check_email", array("%tokenLifetime%" => ($context["tokenLifetime"] ?? $this->getContext($context, "tokenLifetime"))), "FOSUserBundle"), "html", null, true));
        echo "
</p>
";
        
        $__internal_138493f9b4c16ea65de07cd12587c845f2b5256d75621cffe8c4d93881774aea->leave($__internal_138493f9b4c16ea65de07cd12587c845f2b5256d75621cffe8c4d93881774aea_prof);

        
        $__internal_194e827865608da901337d8c7a5fd9f2a357e90436bcbcd4772995f00036e760->leave($__internal_194e827865608da901337d8c7a5fd9f2a357e90436bcbcd4772995f00036e760_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Resetting/check_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 7,  49 => 6,  40 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% trans_default_domain 'FOSUserBundle' %}

{% block fos_user_content %}
<p>
{{ 'resetting.check_email'|trans({'%tokenLifetime%': tokenLifetime})|nl2br }}
</p>
{% endblock %}
", "@FOSUser/Resetting/check_email.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\app\\Resources\\FOSUserBundle\\views\\Resetting\\check_email.html.twig");
    }
}
