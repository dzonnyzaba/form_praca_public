<?php

/* FOSUserBundle:Registration:confirmed.html.twig */
class __TwigTemplate_72e37d53b3c0ea5b2e0a9828a1eb65dd6176e644353de866bdc311f1ec0027b2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Registration:confirmed.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b4146b057f3dc1c8517bc4843fbaea7f3f0e165e637ea58aa21f47ec61802aec = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b4146b057f3dc1c8517bc4843fbaea7f3f0e165e637ea58aa21f47ec61802aec->enter($__internal_b4146b057f3dc1c8517bc4843fbaea7f3f0e165e637ea58aa21f47ec61802aec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:confirmed.html.twig"));

        $__internal_f5e7f62851058dbfd5e958ecb16e75b7cb0b23f708bf1e44b7405ae5190befd0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f5e7f62851058dbfd5e958ecb16e75b7cb0b23f708bf1e44b7405ae5190befd0->enter($__internal_f5e7f62851058dbfd5e958ecb16e75b7cb0b23f708bf1e44b7405ae5190befd0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:confirmed.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b4146b057f3dc1c8517bc4843fbaea7f3f0e165e637ea58aa21f47ec61802aec->leave($__internal_b4146b057f3dc1c8517bc4843fbaea7f3f0e165e637ea58aa21f47ec61802aec_prof);

        
        $__internal_f5e7f62851058dbfd5e958ecb16e75b7cb0b23f708bf1e44b7405ae5190befd0->leave($__internal_f5e7f62851058dbfd5e958ecb16e75b7cb0b23f708bf1e44b7405ae5190befd0_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_84ea73dd158741ea15d56f721a78fd2c574e9a54e512c9b02e74e7c654eb29da = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_84ea73dd158741ea15d56f721a78fd2c574e9a54e512c9b02e74e7c654eb29da->enter($__internal_84ea73dd158741ea15d56f721a78fd2c574e9a54e512c9b02e74e7c654eb29da_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_1b7cf69653798049d77101517fa3e57ac82c370f47b5b20727843768f5ea1024 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1b7cf69653798049d77101517fa3e57ac82c370f47b5b20727843768f5ea1024->enter($__internal_1b7cf69653798049d77101517fa3e57ac82c370f47b5b20727843768f5ea1024_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "    <p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.confirmed", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
    ";
        // line 7
        if (($context["targetUrl"] ?? $this->getContext($context, "targetUrl"))) {
            // line 8
            echo "    <p><a href=\"";
            echo twig_escape_filter($this->env, ($context["targetUrl"] ?? $this->getContext($context, "targetUrl")), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.back", array(), "FOSUserBundle"), "html", null, true);
            echo "</a></p>
    ";
        }
        
        $__internal_1b7cf69653798049d77101517fa3e57ac82c370f47b5b20727843768f5ea1024->leave($__internal_1b7cf69653798049d77101517fa3e57ac82c370f47b5b20727843768f5ea1024_prof);

        
        $__internal_84ea73dd158741ea15d56f721a78fd2c574e9a54e512c9b02e74e7c654eb29da->leave($__internal_84ea73dd158741ea15d56f721a78fd2c574e9a54e512c9b02e74e7c654eb29da_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:confirmed.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  56 => 8,  54 => 7,  49 => 6,  40 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% trans_default_domain 'FOSUserBundle' %}

{% block fos_user_content %}
    <p>{{ 'registration.confirmed'|trans({'%username%': user.username}) }}</p>
    {% if targetUrl %}
    <p><a href=\"{{ targetUrl }}\">{{ 'registration.back'|trans }}</a></p>
    {% endif %}
{% endblock fos_user_content %}
", "FOSUserBundle:Registration:confirmed.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\app/Resources/FOSUserBundle/views/Registration/confirmed.html.twig");
    }
}
