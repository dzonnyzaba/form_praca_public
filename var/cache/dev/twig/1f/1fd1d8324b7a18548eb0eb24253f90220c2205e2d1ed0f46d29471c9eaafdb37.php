<?php

/* @Framework/Form/form_end.html.php */
class __TwigTemplate_c8c02e2e7c4c1be191f60bfcf969a441b80a59321483a91ca09e2dd35689d1c2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e49516933ecdae1a42eaaa0e937a83204bd75897bdfcd92f8dbf542849a3a965 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e49516933ecdae1a42eaaa0e937a83204bd75897bdfcd92f8dbf542849a3a965->enter($__internal_e49516933ecdae1a42eaaa0e937a83204bd75897bdfcd92f8dbf542849a3a965_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        $__internal_c8d37592d46b74f4c4e19a1f6f69e435babd41f9dd44b628f03f899f135e1a17 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c8d37592d46b74f4c4e19a1f6f69e435babd41f9dd44b628f03f899f135e1a17->enter($__internal_c8d37592d46b74f4c4e19a1f6f69e435babd41f9dd44b628f03f899f135e1a17_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        // line 1
        echo "<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
";
        
        $__internal_e49516933ecdae1a42eaaa0e937a83204bd75897bdfcd92f8dbf542849a3a965->leave($__internal_e49516933ecdae1a42eaaa0e937a83204bd75897bdfcd92f8dbf542849a3a965_prof);

        
        $__internal_c8d37592d46b74f4c4e19a1f6f69e435babd41f9dd44b628f03f899f135e1a17->leave($__internal_c8d37592d46b74f4c4e19a1f6f69e435babd41f9dd44b628f03f899f135e1a17_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_end.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
", "@Framework/Form/form_end.html.php", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\form_end.html.php");
    }
}
