<?php

/* @Framework/Form/repeated_row.html.php */
class __TwigTemplate_1e8fb82b3f2bcde7dd2b6e9f6e2fb7a746007be899b0b9ab536a0fd3d25354d0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_581871375c9ce7a34bcd37d5736a8559462635962b93b95fc929200d9e7ccf00 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_581871375c9ce7a34bcd37d5736a8559462635962b93b95fc929200d9e7ccf00->enter($__internal_581871375c9ce7a34bcd37d5736a8559462635962b93b95fc929200d9e7ccf00_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/repeated_row.html.php"));

        $__internal_dab23f6ecc58f8c8394ab560f3e62639102adcb30567974a29a16ff912d19768 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dab23f6ecc58f8c8394ab560f3e62639102adcb30567974a29a16ff912d19768->enter($__internal_dab23f6ecc58f8c8394ab560f3e62639102adcb30567974a29a16ff912d19768_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/repeated_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_rows') ?>
";
        
        $__internal_581871375c9ce7a34bcd37d5736a8559462635962b93b95fc929200d9e7ccf00->leave($__internal_581871375c9ce7a34bcd37d5736a8559462635962b93b95fc929200d9e7ccf00_prof);

        
        $__internal_dab23f6ecc58f8c8394ab560f3e62639102adcb30567974a29a16ff912d19768->leave($__internal_dab23f6ecc58f8c8394ab560f3e62639102adcb30567974a29a16ff912d19768_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/repeated_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_rows') ?>
", "@Framework/Form/repeated_row.html.php", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\repeated_row.html.php");
    }
}
