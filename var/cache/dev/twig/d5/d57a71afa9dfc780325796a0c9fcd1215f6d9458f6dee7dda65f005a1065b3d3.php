<?php

/* FOSUserBundle:Profile:show_content.html.twig */
class __TwigTemplate_2fa20bc883573c5f722ae68cf37e51279c633cedb01a472e85edaec68a9913fb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0c72e5ca4d3ae2ac94f8e9f6a9ee44562d81624b5758d7fc81ad35461d7edde6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0c72e5ca4d3ae2ac94f8e9f6a9ee44562d81624b5758d7fc81ad35461d7edde6->enter($__internal_0c72e5ca4d3ae2ac94f8e9f6a9ee44562d81624b5758d7fc81ad35461d7edde6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:show_content.html.twig"));

        $__internal_17a42eaf29d09b1ec56ebe5f590115d88dddd5130fac13978c8a69388e98884a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_17a42eaf29d09b1ec56ebe5f590115d88dddd5130fac13978c8a69388e98884a->enter($__internal_17a42eaf29d09b1ec56ebe5f590115d88dddd5130fac13978c8a69388e98884a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:show_content.html.twig"));

        // line 2
        echo "
<div class=\"fos_user_user_show\">
    <p>";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("profile.show.username", array(), "FOSUserBundle"), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array()), "html", null, true);
        echo "</p>
    <p>";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("profile.show.email", array(), "FOSUserBundle"), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "email", array()), "html", null, true);
        echo "</p>
</div>
";
        
        $__internal_0c72e5ca4d3ae2ac94f8e9f6a9ee44562d81624b5758d7fc81ad35461d7edde6->leave($__internal_0c72e5ca4d3ae2ac94f8e9f6a9ee44562d81624b5758d7fc81ad35461d7edde6_prof);

        
        $__internal_17a42eaf29d09b1ec56ebe5f590115d88dddd5130fac13978c8a69388e98884a->leave($__internal_17a42eaf29d09b1ec56ebe5f590115d88dddd5130fac13978c8a69388e98884a_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:show_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  35 => 5,  29 => 4,  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}

<div class=\"fos_user_user_show\">
    <p>{{ 'profile.show.username'|trans }}: {{ user.username }}</p>
    <p>{{ 'profile.show.email'|trans }}: {{ user.email }}</p>
</div>
", "FOSUserBundle:Profile:show_content.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\app/Resources/FOSUserBundle/views/Profile/show_content.html.twig");
    }
}
