<?php

/* @FOSUser/Profile/show_content.html.twig */
class __TwigTemplate_40e33a13ef484892b7a77123c0210d79c254d9862d4bb9dfed7d400045ab3e13 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_047171081070e3c20c899ac85ddcc01a7b62ad75e0932ca3e9bb39c15cf4fd8c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_047171081070e3c20c899ac85ddcc01a7b62ad75e0932ca3e9bb39c15cf4fd8c->enter($__internal_047171081070e3c20c899ac85ddcc01a7b62ad75e0932ca3e9bb39c15cf4fd8c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Profile/show_content.html.twig"));

        $__internal_aab88805b02608b0a0651a789dfcd9f1a0145fd98171be58851b789594e389f2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_aab88805b02608b0a0651a789dfcd9f1a0145fd98171be58851b789594e389f2->enter($__internal_aab88805b02608b0a0651a789dfcd9f1a0145fd98171be58851b789594e389f2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Profile/show_content.html.twig"));

        // line 2
        echo "
<div class=\"fos_user_user_show\">
    <p>";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("profile.show.username", array(), "FOSUserBundle"), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array()), "html", null, true);
        echo "</p>
    <p>";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("profile.show.email", array(), "FOSUserBundle"), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "email", array()), "html", null, true);
        echo "</p>
</div>
";
        
        $__internal_047171081070e3c20c899ac85ddcc01a7b62ad75e0932ca3e9bb39c15cf4fd8c->leave($__internal_047171081070e3c20c899ac85ddcc01a7b62ad75e0932ca3e9bb39c15cf4fd8c_prof);

        
        $__internal_aab88805b02608b0a0651a789dfcd9f1a0145fd98171be58851b789594e389f2->leave($__internal_aab88805b02608b0a0651a789dfcd9f1a0145fd98171be58851b789594e389f2_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Profile/show_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  35 => 5,  29 => 4,  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}

<div class=\"fos_user_user_show\">
    <p>{{ 'profile.show.username'|trans }}: {{ user.username }}</p>
    <p>{{ 'profile.show.email'|trans }}: {{ user.email }}</p>
</div>
", "@FOSUser/Profile/show_content.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\app\\Resources\\FOSUserBundle\\views\\Profile\\show_content.html.twig");
    }
}
