<?php

/* FOSUserBundle:Resetting:check_email.html.twig */
class __TwigTemplate_70e010c7b4ff4403cdfda0252f71551df2105b48e4acff64d4925836fbe43417 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Resetting:check_email.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b58801256c69fdabdcd779e15b1d3e730567daeaed25cde941ba5da29901301d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b58801256c69fdabdcd779e15b1d3e730567daeaed25cde941ba5da29901301d->enter($__internal_b58801256c69fdabdcd779e15b1d3e730567daeaed25cde941ba5da29901301d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:check_email.html.twig"));

        $__internal_90d444505a75edcb536b00a5d78b769ecf69f35ca6dd65912c691155e33539d4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_90d444505a75edcb536b00a5d78b769ecf69f35ca6dd65912c691155e33539d4->enter($__internal_90d444505a75edcb536b00a5d78b769ecf69f35ca6dd65912c691155e33539d4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:check_email.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b58801256c69fdabdcd779e15b1d3e730567daeaed25cde941ba5da29901301d->leave($__internal_b58801256c69fdabdcd779e15b1d3e730567daeaed25cde941ba5da29901301d_prof);

        
        $__internal_90d444505a75edcb536b00a5d78b769ecf69f35ca6dd65912c691155e33539d4->leave($__internal_90d444505a75edcb536b00a5d78b769ecf69f35ca6dd65912c691155e33539d4_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_db1c3f63f88d65ae7506a2730b19f705d0e7e3cbbf728c1f7b2d1241ac526f16 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_db1c3f63f88d65ae7506a2730b19f705d0e7e3cbbf728c1f7b2d1241ac526f16->enter($__internal_db1c3f63f88d65ae7506a2730b19f705d0e7e3cbbf728c1f7b2d1241ac526f16_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_c91ac345311549fe04a0d2fb543146c78333dbc309048c3aadd003213ce6d853 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c91ac345311549fe04a0d2fb543146c78333dbc309048c3aadd003213ce6d853->enter($__internal_c91ac345311549fe04a0d2fb543146c78333dbc309048c3aadd003213ce6d853_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "<p>
";
        // line 7
        echo nl2br(twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.check_email", array("%tokenLifetime%" => ($context["tokenLifetime"] ?? $this->getContext($context, "tokenLifetime"))), "FOSUserBundle"), "html", null, true));
        echo "
</p>
";
        
        $__internal_c91ac345311549fe04a0d2fb543146c78333dbc309048c3aadd003213ce6d853->leave($__internal_c91ac345311549fe04a0d2fb543146c78333dbc309048c3aadd003213ce6d853_prof);

        
        $__internal_db1c3f63f88d65ae7506a2730b19f705d0e7e3cbbf728c1f7b2d1241ac526f16->leave($__internal_db1c3f63f88d65ae7506a2730b19f705d0e7e3cbbf728c1f7b2d1241ac526f16_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:check_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 7,  49 => 6,  40 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% trans_default_domain 'FOSUserBundle' %}

{% block fos_user_content %}
<p>
{{ 'resetting.check_email'|trans({'%tokenLifetime%': tokenLifetime})|nl2br }}
</p>
{% endblock %}
", "FOSUserBundle:Resetting:check_email.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\app/Resources/FOSUserBundle/views/Resetting/check_email.html.twig");
    }
}
