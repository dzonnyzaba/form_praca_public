<?php

/* WebProfilerBundle:Profiler:open.html.twig */
class __TwigTemplate_1406c706aafb1d29637c02dc07b8308c8d1175e94ef470af301598ed8fc617a3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/base.html.twig", "WebProfilerBundle:Profiler:open.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_aa10bae2e837c7b9d83e1ac06ce4c6194ed3bf7de7bb88b2400e830b64908dd2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_aa10bae2e837c7b9d83e1ac06ce4c6194ed3bf7de7bb88b2400e830b64908dd2->enter($__internal_aa10bae2e837c7b9d83e1ac06ce4c6194ed3bf7de7bb88b2400e830b64908dd2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:open.html.twig"));

        $__internal_238ed04d35ddb7b33ecf4b8499c1021131c51f91a82dee476762efa9d6305452 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_238ed04d35ddb7b33ecf4b8499c1021131c51f91a82dee476762efa9d6305452->enter($__internal_238ed04d35ddb7b33ecf4b8499c1021131c51f91a82dee476762efa9d6305452_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:open.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_aa10bae2e837c7b9d83e1ac06ce4c6194ed3bf7de7bb88b2400e830b64908dd2->leave($__internal_aa10bae2e837c7b9d83e1ac06ce4c6194ed3bf7de7bb88b2400e830b64908dd2_prof);

        
        $__internal_238ed04d35ddb7b33ecf4b8499c1021131c51f91a82dee476762efa9d6305452->leave($__internal_238ed04d35ddb7b33ecf4b8499c1021131c51f91a82dee476762efa9d6305452_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_81b8112f774572c29c0fe5ac5d2cd6a48cd035dfadcef86de5f146b778a375b5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_81b8112f774572c29c0fe5ac5d2cd6a48cd035dfadcef86de5f146b778a375b5->enter($__internal_81b8112f774572c29c0fe5ac5d2cd6a48cd035dfadcef86de5f146b778a375b5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_449e0048aab52fd0a2a83afa077e27ee4324a816ff5875545741a69aeffe6504 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_449e0048aab52fd0a2a83afa077e27ee4324a816ff5875545741a69aeffe6504->enter($__internal_449e0048aab52fd0a2a83afa077e27ee4324a816ff5875545741a69aeffe6504_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        ";
        // line 5
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/open.css.twig");
        echo "
    </style>
";
        
        $__internal_449e0048aab52fd0a2a83afa077e27ee4324a816ff5875545741a69aeffe6504->leave($__internal_449e0048aab52fd0a2a83afa077e27ee4324a816ff5875545741a69aeffe6504_prof);

        
        $__internal_81b8112f774572c29c0fe5ac5d2cd6a48cd035dfadcef86de5f146b778a375b5->leave($__internal_81b8112f774572c29c0fe5ac5d2cd6a48cd035dfadcef86de5f146b778a375b5_prof);

    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        $__internal_0e6ab5a5a2eb6b2356f0120cb284afd0b51629151fa25ae3438eb9cc31b21374 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0e6ab5a5a2eb6b2356f0120cb284afd0b51629151fa25ae3438eb9cc31b21374->enter($__internal_0e6ab5a5a2eb6b2356f0120cb284afd0b51629151fa25ae3438eb9cc31b21374_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_8d270efce249f93c486664fae73083f9bbb1a654df53da5dfa935b7ff4288a62 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8d270efce249f93c486664fae73083f9bbb1a654df53da5dfa935b7ff4288a62->enter($__internal_8d270efce249f93c486664fae73083f9bbb1a654df53da5dfa935b7ff4288a62_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "<div class=\"header\">
    <h1>";
        // line 11
        echo twig_escape_filter($this->env, ($context["file"] ?? $this->getContext($context, "file")), "html", null, true);
        echo " <small>line ";
        echo twig_escape_filter($this->env, ($context["line"] ?? $this->getContext($context, "line")), "html", null, true);
        echo "</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/";
        // line 12
        echo twig_escape_filter($this->env, twig_constant("Symfony\\Component\\HttpKernel\\Kernel::VERSION"), "html", null, true);
        echo "/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    ";
        // line 15
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->fileExcerpt(($context["filename"] ?? $this->getContext($context, "filename")), ($context["line"] ?? $this->getContext($context, "line")),  -1);
        echo "
</div>
";
        
        $__internal_8d270efce249f93c486664fae73083f9bbb1a654df53da5dfa935b7ff4288a62->leave($__internal_8d270efce249f93c486664fae73083f9bbb1a654df53da5dfa935b7ff4288a62_prof);

        
        $__internal_0e6ab5a5a2eb6b2356f0120cb284afd0b51629151fa25ae3438eb9cc31b21374->leave($__internal_0e6ab5a5a2eb6b2356f0120cb284afd0b51629151fa25ae3438eb9cc31b21374_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:open.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 15,  84 => 12,  78 => 11,  75 => 10,  66 => 9,  53 => 5,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/base.html.twig' %}

{% block head %}
    <style>
        {{ include('@WebProfiler/Profiler/open.css.twig') }}
    </style>
{% endblock %}

{% block body %}
<div class=\"header\">
    <h1>{{ file }} <small>line {{ line }}</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/{{ constant('Symfony\\\\Component\\\\HttpKernel\\\\Kernel::VERSION') }}/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    {{ filename|file_excerpt(line, -1) }}
</div>
{% endblock %}
", "WebProfilerBundle:Profiler:open.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle/Resources/views/Profiler/open.html.twig");
    }
}
