<?php

/* @FOSUser/Registration/email.txt.twig */
class __TwigTemplate_44105c05aef898e5c71d5de114cd08919e427223e3e8789a0835dafc1907cb7d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0f37dfe2f5efe4fb11af40cf39b7ee8902398df16170a187b2787ab33104eb4b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0f37dfe2f5efe4fb11af40cf39b7ee8902398df16170a187b2787ab33104eb4b->enter($__internal_0f37dfe2f5efe4fb11af40cf39b7ee8902398df16170a187b2787ab33104eb4b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/email.txt.twig"));

        $__internal_7412e2464e8bb2330ea73b354e537bdbed9af4c768c208947b99f9412e5247bd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7412e2464e8bb2330ea73b354e537bdbed9af4c768c208947b99f9412e5247bd->enter($__internal_7412e2464e8bb2330ea73b354e537bdbed9af4c768c208947b99f9412e5247bd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        echo "
";
        // line 8
        $this->displayBlock('body_text', $context, $blocks);
        // line 13
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_0f37dfe2f5efe4fb11af40cf39b7ee8902398df16170a187b2787ab33104eb4b->leave($__internal_0f37dfe2f5efe4fb11af40cf39b7ee8902398df16170a187b2787ab33104eb4b_prof);

        
        $__internal_7412e2464e8bb2330ea73b354e537bdbed9af4c768c208947b99f9412e5247bd->leave($__internal_7412e2464e8bb2330ea73b354e537bdbed9af4c768c208947b99f9412e5247bd_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_043fe80c8d8d9f103524a759217204e66da5e3b226f471323242f2398a422982 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_043fe80c8d8d9f103524a759217204e66da5e3b226f471323242f2398a422982->enter($__internal_043fe80c8d8d9f103524a759217204e66da5e3b226f471323242f2398a422982_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        $__internal_112080e3807e1f74185d23f5f358a2c3a79fa7bc1a4efc370a3f9b6a0b3ab6c8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_112080e3807e1f74185d23f5f358a2c3a79fa7bc1a4efc370a3f9b6a0b3ab6c8->enter($__internal_112080e3807e1f74185d23f5f358a2c3a79fa7bc1a4efc370a3f9b6a0b3ab6c8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.email.subject", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => ($context["confirmationUrl"] ?? $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        
        $__internal_112080e3807e1f74185d23f5f358a2c3a79fa7bc1a4efc370a3f9b6a0b3ab6c8->leave($__internal_112080e3807e1f74185d23f5f358a2c3a79fa7bc1a4efc370a3f9b6a0b3ab6c8_prof);

        
        $__internal_043fe80c8d8d9f103524a759217204e66da5e3b226f471323242f2398a422982->leave($__internal_043fe80c8d8d9f103524a759217204e66da5e3b226f471323242f2398a422982_prof);

    }

    // line 8
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_bba796780c786bf8a1c8214c4f5f62ced90f0c95b984f6d72e8ab748c71ecdc9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bba796780c786bf8a1c8214c4f5f62ced90f0c95b984f6d72e8ab748c71ecdc9->enter($__internal_bba796780c786bf8a1c8214c4f5f62ced90f0c95b984f6d72e8ab748c71ecdc9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        $__internal_fcb23ee1888447e0f2ba55d2cc128f64e33d86975e7d6a504e21e8539bd059f3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fcb23ee1888447e0f2ba55d2cc128f64e33d86975e7d6a504e21e8539bd059f3->enter($__internal_fcb23ee1888447e0f2ba55d2cc128f64e33d86975e7d6a504e21e8539bd059f3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.email.message", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => ($context["confirmationUrl"] ?? $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        echo "
";
        
        $__internal_fcb23ee1888447e0f2ba55d2cc128f64e33d86975e7d6a504e21e8539bd059f3->leave($__internal_fcb23ee1888447e0f2ba55d2cc128f64e33d86975e7d6a504e21e8539bd059f3_prof);

        
        $__internal_bba796780c786bf8a1c8214c4f5f62ced90f0c95b984f6d72e8ab748c71ecdc9->leave($__internal_bba796780c786bf8a1c8214c4f5f62ced90f0c95b984f6d72e8ab748c71ecdc9_prof);

    }

    // line 13
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_e3afb9d124d1675e7f23d4229367601c27fac50e281a471d44fc24a6926c9c87 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e3afb9d124d1675e7f23d4229367601c27fac50e281a471d44fc24a6926c9c87->enter($__internal_e3afb9d124d1675e7f23d4229367601c27fac50e281a471d44fc24a6926c9c87_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        $__internal_c2f466502b5b8f1406623c9c93376e4ee0d6a4a6a31ae66f91f7af25ff95507b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c2f466502b5b8f1406623c9c93376e4ee0d6a4a6a31ae66f91f7af25ff95507b->enter($__internal_c2f466502b5b8f1406623c9c93376e4ee0d6a4a6a31ae66f91f7af25ff95507b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_c2f466502b5b8f1406623c9c93376e4ee0d6a4a6a31ae66f91f7af25ff95507b->leave($__internal_c2f466502b5b8f1406623c9c93376e4ee0d6a4a6a31ae66f91f7af25ff95507b_prof);

        
        $__internal_e3afb9d124d1675e7f23d4229367601c27fac50e281a471d44fc24a6926c9c87->leave($__internal_e3afb9d124d1675e7f23d4229367601c27fac50e281a471d44fc24a6926c9c87_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Registration/email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  85 => 13,  73 => 10,  64 => 8,  54 => 4,  45 => 2,  35 => 13,  33 => 8,  30 => 7,  28 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}
{% block subject %}
{%- autoescape false -%}
{{ 'registration.email.subject'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{%- endautoescape -%}
{% endblock %}

{% block body_text %}
{% autoescape false %}
{{ 'registration.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{% endautoescape %}
{% endblock %}
{% block body_html %}{% endblock %}
", "@FOSUser/Registration/email.txt.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\app\\Resources\\FOSUserBundle\\views\\Registration\\email.txt.twig");
    }
}
