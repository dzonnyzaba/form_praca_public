<?php

/* @FOSUser/Resetting/request_content.html.twig */
class __TwigTemplate_f2def74867866ead61a0259ab20bd9b713267978b72112a9be89893992ef9de3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_af43500c97e7f494c0c6dce1ce7da7a245dd54060d77e562a6964db1d8666276 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_af43500c97e7f494c0c6dce1ce7da7a245dd54060d77e562a6964db1d8666276->enter($__internal_af43500c97e7f494c0c6dce1ce7da7a245dd54060d77e562a6964db1d8666276_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Resetting/request_content.html.twig"));

        $__internal_37f9176bffd0c2fd76bda603632d5ebdda28fc8b6a8d41fdda9dec7cbd5aac2e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_37f9176bffd0c2fd76bda603632d5ebdda28fc8b6a8d41fdda9dec7cbd5aac2e->enter($__internal_37f9176bffd0c2fd76bda603632d5ebdda28fc8b6a8d41fdda9dec7cbd5aac2e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Resetting/request_content.html.twig"));

        // line 2
        echo "
<form action=\"";
        // line 3
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_resetting_send_email");
        echo "\" method=\"POST\" class=\"fos_user_resetting_request\">
    <div>
        <label for=\"username\">";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.request.username", array(), "FOSUserBundle"), "html", null, true);
        echo "</label>
        <input type=\"text\" id=\"username\" name=\"username\" required=\"required\" />
    </div>
    <div>
        <input type=\"submit\" value=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.request.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
    </div>
</form>
";
        
        $__internal_af43500c97e7f494c0c6dce1ce7da7a245dd54060d77e562a6964db1d8666276->leave($__internal_af43500c97e7f494c0c6dce1ce7da7a245dd54060d77e562a6964db1d8666276_prof);

        
        $__internal_37f9176bffd0c2fd76bda603632d5ebdda28fc8b6a8d41fdda9dec7cbd5aac2e->leave($__internal_37f9176bffd0c2fd76bda603632d5ebdda28fc8b6a8d41fdda9dec7cbd5aac2e_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Resetting/request_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 9,  33 => 5,  28 => 3,  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}

<form action=\"{{ path('fos_user_resetting_send_email') }}\" method=\"POST\" class=\"fos_user_resetting_request\">
    <div>
        <label for=\"username\">{{ 'resetting.request.username'|trans }}</label>
        <input type=\"text\" id=\"username\" name=\"username\" required=\"required\" />
    </div>
    <div>
        <input type=\"submit\" value=\"{{ 'resetting.request.submit'|trans }}\" />
    </div>
</form>
", "@FOSUser/Resetting/request_content.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\app\\Resources\\FOSUserBundle\\views\\Resetting\\request_content.html.twig");
    }
}
