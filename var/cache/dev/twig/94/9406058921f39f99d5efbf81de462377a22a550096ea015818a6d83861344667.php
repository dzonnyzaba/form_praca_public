<?php

/* FOSUserBundle:Group:list.html.twig */
class __TwigTemplate_5d8f2302a224331be1af27aa4a3fc4d3710de1ad21a1c679d43bd098478eecd8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:list.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b4dae2ff154a27b4e3435460eb6f4f728704abe3f768c01290e8e3d0792748a3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b4dae2ff154a27b4e3435460eb6f4f728704abe3f768c01290e8e3d0792748a3->enter($__internal_b4dae2ff154a27b4e3435460eb6f4f728704abe3f768c01290e8e3d0792748a3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:list.html.twig"));

        $__internal_aa9c1031199b15bb7bcfc5b5e3c28c63da5f104623e242e61f5129685e70f5b2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_aa9c1031199b15bb7bcfc5b5e3c28c63da5f104623e242e61f5129685e70f5b2->enter($__internal_aa9c1031199b15bb7bcfc5b5e3c28c63da5f104623e242e61f5129685e70f5b2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:list.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b4dae2ff154a27b4e3435460eb6f4f728704abe3f768c01290e8e3d0792748a3->leave($__internal_b4dae2ff154a27b4e3435460eb6f4f728704abe3f768c01290e8e3d0792748a3_prof);

        
        $__internal_aa9c1031199b15bb7bcfc5b5e3c28c63da5f104623e242e61f5129685e70f5b2->leave($__internal_aa9c1031199b15bb7bcfc5b5e3c28c63da5f104623e242e61f5129685e70f5b2_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_17ff7c7150671b8b689ae7861806d86cddca706f22a968f62b383d5b047de67e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_17ff7c7150671b8b689ae7861806d86cddca706f22a968f62b383d5b047de67e->enter($__internal_17ff7c7150671b8b689ae7861806d86cddca706f22a968f62b383d5b047de67e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_8a6dd634a5ce19aef90dbc2463b9edac79547f2ba8da37ef38a38888679e7cec = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8a6dd634a5ce19aef90dbc2463b9edac79547f2ba8da37ef38a38888679e7cec->enter($__internal_8a6dd634a5ce19aef90dbc2463b9edac79547f2ba8da37ef38a38888679e7cec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/list_content.html.twig", "FOSUserBundle:Group:list.html.twig", 4)->display($context);
        
        $__internal_8a6dd634a5ce19aef90dbc2463b9edac79547f2ba8da37ef38a38888679e7cec->leave($__internal_8a6dd634a5ce19aef90dbc2463b9edac79547f2ba8da37ef38a38888679e7cec_prof);

        
        $__internal_17ff7c7150671b8b689ae7861806d86cddca706f22a968f62b383d5b047de67e->leave($__internal_17ff7c7150671b8b689ae7861806d86cddca706f22a968f62b383d5b047de67e_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/list_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Group:list.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\app/Resources/FOSUserBundle/views/Group/list.html.twig");
    }
}
