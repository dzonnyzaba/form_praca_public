<?php

/* WebProfilerBundle:Collector:router.html.twig */
class __TwigTemplate_773856ec5765f2af18de8d4e66250af78ef7a69794f5beba82d76d1d259e9ee7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c18d18a43831f10d227f0030c0c36c98e1357f092853228c95f5b7475b08e70c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c18d18a43831f10d227f0030c0c36c98e1357f092853228c95f5b7475b08e70c->enter($__internal_c18d18a43831f10d227f0030c0c36c98e1357f092853228c95f5b7475b08e70c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:router.html.twig"));

        $__internal_a7190c528b539c649ce9965786cab827c0425a8128d5047d76d7bf80f413f748 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a7190c528b539c649ce9965786cab827c0425a8128d5047d76d7bf80f413f748->enter($__internal_a7190c528b539c649ce9965786cab827c0425a8128d5047d76d7bf80f413f748_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c18d18a43831f10d227f0030c0c36c98e1357f092853228c95f5b7475b08e70c->leave($__internal_c18d18a43831f10d227f0030c0c36c98e1357f092853228c95f5b7475b08e70c_prof);

        
        $__internal_a7190c528b539c649ce9965786cab827c0425a8128d5047d76d7bf80f413f748->leave($__internal_a7190c528b539c649ce9965786cab827c0425a8128d5047d76d7bf80f413f748_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_c6d599e907cb8a453b8bfd59a87a624ecd9e1560ce970236b4711e4e5970d306 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c6d599e907cb8a453b8bfd59a87a624ecd9e1560ce970236b4711e4e5970d306->enter($__internal_c6d599e907cb8a453b8bfd59a87a624ecd9e1560ce970236b4711e4e5970d306_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_3792ba539ef0d0b196b7d9e73c12ac6e3b53de70f8a8b741febd4b0658d9a8fc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3792ba539ef0d0b196b7d9e73c12ac6e3b53de70f8a8b741febd4b0658d9a8fc->enter($__internal_3792ba539ef0d0b196b7d9e73c12ac6e3b53de70f8a8b741febd4b0658d9a8fc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_3792ba539ef0d0b196b7d9e73c12ac6e3b53de70f8a8b741febd4b0658d9a8fc->leave($__internal_3792ba539ef0d0b196b7d9e73c12ac6e3b53de70f8a8b741febd4b0658d9a8fc_prof);

        
        $__internal_c6d599e907cb8a453b8bfd59a87a624ecd9e1560ce970236b4711e4e5970d306->leave($__internal_c6d599e907cb8a453b8bfd59a87a624ecd9e1560ce970236b4711e4e5970d306_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_f37b591097d99a52fc64ab46abb5e5e216b97df1e03a7f117b4a71f5c411262f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f37b591097d99a52fc64ab46abb5e5e216b97df1e03a7f117b4a71f5c411262f->enter($__internal_f37b591097d99a52fc64ab46abb5e5e216b97df1e03a7f117b4a71f5c411262f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_b541c6961547baef346923c38cb857043bc18e9d418bcad43de0367fbcd3eec2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b541c6961547baef346923c38cb857043bc18e9d418bcad43de0367fbcd3eec2->enter($__internal_b541c6961547baef346923c38cb857043bc18e9d418bcad43de0367fbcd3eec2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_b541c6961547baef346923c38cb857043bc18e9d418bcad43de0367fbcd3eec2->leave($__internal_b541c6961547baef346923c38cb857043bc18e9d418bcad43de0367fbcd3eec2_prof);

        
        $__internal_f37b591097d99a52fc64ab46abb5e5e216b97df1e03a7f117b4a71f5c411262f->leave($__internal_f37b591097d99a52fc64ab46abb5e5e216b97df1e03a7f117b4a71f5c411262f_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_749da682ca1e4e152014ae57f20490ee9fafebf650af232c6f275de534d9f059 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_749da682ca1e4e152014ae57f20490ee9fafebf650af232c6f275de534d9f059->enter($__internal_749da682ca1e4e152014ae57f20490ee9fafebf650af232c6f275de534d9f059_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_a9a5e62ab35bca2203b6af283c61de7f9ea699fe404dd77d8feb1718f44b6a50 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a9a5e62ab35bca2203b6af283c61de7f9ea699fe404dd77d8feb1718f44b6a50->enter($__internal_a9a5e62ab35bca2203b6af283c61de7f9ea699fe404dd77d8feb1718f44b6a50_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_a9a5e62ab35bca2203b6af283c61de7f9ea699fe404dd77d8feb1718f44b6a50->leave($__internal_a9a5e62ab35bca2203b6af283c61de7f9ea699fe404dd77d8feb1718f44b6a50_prof);

        
        $__internal_749da682ca1e4e152014ae57f20490ee9fafebf650af232c6f275de534d9f059->leave($__internal_749da682ca1e4e152014ae57f20490ee9fafebf650af232c6f275de534d9f059_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "WebProfilerBundle:Collector:router.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
