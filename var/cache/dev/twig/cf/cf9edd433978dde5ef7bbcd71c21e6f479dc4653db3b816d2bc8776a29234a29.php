<?php

/* @Framework/FormTable/form_row.html.php */
class __TwigTemplate_98cc9add6a81fe5647a7b72f484169234177a571e4365477dfac2117a1eb3208 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cc7dd01529fba9ab7225d18f1dae60d5f03edac24af4228d81d046f4fac22802 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cc7dd01529fba9ab7225d18f1dae60d5f03edac24af4228d81d046f4fac22802->enter($__internal_cc7dd01529fba9ab7225d18f1dae60d5f03edac24af4228d81d046f4fac22802_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_row.html.php"));

        $__internal_0c836c39f39563e97c938e76260c8ff6ce491b8d31530107e4290f2ba5a3af89 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0c836c39f39563e97c938e76260c8ff6ce491b8d31530107e4290f2ba5a3af89->enter($__internal_0c836c39f39563e97c938e76260c8ff6ce491b8d31530107e4290f2ba5a3af89_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_row.html.php"));

        // line 1
        echo "<tr>
    <td>
        <?php echo \$view['form']->label(\$form) ?>
    </td>
    <td>
        <?php echo \$view['form']->errors(\$form) ?>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_cc7dd01529fba9ab7225d18f1dae60d5f03edac24af4228d81d046f4fac22802->leave($__internal_cc7dd01529fba9ab7225d18f1dae60d5f03edac24af4228d81d046f4fac22802_prof);

        
        $__internal_0c836c39f39563e97c938e76260c8ff6ce491b8d31530107e4290f2ba5a3af89->leave($__internal_0c836c39f39563e97c938e76260c8ff6ce491b8d31530107e4290f2ba5a3af89_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr>
    <td>
        <?php echo \$view['form']->label(\$form) ?>
    </td>
    <td>
        <?php echo \$view['form']->errors(\$form) ?>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
", "@Framework/FormTable/form_row.html.php", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\FormTable\\form_row.html.php");
    }
}
