<?php

/* @FOSUser/Group/show_content.html.twig */
class __TwigTemplate_c29a65c49824af0f78f8697889790551707d487d98d35e677d2ad7be78848e5d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_04bb419797e4d16be2daddb2fac03f8f7ef2b4bfaf192ad74f48b44c4e1ccbb0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_04bb419797e4d16be2daddb2fac03f8f7ef2b4bfaf192ad74f48b44c4e1ccbb0->enter($__internal_04bb419797e4d16be2daddb2fac03f8f7ef2b4bfaf192ad74f48b44c4e1ccbb0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Group/show_content.html.twig"));

        $__internal_8df24ee43cd17fbd78c1b89af4b655beffa7e1f48c2d34b9c4a14b4d5852fd3a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8df24ee43cd17fbd78c1b89af4b655beffa7e1f48c2d34b9c4a14b4d5852fd3a->enter($__internal_8df24ee43cd17fbd78c1b89af4b655beffa7e1f48c2d34b9c4a14b4d5852fd3a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Group/show_content.html.twig"));

        // line 2
        echo "
<div class=\"fos_user_group_show\">
    <p>";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("group.show.name", array(), "FOSUserBundle"), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["group"] ?? $this->getContext($context, "group")), "getName", array(), "method"), "html", null, true);
        echo "</p>
</div>
";
        
        $__internal_04bb419797e4d16be2daddb2fac03f8f7ef2b4bfaf192ad74f48b44c4e1ccbb0->leave($__internal_04bb419797e4d16be2daddb2fac03f8f7ef2b4bfaf192ad74f48b44c4e1ccbb0_prof);

        
        $__internal_8df24ee43cd17fbd78c1b89af4b655beffa7e1f48c2d34b9c4a14b4d5852fd3a->leave($__internal_8df24ee43cd17fbd78c1b89af4b655beffa7e1f48c2d34b9c4a14b4d5852fd3a_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Group/show_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 4,  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}

<div class=\"fos_user_group_show\">
    <p>{{ 'group.show.name'|trans }}: {{ group.getName() }}</p>
</div>
", "@FOSUser/Group/show_content.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\app\\Resources\\FOSUserBundle\\views\\Group\\show_content.html.twig");
    }
}
