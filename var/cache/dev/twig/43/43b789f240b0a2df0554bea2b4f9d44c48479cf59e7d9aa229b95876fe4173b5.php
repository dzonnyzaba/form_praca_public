<?php

/* default/test.html.twig */
class __TwigTemplate_2d4d4478c3f74179628ddee5c5f95b62fbfd04f4b5694f89b999810453a1dbfb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "default/test.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e17ec85c26dd84ce7bf2657fe1bc51e80ebf98f071243ce8d9d5c6cab84a3835 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e17ec85c26dd84ce7bf2657fe1bc51e80ebf98f071243ce8d9d5c6cab84a3835->enter($__internal_e17ec85c26dd84ce7bf2657fe1bc51e80ebf98f071243ce8d9d5c6cab84a3835_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/test.html.twig"));

        $__internal_59b38c45befc0fcabc07a8d266f6f9bf7bebb09beed5b482d75cc7b44eade7bf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_59b38c45befc0fcabc07a8d266f6f9bf7bebb09beed5b482d75cc7b44eade7bf->enter($__internal_59b38c45befc0fcabc07a8d266f6f9bf7bebb09beed5b482d75cc7b44eade7bf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/test.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e17ec85c26dd84ce7bf2657fe1bc51e80ebf98f071243ce8d9d5c6cab84a3835->leave($__internal_e17ec85c26dd84ce7bf2657fe1bc51e80ebf98f071243ce8d9d5c6cab84a3835_prof);

        
        $__internal_59b38c45befc0fcabc07a8d266f6f9bf7bebb09beed5b482d75cc7b44eade7bf->leave($__internal_59b38c45befc0fcabc07a8d266f6f9bf7bebb09beed5b482d75cc7b44eade7bf_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_4df47061ef7647fcf1dbad0e8c62fc1da6723b4219bd81aa28bf9198a24c1564 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4df47061ef7647fcf1dbad0e8c62fc1da6723b4219bd81aa28bf9198a24c1564->enter($__internal_4df47061ef7647fcf1dbad0e8c62fc1da6723b4219bd81aa28bf9198a24c1564_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_88649dc1a25fb128b1894c6c7d8abad44d309fb62a70fd22d24a115bb675a95a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_88649dc1a25fb128b1894c6c7d8abad44d309fb62a70fd22d24a115bb675a95a->enter($__internal_88649dc1a25fb128b1894c6c7d8abad44d309fb62a70fd22d24a115bb675a95a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
<p>cześć!</p>
";
        
        $__internal_88649dc1a25fb128b1894c6c7d8abad44d309fb62a70fd22d24a115bb675a95a->leave($__internal_88649dc1a25fb128b1894c6c7d8abad44d309fb62a70fd22d24a115bb675a95a_prof);

        
        $__internal_4df47061ef7647fcf1dbad0e8c62fc1da6723b4219bd81aa28bf9198a24c1564->leave($__internal_4df47061ef7647fcf1dbad0e8c62fc1da6723b4219bd81aa28bf9198a24c1564_prof);

    }

    public function getTemplateName()
    {
        return "default/test.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '::base.html.twig' %}

{% block body %}

<p>cześć!</p>
{% endblock %}", "default/test.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\app\\Resources\\views\\default\\test.html.twig");
    }
}
