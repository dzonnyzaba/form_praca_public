<?php

/* ::base.html.twig */
class __TwigTemplate_7ba426c3fd6db79c519a62bea0910f1177809241c289686031b107a8977fbd24 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ca7a8d7b46feb7b2a5295f7b35db4a952ab26e0caed3dee0fbd6278ab77b5e9e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ca7a8d7b46feb7b2a5295f7b35db4a952ab26e0caed3dee0fbd6278ab77b5e9e->enter($__internal_ca7a8d7b46feb7b2a5295f7b35db4a952ab26e0caed3dee0fbd6278ab77b5e9e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        $__internal_97485ee577d92a7295a4acde82c63e8bdd62fdb7e17e2411f6f34bafbc588220 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_97485ee577d92a7295a4acde82c63e8bdd62fdb7e17e2411f6f34bafbc588220->enter($__internal_97485ee577d92a7295a4acde82c63e8bdd62fdb7e17e2411f6f34bafbc588220_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">
        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 11
        $this->displayBlock('body', $context, $blocks);
        // line 12
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 13
        echo "    </body>
</html>
";
        
        $__internal_ca7a8d7b46feb7b2a5295f7b35db4a952ab26e0caed3dee0fbd6278ab77b5e9e->leave($__internal_ca7a8d7b46feb7b2a5295f7b35db4a952ab26e0caed3dee0fbd6278ab77b5e9e_prof);

        
        $__internal_97485ee577d92a7295a4acde82c63e8bdd62fdb7e17e2411f6f34bafbc588220->leave($__internal_97485ee577d92a7295a4acde82c63e8bdd62fdb7e17e2411f6f34bafbc588220_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_3d71f097f9dd6f43573fef498aab1e99ed922bcb21639fe59b6cebfefac4acf6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3d71f097f9dd6f43573fef498aab1e99ed922bcb21639fe59b6cebfefac4acf6->enter($__internal_3d71f097f9dd6f43573fef498aab1e99ed922bcb21639fe59b6cebfefac4acf6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_ed9102d4fa5ff696eb16b881612d01c91bd342130f611ceab6eed2fae11fbf31 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ed9102d4fa5ff696eb16b881612d01c91bd342130f611ceab6eed2fae11fbf31->enter($__internal_ed9102d4fa5ff696eb16b881612d01c91bd342130f611ceab6eed2fae11fbf31_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_ed9102d4fa5ff696eb16b881612d01c91bd342130f611ceab6eed2fae11fbf31->leave($__internal_ed9102d4fa5ff696eb16b881612d01c91bd342130f611ceab6eed2fae11fbf31_prof);

        
        $__internal_3d71f097f9dd6f43573fef498aab1e99ed922bcb21639fe59b6cebfefac4acf6->leave($__internal_3d71f097f9dd6f43573fef498aab1e99ed922bcb21639fe59b6cebfefac4acf6_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_ffbe2667c6a57e73e193aed9c16e5285ada43e5dc4d1b08a3132ebc406c03c9f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ffbe2667c6a57e73e193aed9c16e5285ada43e5dc4d1b08a3132ebc406c03c9f->enter($__internal_ffbe2667c6a57e73e193aed9c16e5285ada43e5dc4d1b08a3132ebc406c03c9f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_194b1d552ccc932b44d53e90b293b52136eb851235f7a41cb3d530ec1067d1b7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_194b1d552ccc932b44d53e90b293b52136eb851235f7a41cb3d530ec1067d1b7->enter($__internal_194b1d552ccc932b44d53e90b293b52136eb851235f7a41cb3d530ec1067d1b7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_194b1d552ccc932b44d53e90b293b52136eb851235f7a41cb3d530ec1067d1b7->leave($__internal_194b1d552ccc932b44d53e90b293b52136eb851235f7a41cb3d530ec1067d1b7_prof);

        
        $__internal_ffbe2667c6a57e73e193aed9c16e5285ada43e5dc4d1b08a3132ebc406c03c9f->leave($__internal_ffbe2667c6a57e73e193aed9c16e5285ada43e5dc4d1b08a3132ebc406c03c9f_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_6936f7fb8cc50355f2cb031101b0548a2fde9c1386e3c74327e244022cbed4e8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6936f7fb8cc50355f2cb031101b0548a2fde9c1386e3c74327e244022cbed4e8->enter($__internal_6936f7fb8cc50355f2cb031101b0548a2fde9c1386e3c74327e244022cbed4e8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_b4df4e5b20ab85565292da9e2ef41b822c377f8a5ef0bc4eaacc311aa524439c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b4df4e5b20ab85565292da9e2ef41b822c377f8a5ef0bc4eaacc311aa524439c->enter($__internal_b4df4e5b20ab85565292da9e2ef41b822c377f8a5ef0bc4eaacc311aa524439c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_b4df4e5b20ab85565292da9e2ef41b822c377f8a5ef0bc4eaacc311aa524439c->leave($__internal_b4df4e5b20ab85565292da9e2ef41b822c377f8a5ef0bc4eaacc311aa524439c_prof);

        
        $__internal_6936f7fb8cc50355f2cb031101b0548a2fde9c1386e3c74327e244022cbed4e8->leave($__internal_6936f7fb8cc50355f2cb031101b0548a2fde9c1386e3c74327e244022cbed4e8_prof);

    }

    // line 12
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_2be302ddc2fbab4b6c30428a1275ab20b2fd8edd2118344dc2582ec72078685f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2be302ddc2fbab4b6c30428a1275ab20b2fd8edd2118344dc2582ec72078685f->enter($__internal_2be302ddc2fbab4b6c30428a1275ab20b2fd8edd2118344dc2582ec72078685f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_7615fd12239c9df991c74a4ef9cd75eb7955b9d0093d06a0fc9373179c1e714c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7615fd12239c9df991c74a4ef9cd75eb7955b9d0093d06a0fc9373179c1e714c->enter($__internal_7615fd12239c9df991c74a4ef9cd75eb7955b9d0093d06a0fc9373179c1e714c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_7615fd12239c9df991c74a4ef9cd75eb7955b9d0093d06a0fc9373179c1e714c->leave($__internal_7615fd12239c9df991c74a4ef9cd75eb7955b9d0093d06a0fc9373179c1e714c_prof);

        
        $__internal_2be302ddc2fbab4b6c30428a1275ab20b2fd8edd2118344dc2582ec72078685f->leave($__internal_2be302ddc2fbab4b6c30428a1275ab20b2fd8edd2118344dc2582ec72078685f_prof);

    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  119 => 12,  102 => 11,  85 => 6,  67 => 5,  55 => 13,  52 => 12,  50 => 11,  44 => 8,  41 => 7,  39 => 6,  35 => 5,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>
    <body>
        {% block body %}{% endblock %}
        {% block javascripts %}{% endblock %}
    </body>
</html>
", "::base.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\app/Resources\\views/base.html.twig");
    }
}
