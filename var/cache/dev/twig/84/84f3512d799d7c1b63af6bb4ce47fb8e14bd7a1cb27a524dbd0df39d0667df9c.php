<?php

/* @Framework/Form/form_rest.html.php */
class __TwigTemplate_0fe20de790e04fd2b620675530ee2f85f512ba7de5aef281748e72aaaa8a7575 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_840f1d0deb6c6b10b62635bb64dbaeefc6ab1b921c73533d9837521d3a790c09 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_840f1d0deb6c6b10b62635bb64dbaeefc6ab1b921c73533d9837521d3a790c09->enter($__internal_840f1d0deb6c6b10b62635bb64dbaeefc6ab1b921c73533d9837521d3a790c09_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        $__internal_4c0288cda6efe1f02026acd6b716370cb794b5f6a561f7bcb2990f434b74f1c5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4c0288cda6efe1f02026acd6b716370cb794b5f6a561f7bcb2990f434b74f1c5->enter($__internal_4c0288cda6efe1f02026acd6b716370cb794b5f6a561f7bcb2990f434b74f1c5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
";
        
        $__internal_840f1d0deb6c6b10b62635bb64dbaeefc6ab1b921c73533d9837521d3a790c09->leave($__internal_840f1d0deb6c6b10b62635bb64dbaeefc6ab1b921c73533d9837521d3a790c09_prof);

        
        $__internal_4c0288cda6efe1f02026acd6b716370cb794b5f6a561f7bcb2990f434b74f1c5->leave($__internal_4c0288cda6efe1f02026acd6b716370cb794b5f6a561f7bcb2990f434b74f1c5_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rest.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
", "@Framework/Form/form_rest.html.php", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\form_rest.html.php");
    }
}
