<?php

/* @Framework/FormTable/hidden_row.html.php */
class __TwigTemplate_c1c6d9174d573178796b3159335813abc7dff9cbd033779c19d1ee5b9c5fa108 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_03bec386d466f81405c38578cf37a75826a748717de1d2977163eefee46c4ceb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_03bec386d466f81405c38578cf37a75826a748717de1d2977163eefee46c4ceb->enter($__internal_03bec386d466f81405c38578cf37a75826a748717de1d2977163eefee46c4ceb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        $__internal_459a73968dc4d654d74f55b7f5a75953609d37285e6e9c60cbcc51534c5ad2fe = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_459a73968dc4d654d74f55b7f5a75953609d37285e6e9c60cbcc51534c5ad2fe->enter($__internal_459a73968dc4d654d74f55b7f5a75953609d37285e6e9c60cbcc51534c5ad2fe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        // line 1
        echo "<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_03bec386d466f81405c38578cf37a75826a748717de1d2977163eefee46c4ceb->leave($__internal_03bec386d466f81405c38578cf37a75826a748717de1d2977163eefee46c4ceb_prof);

        
        $__internal_459a73968dc4d654d74f55b7f5a75953609d37285e6e9c60cbcc51534c5ad2fe->leave($__internal_459a73968dc4d654d74f55b7f5a75953609d37285e6e9c60cbcc51534c5ad2fe_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
", "@Framework/FormTable/hidden_row.html.php", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\FormTable\\hidden_row.html.php");
    }
}
