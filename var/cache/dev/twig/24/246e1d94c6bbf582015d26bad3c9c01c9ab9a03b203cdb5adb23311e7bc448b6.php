<?php

/* WebProfilerBundle:Collector:exception.html.twig */
class __TwigTemplate_f2061152e22b3b2391859ea642e0d745ff39c7846225ac74f8dcde9a43603dbe extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c98421d257c29d43b4808e6766d31a9acbec190463bced4470409ecd393c17b6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c98421d257c29d43b4808e6766d31a9acbec190463bced4470409ecd393c17b6->enter($__internal_c98421d257c29d43b4808e6766d31a9acbec190463bced4470409ecd393c17b6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:exception.html.twig"));

        $__internal_e51e376c60639ceb1dceb01cd89cb0263e87dffe924cf1710c119ca6127c0974 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e51e376c60639ceb1dceb01cd89cb0263e87dffe924cf1710c119ca6127c0974->enter($__internal_e51e376c60639ceb1dceb01cd89cb0263e87dffe924cf1710c119ca6127c0974_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c98421d257c29d43b4808e6766d31a9acbec190463bced4470409ecd393c17b6->leave($__internal_c98421d257c29d43b4808e6766d31a9acbec190463bced4470409ecd393c17b6_prof);

        
        $__internal_e51e376c60639ceb1dceb01cd89cb0263e87dffe924cf1710c119ca6127c0974->leave($__internal_e51e376c60639ceb1dceb01cd89cb0263e87dffe924cf1710c119ca6127c0974_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_e037a8f87ed613de811ff275319f1a7bb82d1ddafde841591bb877092b54893b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e037a8f87ed613de811ff275319f1a7bb82d1ddafde841591bb877092b54893b->enter($__internal_e037a8f87ed613de811ff275319f1a7bb82d1ddafde841591bb877092b54893b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_e6cd7ee9676865139a9812d8fcefc355578d71de49e5f925de1a120514ae0900 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e6cd7ee9676865139a9812d8fcefc355578d71de49e5f925de1a120514ae0900->enter($__internal_e6cd7ee9676865139a9812d8fcefc355578d71de49e5f925de1a120514ae0900_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_e6cd7ee9676865139a9812d8fcefc355578d71de49e5f925de1a120514ae0900->leave($__internal_e6cd7ee9676865139a9812d8fcefc355578d71de49e5f925de1a120514ae0900_prof);

        
        $__internal_e037a8f87ed613de811ff275319f1a7bb82d1ddafde841591bb877092b54893b->leave($__internal_e037a8f87ed613de811ff275319f1a7bb82d1ddafde841591bb877092b54893b_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_ceb7b22c9406755ee5a79ff8c5ffb10d52c728c9ebd44614184f29f2228a4e0d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ceb7b22c9406755ee5a79ff8c5ffb10d52c728c9ebd44614184f29f2228a4e0d->enter($__internal_ceb7b22c9406755ee5a79ff8c5ffb10d52c728c9ebd44614184f29f2228a4e0d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_cb932a0737bec728dab75e20ad48bf7fed7648c613829124dcbbfe088762d391 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cb932a0737bec728dab75e20ad48bf7fed7648c613829124dcbbfe088762d391->enter($__internal_cb932a0737bec728dab75e20ad48bf7fed7648c613829124dcbbfe088762d391_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_cb932a0737bec728dab75e20ad48bf7fed7648c613829124dcbbfe088762d391->leave($__internal_cb932a0737bec728dab75e20ad48bf7fed7648c613829124dcbbfe088762d391_prof);

        
        $__internal_ceb7b22c9406755ee5a79ff8c5ffb10d52c728c9ebd44614184f29f2228a4e0d->leave($__internal_ceb7b22c9406755ee5a79ff8c5ffb10d52c728c9ebd44614184f29f2228a4e0d_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_8aace340d143fb98eda84eb9446ad479a4ce83f93559b789adc75d5dccc9313e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8aace340d143fb98eda84eb9446ad479a4ce83f93559b789adc75d5dccc9313e->enter($__internal_8aace340d143fb98eda84eb9446ad479a4ce83f93559b789adc75d5dccc9313e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_8c54a245cece0acf50c603f491a967553893a179672ff210da36552982f3a7ee = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8c54a245cece0acf50c603f491a967553893a179672ff210da36552982f3a7ee->enter($__internal_8c54a245cece0acf50c603f491a967553893a179672ff210da36552982f3a7ee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_8c54a245cece0acf50c603f491a967553893a179672ff210da36552982f3a7ee->leave($__internal_8c54a245cece0acf50c603f491a967553893a179672ff210da36552982f3a7ee_prof);

        
        $__internal_8aace340d143fb98eda84eb9446ad479a4ce83f93559b789adc75d5dccc9313e->leave($__internal_8aace340d143fb98eda84eb9446ad479a4ce83f93559b789adc75d5dccc9313e_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "WebProfilerBundle:Collector:exception.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle/Resources/views/Collector/exception.html.twig");
    }
}
