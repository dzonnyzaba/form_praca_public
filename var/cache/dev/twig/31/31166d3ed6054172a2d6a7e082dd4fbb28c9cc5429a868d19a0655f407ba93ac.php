<?php

/* FOSUserBundle:Group:new.html.twig */
class __TwigTemplate_cd4de73fde2a71f79dcaa6c79c7012113e05272a31fd01d3c9886166d07f977f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:new.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ab3c95b496cf6b323f7fe29db1a56c69b67c52322e1c2318308e838881c60a95 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ab3c95b496cf6b323f7fe29db1a56c69b67c52322e1c2318308e838881c60a95->enter($__internal_ab3c95b496cf6b323f7fe29db1a56c69b67c52322e1c2318308e838881c60a95_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:new.html.twig"));

        $__internal_df2a6c057a5ca317f54ba38ff1e0a46269777a741657d11e3e1979ee5d7717f8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_df2a6c057a5ca317f54ba38ff1e0a46269777a741657d11e3e1979ee5d7717f8->enter($__internal_df2a6c057a5ca317f54ba38ff1e0a46269777a741657d11e3e1979ee5d7717f8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ab3c95b496cf6b323f7fe29db1a56c69b67c52322e1c2318308e838881c60a95->leave($__internal_ab3c95b496cf6b323f7fe29db1a56c69b67c52322e1c2318308e838881c60a95_prof);

        
        $__internal_df2a6c057a5ca317f54ba38ff1e0a46269777a741657d11e3e1979ee5d7717f8->leave($__internal_df2a6c057a5ca317f54ba38ff1e0a46269777a741657d11e3e1979ee5d7717f8_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_c3398a358839072aac9130fb886f05ed721010bc7c94e32ecc20be6ee3e71d3d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c3398a358839072aac9130fb886f05ed721010bc7c94e32ecc20be6ee3e71d3d->enter($__internal_c3398a358839072aac9130fb886f05ed721010bc7c94e32ecc20be6ee3e71d3d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_de5dbc2302f87f95077bed8cfaa6579553ee3f8cc67031e94f5b354e8f881b49 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_de5dbc2302f87f95077bed8cfaa6579553ee3f8cc67031e94f5b354e8f881b49->enter($__internal_de5dbc2302f87f95077bed8cfaa6579553ee3f8cc67031e94f5b354e8f881b49_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/new_content.html.twig", "FOSUserBundle:Group:new.html.twig", 4)->display($context);
        
        $__internal_de5dbc2302f87f95077bed8cfaa6579553ee3f8cc67031e94f5b354e8f881b49->leave($__internal_de5dbc2302f87f95077bed8cfaa6579553ee3f8cc67031e94f5b354e8f881b49_prof);

        
        $__internal_c3398a358839072aac9130fb886f05ed721010bc7c94e32ecc20be6ee3e71d3d->leave($__internal_c3398a358839072aac9130fb886f05ed721010bc7c94e32ecc20be6ee3e71d3d_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/new_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Group:new.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\app/Resources/FOSUserBundle/views/Group/new.html.twig");
    }
}
