<?php

/* @FOSUser/Registration/confirmed.html.twig */
class __TwigTemplate_22e3d684e273eeee28b5254e3271dc32f0e9923eea92051a16a2651d95620f23 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Registration/confirmed.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_505586da3ed9a67321ec58f31f570a3eeff864170f08f770e675ecb2e8778c8f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_505586da3ed9a67321ec58f31f570a3eeff864170f08f770e675ecb2e8778c8f->enter($__internal_505586da3ed9a67321ec58f31f570a3eeff864170f08f770e675ecb2e8778c8f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/confirmed.html.twig"));

        $__internal_e228737142e04173e19a72c5125f6266d2e68ba1cae0bc1f22663147fd2576b2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e228737142e04173e19a72c5125f6266d2e68ba1cae0bc1f22663147fd2576b2->enter($__internal_e228737142e04173e19a72c5125f6266d2e68ba1cae0bc1f22663147fd2576b2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/confirmed.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_505586da3ed9a67321ec58f31f570a3eeff864170f08f770e675ecb2e8778c8f->leave($__internal_505586da3ed9a67321ec58f31f570a3eeff864170f08f770e675ecb2e8778c8f_prof);

        
        $__internal_e228737142e04173e19a72c5125f6266d2e68ba1cae0bc1f22663147fd2576b2->leave($__internal_e228737142e04173e19a72c5125f6266d2e68ba1cae0bc1f22663147fd2576b2_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_eb0f1142e927dd723b5ac2ee4b436bb3543534615efdbea433eab750552ab34c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_eb0f1142e927dd723b5ac2ee4b436bb3543534615efdbea433eab750552ab34c->enter($__internal_eb0f1142e927dd723b5ac2ee4b436bb3543534615efdbea433eab750552ab34c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_43cb357edc546d0ec201b29e568a60e26bf6fa52efa203e8380d27de58056812 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_43cb357edc546d0ec201b29e568a60e26bf6fa52efa203e8380d27de58056812->enter($__internal_43cb357edc546d0ec201b29e568a60e26bf6fa52efa203e8380d27de58056812_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "    <p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.confirmed", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
    ";
        // line 7
        if (($context["targetUrl"] ?? $this->getContext($context, "targetUrl"))) {
            // line 8
            echo "    <p><a href=\"";
            echo twig_escape_filter($this->env, ($context["targetUrl"] ?? $this->getContext($context, "targetUrl")), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.back", array(), "FOSUserBundle"), "html", null, true);
            echo "</a></p>
    ";
        }
        
        $__internal_43cb357edc546d0ec201b29e568a60e26bf6fa52efa203e8380d27de58056812->leave($__internal_43cb357edc546d0ec201b29e568a60e26bf6fa52efa203e8380d27de58056812_prof);

        
        $__internal_eb0f1142e927dd723b5ac2ee4b436bb3543534615efdbea433eab750552ab34c->leave($__internal_eb0f1142e927dd723b5ac2ee4b436bb3543534615efdbea433eab750552ab34c_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Registration/confirmed.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  56 => 8,  54 => 7,  49 => 6,  40 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% trans_default_domain 'FOSUserBundle' %}

{% block fos_user_content %}
    <p>{{ 'registration.confirmed'|trans({'%username%': user.username}) }}</p>
    {% if targetUrl %}
    <p><a href=\"{{ targetUrl }}\">{{ 'registration.back'|trans }}</a></p>
    {% endif %}
{% endblock fos_user_content %}
", "@FOSUser/Registration/confirmed.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\app\\Resources\\FOSUserBundle\\views\\Registration\\confirmed.html.twig");
    }
}
