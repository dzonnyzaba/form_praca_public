<?php

/* @WebProfiler/Profiler/toolbar_redirect.html.twig */
class __TwigTemplate_a19bda121ee4596e3f49be1d55380b502617e75c6213f5a735ef4dbf693a42c4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@WebProfiler/Profiler/toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_aa0b97c75d2fd64e02d48e65e82fc8a94011fd93015704fd660c5df7eee286e5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_aa0b97c75d2fd64e02d48e65e82fc8a94011fd93015704fd660c5df7eee286e5->enter($__internal_aa0b97c75d2fd64e02d48e65e82fc8a94011fd93015704fd660c5df7eee286e5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/toolbar_redirect.html.twig"));

        $__internal_54158942f722825fe2ae61a49d76e3e62f6edc9c6251991a11157a6fc251b30d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_54158942f722825fe2ae61a49d76e3e62f6edc9c6251991a11157a6fc251b30d->enter($__internal_54158942f722825fe2ae61a49d76e3e62f6edc9c6251991a11157a6fc251b30d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_aa0b97c75d2fd64e02d48e65e82fc8a94011fd93015704fd660c5df7eee286e5->leave($__internal_aa0b97c75d2fd64e02d48e65e82fc8a94011fd93015704fd660c5df7eee286e5_prof);

        
        $__internal_54158942f722825fe2ae61a49d76e3e62f6edc9c6251991a11157a6fc251b30d->leave($__internal_54158942f722825fe2ae61a49d76e3e62f6edc9c6251991a11157a6fc251b30d_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_45eafeaf9ecd8d83d091c13579e93165b89ee5bd18599b210a4afdce633184b5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_45eafeaf9ecd8d83d091c13579e93165b89ee5bd18599b210a4afdce633184b5->enter($__internal_45eafeaf9ecd8d83d091c13579e93165b89ee5bd18599b210a4afdce633184b5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_da9649981260feffad019b7305c452414cc900b39eb148ad3127e0cd64a1a21c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_da9649981260feffad019b7305c452414cc900b39eb148ad3127e0cd64a1a21c->enter($__internal_da9649981260feffad019b7305c452414cc900b39eb148ad3127e0cd64a1a21c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_da9649981260feffad019b7305c452414cc900b39eb148ad3127e0cd64a1a21c->leave($__internal_da9649981260feffad019b7305c452414cc900b39eb148ad3127e0cd64a1a21c_prof);

        
        $__internal_45eafeaf9ecd8d83d091c13579e93165b89ee5bd18599b210a4afdce633184b5->leave($__internal_45eafeaf9ecd8d83d091c13579e93165b89ee5bd18599b210a4afdce633184b5_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_12e48c2d4881c238429efc867821e11bc130a42fb51bb57b71e3d4186e9eeaeb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_12e48c2d4881c238429efc867821e11bc130a42fb51bb57b71e3d4186e9eeaeb->enter($__internal_12e48c2d4881c238429efc867821e11bc130a42fb51bb57b71e3d4186e9eeaeb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_d59b1995b4f2d1aea508a5056e33dd97003b32eec6a22af5b2b9daddb4648f11 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d59b1995b4f2d1aea508a5056e33dd97003b32eec6a22af5b2b9daddb4648f11->enter($__internal_d59b1995b4f2d1aea508a5056e33dd97003b32eec6a22af5b2b9daddb4648f11_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, ($context["location"] ?? $this->getContext($context, "location")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, ($context["location"] ?? $this->getContext($context, "location")), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_d59b1995b4f2d1aea508a5056e33dd97003b32eec6a22af5b2b9daddb4648f11->leave($__internal_d59b1995b4f2d1aea508a5056e33dd97003b32eec6a22af5b2b9daddb4648f11_prof);

        
        $__internal_12e48c2d4881c238429efc867821e11bc130a42fb51bb57b71e3d4186e9eeaeb->leave($__internal_12e48c2d4881c238429efc867821e11bc130a42fb51bb57b71e3d4186e9eeaeb_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 8,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block title 'Redirection Intercepted' %}

{% block body %}
    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"{{ location }}\">{{ location }}</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
{% endblock %}
", "@WebProfiler/Profiler/toolbar_redirect.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Profiler\\toolbar_redirect.html.twig");
    }
}
