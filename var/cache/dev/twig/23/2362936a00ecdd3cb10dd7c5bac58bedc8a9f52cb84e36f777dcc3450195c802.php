<?php

/* FOSUserBundle:Registration:check_email.html.twig */
class __TwigTemplate_ddf2108aa83c97024691b1f725f9db1c9b53b098424ea4327de277dc69adc7f9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Registration:check_email.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_920aebd4ac26b7254692e15e1d1e8fa8caaa8627c1b74c5822fa7bd153897960 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_920aebd4ac26b7254692e15e1d1e8fa8caaa8627c1b74c5822fa7bd153897960->enter($__internal_920aebd4ac26b7254692e15e1d1e8fa8caaa8627c1b74c5822fa7bd153897960_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:check_email.html.twig"));

        $__internal_9f0bf252558ecb0ba306e76406ba04c095a13f12e23f013aa541ed51d473b0cb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9f0bf252558ecb0ba306e76406ba04c095a13f12e23f013aa541ed51d473b0cb->enter($__internal_9f0bf252558ecb0ba306e76406ba04c095a13f12e23f013aa541ed51d473b0cb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:check_email.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_920aebd4ac26b7254692e15e1d1e8fa8caaa8627c1b74c5822fa7bd153897960->leave($__internal_920aebd4ac26b7254692e15e1d1e8fa8caaa8627c1b74c5822fa7bd153897960_prof);

        
        $__internal_9f0bf252558ecb0ba306e76406ba04c095a13f12e23f013aa541ed51d473b0cb->leave($__internal_9f0bf252558ecb0ba306e76406ba04c095a13f12e23f013aa541ed51d473b0cb_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_b1dadfddd310edde6dc8256cb76ac929f994c2832500c90a83653b87f68c46d1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b1dadfddd310edde6dc8256cb76ac929f994c2832500c90a83653b87f68c46d1->enter($__internal_b1dadfddd310edde6dc8256cb76ac929f994c2832500c90a83653b87f68c46d1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_9e3a00000a7393102f291dcc671c9e0fed46b2361477db9cd4d4bd536cf0ac80 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9e3a00000a7393102f291dcc671c9e0fed46b2361477db9cd4d4bd536cf0ac80->enter($__internal_9e3a00000a7393102f291dcc671c9e0fed46b2361477db9cd4d4bd536cf0ac80_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "    <p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.check_email", array("%email%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "email", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
";
        
        $__internal_9e3a00000a7393102f291dcc671c9e0fed46b2361477db9cd4d4bd536cf0ac80->leave($__internal_9e3a00000a7393102f291dcc671c9e0fed46b2361477db9cd4d4bd536cf0ac80_prof);

        
        $__internal_b1dadfddd310edde6dc8256cb76ac929f994c2832500c90a83653b87f68c46d1->leave($__internal_b1dadfddd310edde6dc8256cb76ac929f994c2832500c90a83653b87f68c46d1_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:check_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 6,  40 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% trans_default_domain 'FOSUserBundle' %}

{% block fos_user_content %}
    <p>{{ 'registration.check_email'|trans({'%email%': user.email}) }}</p>
{% endblock fos_user_content %}
", "FOSUserBundle:Registration:check_email.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\app/Resources/FOSUserBundle/views/Registration/check_email.html.twig");
    }
}
