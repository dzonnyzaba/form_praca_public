<?php

/* FOSUserBundle:Profile:show.html.twig */
class __TwigTemplate_2f69a5ebf728b3f5acbc706fb1554b7bc1ba7cf8ba31091a7d04b10fd6864fd3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Profile:show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e0c3d89e476c9a307ff56a1da92c418c3ff99fffd82b9473421f2517d8cfd35a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e0c3d89e476c9a307ff56a1da92c418c3ff99fffd82b9473421f2517d8cfd35a->enter($__internal_e0c3d89e476c9a307ff56a1da92c418c3ff99fffd82b9473421f2517d8cfd35a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:show.html.twig"));

        $__internal_7779bd9771846a80d7ca9b0ea2206cf5666487684c74220cb26922ebda9477ff = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7779bd9771846a80d7ca9b0ea2206cf5666487684c74220cb26922ebda9477ff->enter($__internal_7779bd9771846a80d7ca9b0ea2206cf5666487684c74220cb26922ebda9477ff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e0c3d89e476c9a307ff56a1da92c418c3ff99fffd82b9473421f2517d8cfd35a->leave($__internal_e0c3d89e476c9a307ff56a1da92c418c3ff99fffd82b9473421f2517d8cfd35a_prof);

        
        $__internal_7779bd9771846a80d7ca9b0ea2206cf5666487684c74220cb26922ebda9477ff->leave($__internal_7779bd9771846a80d7ca9b0ea2206cf5666487684c74220cb26922ebda9477ff_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_e21113a41c9f736819b27ef30a7922489ff33773e807c75512f18c43431e337f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e21113a41c9f736819b27ef30a7922489ff33773e807c75512f18c43431e337f->enter($__internal_e21113a41c9f736819b27ef30a7922489ff33773e807c75512f18c43431e337f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_b950fccc006224e5f412acee0378da7d5cbed4542b6d504bff0947370e1691ef = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b950fccc006224e5f412acee0378da7d5cbed4542b6d504bff0947370e1691ef->enter($__internal_b950fccc006224e5f412acee0378da7d5cbed4542b6d504bff0947370e1691ef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Profile/show_content.html.twig", "FOSUserBundle:Profile:show.html.twig", 4)->display($context);
        
        $__internal_b950fccc006224e5f412acee0378da7d5cbed4542b6d504bff0947370e1691ef->leave($__internal_b950fccc006224e5f412acee0378da7d5cbed4542b6d504bff0947370e1691ef_prof);

        
        $__internal_e21113a41c9f736819b27ef30a7922489ff33773e807c75512f18c43431e337f->leave($__internal_e21113a41c9f736819b27ef30a7922489ff33773e807c75512f18c43431e337f_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Profile/show_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Profile:show.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\app/Resources/FOSUserBundle/views/Profile/show.html.twig");
    }
}
