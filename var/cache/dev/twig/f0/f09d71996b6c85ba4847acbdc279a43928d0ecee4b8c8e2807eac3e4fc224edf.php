<?php

/* @WebProfiler/Profiler/info.html.twig */
class __TwigTemplate_cf902a21c4c1433c9ea27b88cfa3e9140d354bcacd2d33cd74b9d7846d455b53 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Profiler/info.html.twig", 1);
        $this->blocks = array(
            'summary' => array($this, 'block_summary'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d0e7bac90f6aff48cce97c875ebc53eb9c2e6199ab287154ba25056fda97c797 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d0e7bac90f6aff48cce97c875ebc53eb9c2e6199ab287154ba25056fda97c797->enter($__internal_d0e7bac90f6aff48cce97c875ebc53eb9c2e6199ab287154ba25056fda97c797_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/info.html.twig"));

        $__internal_967aba27ae4c33381358834c4a38b715cfd684f7f8867befddc77cfa13ec938c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_967aba27ae4c33381358834c4a38b715cfd684f7f8867befddc77cfa13ec938c->enter($__internal_967aba27ae4c33381358834c4a38b715cfd684f7f8867befddc77cfa13ec938c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/info.html.twig"));

        // line 3
        $context["messages"] = array("no_token" => array("status" => "error", "title" => (((((        // line 6
array_key_exists("token", $context)) ? (_twig_default_filter(($context["token"] ?? $this->getContext($context, "token")), "")) : ("")) == "latest")) ? ("There are no profiles") : ("Token not found")), "message" => (((((        // line 7
array_key_exists("token", $context)) ? (_twig_default_filter(($context["token"] ?? $this->getContext($context, "token")), "")) : ("")) == "latest")) ? ("No profiles found in the database.") : ((("Token \"" . ((array_key_exists("token", $context)) ? (_twig_default_filter(($context["token"] ?? $this->getContext($context, "token")), "")) : (""))) . "\" was not found in the database.")))));
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d0e7bac90f6aff48cce97c875ebc53eb9c2e6199ab287154ba25056fda97c797->leave($__internal_d0e7bac90f6aff48cce97c875ebc53eb9c2e6199ab287154ba25056fda97c797_prof);

        
        $__internal_967aba27ae4c33381358834c4a38b715cfd684f7f8867befddc77cfa13ec938c->leave($__internal_967aba27ae4c33381358834c4a38b715cfd684f7f8867befddc77cfa13ec938c_prof);

    }

    // line 11
    public function block_summary($context, array $blocks = array())
    {
        $__internal_e30354c6e6704e1cd45891be400ab92108aafbc4c934e614299610072ad9f83a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e30354c6e6704e1cd45891be400ab92108aafbc4c934e614299610072ad9f83a->enter($__internal_e30354c6e6704e1cd45891be400ab92108aafbc4c934e614299610072ad9f83a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "summary"));

        $__internal_276de3ac093013be009927b8dae8469a8c14b5e671e93cd785001a13189227bc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_276de3ac093013be009927b8dae8469a8c14b5e671e93cd785001a13189227bc->enter($__internal_276de3ac093013be009927b8dae8469a8c14b5e671e93cd785001a13189227bc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "summary"));

        // line 12
        echo "    <div class=\"status status-";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "status", array()), "html", null, true);
        echo "\">
        <div class=\"container\">
            <h2>";
        // line 14
        echo twig_escape_filter($this->env, twig_title_string_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "status", array())), "html", null, true);
        echo "</h2>
        </div>
    </div>
";
        
        $__internal_276de3ac093013be009927b8dae8469a8c14b5e671e93cd785001a13189227bc->leave($__internal_276de3ac093013be009927b8dae8469a8c14b5e671e93cd785001a13189227bc_prof);

        
        $__internal_e30354c6e6704e1cd45891be400ab92108aafbc4c934e614299610072ad9f83a->leave($__internal_e30354c6e6704e1cd45891be400ab92108aafbc4c934e614299610072ad9f83a_prof);

    }

    // line 19
    public function block_panel($context, array $blocks = array())
    {
        $__internal_d5d64092283da8924a131f6e9ec19a7ae9c0b4d38e96dce321c6b6c539c2c5fe = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d5d64092283da8924a131f6e9ec19a7ae9c0b4d38e96dce321c6b6c539c2c5fe->enter($__internal_d5d64092283da8924a131f6e9ec19a7ae9c0b4d38e96dce321c6b6c539c2c5fe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_ae9dbedc5a2c2def35e028dbb15b6ccf5a5b26a19f9530e39dc048121cc843a1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ae9dbedc5a2c2def35e028dbb15b6ccf5a5b26a19f9530e39dc048121cc843a1->enter($__internal_ae9dbedc5a2c2def35e028dbb15b6ccf5a5b26a19f9530e39dc048121cc843a1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 20
        echo "    <h2>";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "title", array()), "html", null, true);
        echo "</h2>
    <p>";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "message", array()), "html", null, true);
        echo "</p>
";
        
        $__internal_ae9dbedc5a2c2def35e028dbb15b6ccf5a5b26a19f9530e39dc048121cc843a1->leave($__internal_ae9dbedc5a2c2def35e028dbb15b6ccf5a5b26a19f9530e39dc048121cc843a1_prof);

        
        $__internal_d5d64092283da8924a131f6e9ec19a7ae9c0b4d38e96dce321c6b6c539c2c5fe->leave($__internal_d5d64092283da8924a131f6e9ec19a7ae9c0b4d38e96dce321c6b6c539c2c5fe_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/info.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 21,  84 => 20,  75 => 19,  61 => 14,  55 => 12,  46 => 11,  36 => 1,  34 => 7,  33 => 6,  32 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% set messages = {
    'no_token' : {
        status:  'error',
        title:   (token|default('') == 'latest') ? 'There are no profiles' : 'Token not found',
        message: (token|default('') == 'latest') ? 'No profiles found in the database.' : 'Token \"' ~ token|default('') ~ '\" was not found in the database.'
    }
} %}

{% block summary %}
    <div class=\"status status-{{ messages[about].status }}\">
        <div class=\"container\">
            <h2>{{ messages[about].status|title }}</h2>
        </div>
    </div>
{% endblock %}

{% block panel %}
    <h2>{{ messages[about].title }}</h2>
    <p>{{ messages[about].message }}</p>
{% endblock %}
", "@WebProfiler/Profiler/info.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Profiler\\info.html.twig");
    }
}
