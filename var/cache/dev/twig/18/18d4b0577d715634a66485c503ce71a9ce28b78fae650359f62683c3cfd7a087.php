<?php

/* @Framework/Form/button_row.html.php */
class __TwigTemplate_f8d63f96e59730aa31ded04d89d5ea9d3c00c754f5589fe6da826ecc8d295fd3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_86349bcbd7f13111c51cfd51e6b90743124b83be7fa39a4840f36fe172261ca5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_86349bcbd7f13111c51cfd51e6b90743124b83be7fa39a4840f36fe172261ca5->enter($__internal_86349bcbd7f13111c51cfd51e6b90743124b83be7fa39a4840f36fe172261ca5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        $__internal_19c882a52ee0e82a68ee94705a7ff14e9518fb34e1f576ed6b4ce97de5b930ac = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_19c882a52ee0e82a68ee94705a7ff14e9518fb34e1f576ed6b4ce97de5b930ac->enter($__internal_19c882a52ee0e82a68ee94705a7ff14e9518fb34e1f576ed6b4ce97de5b930ac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_86349bcbd7f13111c51cfd51e6b90743124b83be7fa39a4840f36fe172261ca5->leave($__internal_86349bcbd7f13111c51cfd51e6b90743124b83be7fa39a4840f36fe172261ca5_prof);

        
        $__internal_19c882a52ee0e82a68ee94705a7ff14e9518fb34e1f576ed6b4ce97de5b930ac->leave($__internal_19c882a52ee0e82a68ee94705a7ff14e9518fb34e1f576ed6b4ce97de5b930ac_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
", "@Framework/Form/button_row.html.php", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\button_row.html.php");
    }
}
