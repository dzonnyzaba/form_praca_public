<?php

/* @FOSUser/Registration/register.html.twig */
class __TwigTemplate_0919eee8d31f7752d96455f8501378ee83191ea1ee5170ff00f4ff97f54c3b51 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Registration/register.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fd19a4045cebd1661225a283ca212d3f389acc10417770f6b4c83a5efe46d511 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fd19a4045cebd1661225a283ca212d3f389acc10417770f6b4c83a5efe46d511->enter($__internal_fd19a4045cebd1661225a283ca212d3f389acc10417770f6b4c83a5efe46d511_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/register.html.twig"));

        $__internal_731e7edd849a06588a6605bc86e25e460b25fd8db3f2b52f56ac0faed9a12866 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_731e7edd849a06588a6605bc86e25e460b25fd8db3f2b52f56ac0faed9a12866->enter($__internal_731e7edd849a06588a6605bc86e25e460b25fd8db3f2b52f56ac0faed9a12866_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/register.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_fd19a4045cebd1661225a283ca212d3f389acc10417770f6b4c83a5efe46d511->leave($__internal_fd19a4045cebd1661225a283ca212d3f389acc10417770f6b4c83a5efe46d511_prof);

        
        $__internal_731e7edd849a06588a6605bc86e25e460b25fd8db3f2b52f56ac0faed9a12866->leave($__internal_731e7edd849a06588a6605bc86e25e460b25fd8db3f2b52f56ac0faed9a12866_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_4452ee6174dac589e3536bf6f8d7a91b589a9404fd5b1691ea260f154af06ea1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4452ee6174dac589e3536bf6f8d7a91b589a9404fd5b1691ea260f154af06ea1->enter($__internal_4452ee6174dac589e3536bf6f8d7a91b589a9404fd5b1691ea260f154af06ea1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_c8647aea149310172513dab6e62b876c368ae23aac262a2332d4dbb35faa2496 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c8647aea149310172513dab6e62b876c368ae23aac262a2332d4dbb35faa2496->enter($__internal_c8647aea149310172513dab6e62b876c368ae23aac262a2332d4dbb35faa2496_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Registration/register_content.html.twig", "@FOSUser/Registration/register.html.twig", 4)->display($context);
        
        $__internal_c8647aea149310172513dab6e62b876c368ae23aac262a2332d4dbb35faa2496->leave($__internal_c8647aea149310172513dab6e62b876c368ae23aac262a2332d4dbb35faa2496_prof);

        
        $__internal_4452ee6174dac589e3536bf6f8d7a91b589a9404fd5b1691ea260f154af06ea1->leave($__internal_4452ee6174dac589e3536bf6f8d7a91b589a9404fd5b1691ea260f154af06ea1_prof);

    }

    // line 6
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_268a172340296bad60983ac9540dcd42560bbce1adfdbfafd9eca64d57bb91a2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_268a172340296bad60983ac9540dcd42560bbce1adfdbfafd9eca64d57bb91a2->enter($__internal_268a172340296bad60983ac9540dcd42560bbce1adfdbfafd9eca64d57bb91a2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_6270ae80f7c4916df538cb6c9c5b9a3860a08e21500d197bb9e495d73dd35481 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6270ae80f7c4916df538cb6c9c5b9a3860a08e21500d197bb9e495d73dd35481->enter($__internal_6270ae80f7c4916df538cb6c9c5b9a3860a08e21500d197bb9e495d73dd35481_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 7
        echo "<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js\"></script>
<script>
    
    \$(document).ready(function(){
        
        \$('#fos_user_registration_form_imie').on('blur', function(){
            var input = \$(this);
            var input_length = input.val().length;
            if(input_length < 1){
                input.removeClass(\"valid\").addClass(\"invalid\");
                input.next('.komunikat').text(\"to pole nie może być puste.\").removeClass(\"ok\").addClass(\"blad\");
            }else if(input_length > 10){
                input.removeClass(\"valid\").addClass(\"invalid\");
                input.next('.komunikat').text(\"to pole nie może mieć więcej niż 10 znaków\").removeClass(\"ok\").addClass(\"blad\");
            }else{
                input.removeClass(\"invalid\").addClass(\"valid\");
                input.next('.komunikat').text(\"\").removeClass(\"blad\").addClass(\"ok\");
            }
        });
        
        \$('#fos_user_registration_form_nazwisko').on('blur', function(){
            var input = \$(this);
            var input_length = input.val().length;
            if(input_length < 1){
                input.removeClass(\"valid\").addClass(\"invalid\");
                input.next('.komunikat').text(\"to pole nie może być puste.\").removeClass(\"ok\").addClass(\"blad\");
            }else if(input_length > 10){
                input.removeClass(\"valid\").addClass(\"invalid\");
                input.next('.komunikat').text(\"to pole nie może mieć więcej niż 10 znaków\").removeClass(\"ok\").addClass(\"blad\");
            }else{
                input.removeClass(\"invalid\").addClass(\"valid\");
                input.next('.komunikat').text(\"\").removeClass(\"blad\").addClass(\"ok\");
            }
        });
        
        \$('#fos_user_registration_form_username').on('blur', function(){
            var input = \$(this);
            var input_length = input.val().length;
            if(input_length < 1){
                input.removeClass(\"valid\").addClass(\"invalid\");
                input.next('.komunikat').text(\"to pole nie może być puste.\").removeClass(\"ok\").addClass(\"blad\");
            }else if(input_length > 10){
                input.removeClass(\"valid\").addClass(\"invalid\");
                input.next('.komunikat').text(\"to pole nie może mieć więcej niż 10 znaków\").removeClass(\"ok\").addClass(\"blad\");
            }else{
                input.removeClass(\"invalid\").addClass(\"valid\");
                input.next('.komunikat').text(\"\").removeClass(\"blad\").addClass(\"ok\");
            }
        });
        
        \$('#fos_user_registration_form_plainPassword_first').on('blur', function(){
            var input = \$(this);
            var pattern = /^[a-zA-Z0-9-,\\^=:+<>\\{\\}\\.\\[\\]\\\\/}()_@!#\$%&*?]+\$/;
            var is_pass_valid = pattern.test(input.val());
            var input_length = input.val().length;
            var input_second = \$('#fos_user_registration_form_plainPassword_second');
            if(input_length < 8){
                input.removeClass(\"valid\").addClass(\"invalid\");
                input.next('.komunikat').text(\"to pole nie może mieć mniej niż 8 znaków.\").removeClass(\"ok\").addClass(\"blad\");
            }else if(input_length > 14){
                input.removeClass(\"valid\").addClass(\"invalid\");
                input.next('.komunikat').text(\"to pole nie może mieć więcej niż 14 znaków\").removeClass(\"ok\").addClass(\"blad\");
            }else if(!is_pass_valid){
                input.removeClass(\"valid\").addClass(\"invalid\");
                input.next('.komunikat').text(\"dozwolone znaki alfanumeryczne, specjalne i interpunkcyjne =,:-+<>.[]\\/{}()_@!#\$%^&*?\").removeClass(\"ok\").addClass(\"blad\");

            }else{
                if(input.val() != input_second.val()){
                    input.removeClass(\"valid\").addClass(\"invalid\");
                    input.next('.komunikat').text(\"podane hasła powinny być takie same\").removeClass(\"ok\").addClass(\"blad\");
                    input_second.removeClass(\"valid\").addClass(\"invalid\");
                }else{
                    input.removeClass(\"invalid\").addClass(\"valid\");
                    input.next('.komunikat').text(\"\").removeClass(\"blad\").addClass(\"ok\");
                    input_second.removeClass(\"invalid\").addClass(\"valid\");
                    input_second.next('.komunikat').text(\"\").removeClass(\"blad\").addClass(\"ok\");
                }
            }
        });
        
        \$('#fos_user_registration_form_plainPassword_second').on('blur', function(){
            var input = \$(this);
            var pattern = /^[a-zA-Z0-9-,\\^=:+<>\\{\\}\\.\\[\\]\\\\/}()_@!#\$%&*?]+\$/;
            var is_pass_valid = pattern.test(input.val());
            var input_length = input.val().length;
            var input_first = \$('#fos_user_registration_form_plainPassword_first');
            console.log(input.val()+\" \"+input_first.val());
            if(input_length < 8){
                input.removeClass(\"valid\").addClass(\"invalid\");
                input.next('.komunikat').text(\"to pole nie może mieć mniej niż 8 znaków.\").removeClass(\"ok\").addClass(\"blad\");
            }else if(input_length > 14){
                input.removeClass(\"valid\").addClass(\"invalid\");
                input.next('.komunikat').text(\"to pole nie może mieć więcej niż 14 znaków\").removeClass(\"ok\").addClass(\"blad\");
            }else if(!is_pass_valid){
                input.removeClass(\"valid\").addClass(\"invalid\");
                input.next('.komunikat').text(\"dozwolone znaki alfanumeryczne, specjalne i interpunkcyjne =,:-+<>.[]\\/{}()_@!#\$%^&*?\").removeClass(\"ok\").addClass(\"blad\");
            }else{
                if(input.val() != input_first.val()){
                    input.removeClass(\"valid\").addClass(\"invalid\");
                    input.next('.komunikat').text(\"podane hasła powinny być takie same\").removeClass(\"ok\").addClass(\"blad\");
                    input_first.removeClass(\"valid\").addClass(\"invalid\");
                }else{
                    input.removeClass(\"invalid\").addClass(\"valid\");
                    input.next('.komunikat').text(\"\").removeClass(\"blad\").addClass(\"ok\");
                    input_first.removeClass(\"invalid\").addClass(\"valid\");
                    input_first.next('.komunikat').text(\"\").removeClass(\"blad\").addClass(\"ok\");
                }
            }
        });
        
        \$('#fos_user_registration_form_email').on('blur', function(){
            var input = \$(this);
            var pattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-.]+\\.[a-zA-Z]{2,4}\$/;
            var is_email = pattern.test(input.val());
            var input_length = input.val().length;
            if(input_length < 1){
                input.removeClass(\"valid\").addClass(\"invalid\");
                input.next('.komunikat').text(\"to pole nie może być puste.\").removeClass(\"ok\").addClass(\"blad\");
            }else if(!is_email){
                input.removeClass(\"valid\").addClass(\"invalid\");
                input.next('.komunikat').text(\"to nie jest prawidłowy format adresu email\").removeClass(\"ok\").addClass(\"blad\");
            }else{
                input.removeClass(\"invalid\").addClass(\"valid\");
                input.next('.komunikat').text(\"\").removeClass(\"blad\").addClass(\"ok\");
            }
        });
       
        \$('#form_save').click(function(event){
            var imie = \$('#fos_user_registration_form_imie');
            var nazwisko = \$('#fos_user_registration_form_nazwisko');
            var login = \$('#fos_user_registration_form_username');
            var haslo_first = \$('#fos_user_registration_form_plainPassword_first');
            var haslo_second = \$('#fos_user_registration_form_plainPassword_second');
            var email = \$('#fos_user_registration_form_email');
            
            if(imie.hasClass('valid') && nazwisko.hasClass('valid') && login.hasClass('valid') && 
                haslo_first.hasClass('valid') && haslo_second.hasClass('valid') && email.hasClass('valid')){
                
            }else{
                event.preventDefault();
                \$(this).next('.komunikat_submit').text(\"Wypełnij prawidłowo wszystkie pola\").removeClass(\"ok\").addClass(\"blad\");
            }
        });
    
    });
    </script>
";
        
        $__internal_6270ae80f7c4916df538cb6c9c5b9a3860a08e21500d197bb9e495d73dd35481->leave($__internal_6270ae80f7c4916df538cb6c9c5b9a3860a08e21500d197bb9e495d73dd35481_prof);

        
        $__internal_268a172340296bad60983ac9540dcd42560bbce1adfdbfafd9eca64d57bb91a2->leave($__internal_268a172340296bad60983ac9540dcd42560bbce1adfdbfafd9eca64d57bb91a2_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Registration/register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  69 => 7,  60 => 6,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Registration/register_content.html.twig\" %}
{% endblock fos_user_content %}
{% block javascripts %}
<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js\"></script>
<script>
    
    \$(document).ready(function(){
        
        \$('#fos_user_registration_form_imie').on('blur', function(){
            var input = \$(this);
            var input_length = input.val().length;
            if(input_length < 1){
                input.removeClass(\"valid\").addClass(\"invalid\");
                input.next('.komunikat').text(\"to pole nie może być puste.\").removeClass(\"ok\").addClass(\"blad\");
            }else if(input_length > 10){
                input.removeClass(\"valid\").addClass(\"invalid\");
                input.next('.komunikat').text(\"to pole nie może mieć więcej niż 10 znaków\").removeClass(\"ok\").addClass(\"blad\");
            }else{
                input.removeClass(\"invalid\").addClass(\"valid\");
                input.next('.komunikat').text(\"\").removeClass(\"blad\").addClass(\"ok\");
            }
        });
        
        \$('#fos_user_registration_form_nazwisko').on('blur', function(){
            var input = \$(this);
            var input_length = input.val().length;
            if(input_length < 1){
                input.removeClass(\"valid\").addClass(\"invalid\");
                input.next('.komunikat').text(\"to pole nie może być puste.\").removeClass(\"ok\").addClass(\"blad\");
            }else if(input_length > 10){
                input.removeClass(\"valid\").addClass(\"invalid\");
                input.next('.komunikat').text(\"to pole nie może mieć więcej niż 10 znaków\").removeClass(\"ok\").addClass(\"blad\");
            }else{
                input.removeClass(\"invalid\").addClass(\"valid\");
                input.next('.komunikat').text(\"\").removeClass(\"blad\").addClass(\"ok\");
            }
        });
        
        \$('#fos_user_registration_form_username').on('blur', function(){
            var input = \$(this);
            var input_length = input.val().length;
            if(input_length < 1){
                input.removeClass(\"valid\").addClass(\"invalid\");
                input.next('.komunikat').text(\"to pole nie może być puste.\").removeClass(\"ok\").addClass(\"blad\");
            }else if(input_length > 10){
                input.removeClass(\"valid\").addClass(\"invalid\");
                input.next('.komunikat').text(\"to pole nie może mieć więcej niż 10 znaków\").removeClass(\"ok\").addClass(\"blad\");
            }else{
                input.removeClass(\"invalid\").addClass(\"valid\");
                input.next('.komunikat').text(\"\").removeClass(\"blad\").addClass(\"ok\");
            }
        });
        
        \$('#fos_user_registration_form_plainPassword_first').on('blur', function(){
            var input = \$(this);
            var pattern = /^[a-zA-Z0-9-,\\^=:+<>\\{\\}\\.\\[\\]\\\\/}()_@!#\$%&*?]+\$/;
            var is_pass_valid = pattern.test(input.val());
            var input_length = input.val().length;
            var input_second = \$('#fos_user_registration_form_plainPassword_second');
            if(input_length < 8){
                input.removeClass(\"valid\").addClass(\"invalid\");
                input.next('.komunikat').text(\"to pole nie może mieć mniej niż 8 znaków.\").removeClass(\"ok\").addClass(\"blad\");
            }else if(input_length > 14){
                input.removeClass(\"valid\").addClass(\"invalid\");
                input.next('.komunikat').text(\"to pole nie może mieć więcej niż 14 znaków\").removeClass(\"ok\").addClass(\"blad\");
            }else if(!is_pass_valid){
                input.removeClass(\"valid\").addClass(\"invalid\");
                input.next('.komunikat').text(\"dozwolone znaki alfanumeryczne, specjalne i interpunkcyjne =,:-+<>.[]\\/{}()_@!#\$%^&*?\").removeClass(\"ok\").addClass(\"blad\");

            }else{
                if(input.val() != input_second.val()){
                    input.removeClass(\"valid\").addClass(\"invalid\");
                    input.next('.komunikat').text(\"podane hasła powinny być takie same\").removeClass(\"ok\").addClass(\"blad\");
                    input_second.removeClass(\"valid\").addClass(\"invalid\");
                }else{
                    input.removeClass(\"invalid\").addClass(\"valid\");
                    input.next('.komunikat').text(\"\").removeClass(\"blad\").addClass(\"ok\");
                    input_second.removeClass(\"invalid\").addClass(\"valid\");
                    input_second.next('.komunikat').text(\"\").removeClass(\"blad\").addClass(\"ok\");
                }
            }
        });
        
        \$('#fos_user_registration_form_plainPassword_second').on('blur', function(){
            var input = \$(this);
            var pattern = /^[a-zA-Z0-9-,\\^=:+<>\\{\\}\\.\\[\\]\\\\/}()_@!#\$%&*?]+\$/;
            var is_pass_valid = pattern.test(input.val());
            var input_length = input.val().length;
            var input_first = \$('#fos_user_registration_form_plainPassword_first');
            console.log(input.val()+\" \"+input_first.val());
            if(input_length < 8){
                input.removeClass(\"valid\").addClass(\"invalid\");
                input.next('.komunikat').text(\"to pole nie może mieć mniej niż 8 znaków.\").removeClass(\"ok\").addClass(\"blad\");
            }else if(input_length > 14){
                input.removeClass(\"valid\").addClass(\"invalid\");
                input.next('.komunikat').text(\"to pole nie może mieć więcej niż 14 znaków\").removeClass(\"ok\").addClass(\"blad\");
            }else if(!is_pass_valid){
                input.removeClass(\"valid\").addClass(\"invalid\");
                input.next('.komunikat').text(\"dozwolone znaki alfanumeryczne, specjalne i interpunkcyjne =,:-+<>.[]\\/{}()_@!#\$%^&*?\").removeClass(\"ok\").addClass(\"blad\");
            }else{
                if(input.val() != input_first.val()){
                    input.removeClass(\"valid\").addClass(\"invalid\");
                    input.next('.komunikat').text(\"podane hasła powinny być takie same\").removeClass(\"ok\").addClass(\"blad\");
                    input_first.removeClass(\"valid\").addClass(\"invalid\");
                }else{
                    input.removeClass(\"invalid\").addClass(\"valid\");
                    input.next('.komunikat').text(\"\").removeClass(\"blad\").addClass(\"ok\");
                    input_first.removeClass(\"invalid\").addClass(\"valid\");
                    input_first.next('.komunikat').text(\"\").removeClass(\"blad\").addClass(\"ok\");
                }
            }
        });
        
        \$('#fos_user_registration_form_email').on('blur', function(){
            var input = \$(this);
            var pattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-.]+\\.[a-zA-Z]{2,4}\$/;
            var is_email = pattern.test(input.val());
            var input_length = input.val().length;
            if(input_length < 1){
                input.removeClass(\"valid\").addClass(\"invalid\");
                input.next('.komunikat').text(\"to pole nie może być puste.\").removeClass(\"ok\").addClass(\"blad\");
            }else if(!is_email){
                input.removeClass(\"valid\").addClass(\"invalid\");
                input.next('.komunikat').text(\"to nie jest prawidłowy format adresu email\").removeClass(\"ok\").addClass(\"blad\");
            }else{
                input.removeClass(\"invalid\").addClass(\"valid\");
                input.next('.komunikat').text(\"\").removeClass(\"blad\").addClass(\"ok\");
            }
        });
       
        \$('#form_save').click(function(event){
            var imie = \$('#fos_user_registration_form_imie');
            var nazwisko = \$('#fos_user_registration_form_nazwisko');
            var login = \$('#fos_user_registration_form_username');
            var haslo_first = \$('#fos_user_registration_form_plainPassword_first');
            var haslo_second = \$('#fos_user_registration_form_plainPassword_second');
            var email = \$('#fos_user_registration_form_email');
            
            if(imie.hasClass('valid') && nazwisko.hasClass('valid') && login.hasClass('valid') && 
                haslo_first.hasClass('valid') && haslo_second.hasClass('valid') && email.hasClass('valid')){
                
            }else{
                event.preventDefault();
                \$(this).next('.komunikat_submit').text(\"Wypełnij prawidłowo wszystkie pola\").removeClass(\"ok\").addClass(\"blad\");
            }
        });
    
    });
    </script>
{% endblock %}", "@FOSUser/Registration/register.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\app\\Resources\\FOSUserBundle\\views\\Registration\\register.html.twig");
    }
}
