<?php

/* WebProfilerBundle:Profiler:info.html.twig */
class __TwigTemplate_26b44d7e6b28c60a6a482eb8f80b0c6073ce48d515fbed5c1af7026d8fe693e6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Profiler:info.html.twig", 1);
        $this->blocks = array(
            'summary' => array($this, 'block_summary'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fa9446845b86724fbe210cbfd9c8e2e1644e86afe632d6227bf9f394c64991d9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fa9446845b86724fbe210cbfd9c8e2e1644e86afe632d6227bf9f394c64991d9->enter($__internal_fa9446845b86724fbe210cbfd9c8e2e1644e86afe632d6227bf9f394c64991d9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:info.html.twig"));

        $__internal_c6459c40419951256d8dfc089b2c7dc66b7dfe02b1d252ceff8003e2edd6595d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c6459c40419951256d8dfc089b2c7dc66b7dfe02b1d252ceff8003e2edd6595d->enter($__internal_c6459c40419951256d8dfc089b2c7dc66b7dfe02b1d252ceff8003e2edd6595d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:info.html.twig"));

        // line 3
        $context["messages"] = array("no_token" => array("status" => "error", "title" => (((((        // line 6
array_key_exists("token", $context)) ? (_twig_default_filter(($context["token"] ?? $this->getContext($context, "token")), "")) : ("")) == "latest")) ? ("There are no profiles") : ("Token not found")), "message" => (((((        // line 7
array_key_exists("token", $context)) ? (_twig_default_filter(($context["token"] ?? $this->getContext($context, "token")), "")) : ("")) == "latest")) ? ("No profiles found in the database.") : ((("Token \"" . ((array_key_exists("token", $context)) ? (_twig_default_filter(($context["token"] ?? $this->getContext($context, "token")), "")) : (""))) . "\" was not found in the database.")))));
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_fa9446845b86724fbe210cbfd9c8e2e1644e86afe632d6227bf9f394c64991d9->leave($__internal_fa9446845b86724fbe210cbfd9c8e2e1644e86afe632d6227bf9f394c64991d9_prof);

        
        $__internal_c6459c40419951256d8dfc089b2c7dc66b7dfe02b1d252ceff8003e2edd6595d->leave($__internal_c6459c40419951256d8dfc089b2c7dc66b7dfe02b1d252ceff8003e2edd6595d_prof);

    }

    // line 11
    public function block_summary($context, array $blocks = array())
    {
        $__internal_3e64931b2598836364ca10a6a8a286db596f754ca1a085ba27ae4c1ea5e30bcd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3e64931b2598836364ca10a6a8a286db596f754ca1a085ba27ae4c1ea5e30bcd->enter($__internal_3e64931b2598836364ca10a6a8a286db596f754ca1a085ba27ae4c1ea5e30bcd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "summary"));

        $__internal_19a3dbd2402cb1f7f4de9be337fdbe99e07a3c2b20c6fa6c3b0bf225216d9c89 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_19a3dbd2402cb1f7f4de9be337fdbe99e07a3c2b20c6fa6c3b0bf225216d9c89->enter($__internal_19a3dbd2402cb1f7f4de9be337fdbe99e07a3c2b20c6fa6c3b0bf225216d9c89_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "summary"));

        // line 12
        echo "    <div class=\"status status-";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "status", array()), "html", null, true);
        echo "\">
        <div class=\"container\">
            <h2>";
        // line 14
        echo twig_escape_filter($this->env, twig_title_string_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "status", array())), "html", null, true);
        echo "</h2>
        </div>
    </div>
";
        
        $__internal_19a3dbd2402cb1f7f4de9be337fdbe99e07a3c2b20c6fa6c3b0bf225216d9c89->leave($__internal_19a3dbd2402cb1f7f4de9be337fdbe99e07a3c2b20c6fa6c3b0bf225216d9c89_prof);

        
        $__internal_3e64931b2598836364ca10a6a8a286db596f754ca1a085ba27ae4c1ea5e30bcd->leave($__internal_3e64931b2598836364ca10a6a8a286db596f754ca1a085ba27ae4c1ea5e30bcd_prof);

    }

    // line 19
    public function block_panel($context, array $blocks = array())
    {
        $__internal_17da40009d1cd6beaba1925c5744c9ffa3978c8872e4b043ced1b16576baf887 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_17da40009d1cd6beaba1925c5744c9ffa3978c8872e4b043ced1b16576baf887->enter($__internal_17da40009d1cd6beaba1925c5744c9ffa3978c8872e4b043ced1b16576baf887_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_1e44d4c1751618815b719e99210aeede2e452194c66c793eaed7e459f69f65a7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1e44d4c1751618815b719e99210aeede2e452194c66c793eaed7e459f69f65a7->enter($__internal_1e44d4c1751618815b719e99210aeede2e452194c66c793eaed7e459f69f65a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 20
        echo "    <h2>";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "title", array()), "html", null, true);
        echo "</h2>
    <p>";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "message", array()), "html", null, true);
        echo "</p>
";
        
        $__internal_1e44d4c1751618815b719e99210aeede2e452194c66c793eaed7e459f69f65a7->leave($__internal_1e44d4c1751618815b719e99210aeede2e452194c66c793eaed7e459f69f65a7_prof);

        
        $__internal_17da40009d1cd6beaba1925c5744c9ffa3978c8872e4b043ced1b16576baf887->leave($__internal_17da40009d1cd6beaba1925c5744c9ffa3978c8872e4b043ced1b16576baf887_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:info.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 21,  84 => 20,  75 => 19,  61 => 14,  55 => 12,  46 => 11,  36 => 1,  34 => 7,  33 => 6,  32 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% set messages = {
    'no_token' : {
        status:  'error',
        title:   (token|default('') == 'latest') ? 'There are no profiles' : 'Token not found',
        message: (token|default('') == 'latest') ? 'No profiles found in the database.' : 'Token \"' ~ token|default('') ~ '\" was not found in the database.'
    }
} %}

{% block summary %}
    <div class=\"status status-{{ messages[about].status }}\">
        <div class=\"container\">
            <h2>{{ messages[about].status|title }}</h2>
        </div>
    </div>
{% endblock %}

{% block panel %}
    <h2>{{ messages[about].title }}</h2>
    <p>{{ messages[about].message }}</p>
{% endblock %}
", "WebProfilerBundle:Profiler:info.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle/Resources/views/Profiler/info.html.twig");
    }
}
