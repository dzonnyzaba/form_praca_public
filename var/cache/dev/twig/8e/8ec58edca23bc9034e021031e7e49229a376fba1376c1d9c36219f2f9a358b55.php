<?php

/* @FOSUser/layout.html.twig */
class __TwigTemplate_906e121ca2b51e14af30f3fa6f655c55c0074ef416fb9b669033d2be6938ac4d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "@FOSUser/layout.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9c0cdba0e15a95275cf157f1a7a019493cd58e046920cd7b1db25e8705d9b9c4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9c0cdba0e15a95275cf157f1a7a019493cd58e046920cd7b1db25e8705d9b9c4->enter($__internal_9c0cdba0e15a95275cf157f1a7a019493cd58e046920cd7b1db25e8705d9b9c4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/layout.html.twig"));

        $__internal_051d304c589cf3f30e73267a201391da42d4eceeb2b57ee140c3ded123c5532b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_051d304c589cf3f30e73267a201391da42d4eceeb2b57ee140c3ded123c5532b->enter($__internal_051d304c589cf3f30e73267a201391da42d4eceeb2b57ee140c3ded123c5532b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9c0cdba0e15a95275cf157f1a7a019493cd58e046920cd7b1db25e8705d9b9c4->leave($__internal_9c0cdba0e15a95275cf157f1a7a019493cd58e046920cd7b1db25e8705d9b9c4_prof);

        
        $__internal_051d304c589cf3f30e73267a201391da42d4eceeb2b57ee140c3ded123c5532b->leave($__internal_051d304c589cf3f30e73267a201391da42d4eceeb2b57ee140c3ded123c5532b_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_aac1eaa4f444a57357b15067677260929e1debb464188a170bee1bbdfc0e9d32 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_aac1eaa4f444a57357b15067677260929e1debb464188a170bee1bbdfc0e9d32->enter($__internal_aac1eaa4f444a57357b15067677260929e1debb464188a170bee1bbdfc0e9d32_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_9a484eb8d575031bbe3801a3cadcd7c19d5cc177298accf988f9fd9ab319197e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9a484eb8d575031bbe3801a3cadcd7c19d5cc177298accf988f9fd9ab319197e->enter($__internal_9a484eb8d575031bbe3801a3cadcd7c19d5cc177298accf988f9fd9ab319197e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "

";
        // line 6
        if ($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "hasPreviousSession", array())) {
            // line 7
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "session", array()), "flashbag", array()), "all", array(), "method"));
            foreach ($context['_seq'] as $context["type"] => $context["messages"]) {
                // line 8
                echo "        ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($context["messages"]);
                foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                    // line 9
                    echo "            <div class=\"flash-";
                    echo twig_escape_filter($this->env, $context["type"], "html", null, true);
                    echo "\">
                ";
                    // line 10
                    echo twig_escape_filter($this->env, $context["message"], "html", null, true);
                    echo "
            </div>
        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 13
                echo "    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['type'], $context['messages'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        // line 15
        echo "
<div>
    ";
        // line 17
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 19
        echo "</div>

";
        
        $__internal_9a484eb8d575031bbe3801a3cadcd7c19d5cc177298accf988f9fd9ab319197e->leave($__internal_9a484eb8d575031bbe3801a3cadcd7c19d5cc177298accf988f9fd9ab319197e_prof);

        
        $__internal_aac1eaa4f444a57357b15067677260929e1debb464188a170bee1bbdfc0e9d32->leave($__internal_aac1eaa4f444a57357b15067677260929e1debb464188a170bee1bbdfc0e9d32_prof);

    }

    // line 17
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_d6f331166245a8f169a4efc2c371ffe95b45adcc1909f0e53fbfe4649b8623d9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d6f331166245a8f169a4efc2c371ffe95b45adcc1909f0e53fbfe4649b8623d9->enter($__internal_d6f331166245a8f169a4efc2c371ffe95b45adcc1909f0e53fbfe4649b8623d9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_1c8ed82b2d7c1721c3f07a65138b3acfcf84007a00a52b0707825ad60e7856ca = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1c8ed82b2d7c1721c3f07a65138b3acfcf84007a00a52b0707825ad60e7856ca->enter($__internal_1c8ed82b2d7c1721c3f07a65138b3acfcf84007a00a52b0707825ad60e7856ca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 18
        echo "    ";
        
        $__internal_1c8ed82b2d7c1721c3f07a65138b3acfcf84007a00a52b0707825ad60e7856ca->leave($__internal_1c8ed82b2d7c1721c3f07a65138b3acfcf84007a00a52b0707825ad60e7856ca_prof);

        
        $__internal_d6f331166245a8f169a4efc2c371ffe95b45adcc1909f0e53fbfe4649b8623d9->leave($__internal_d6f331166245a8f169a4efc2c371ffe95b45adcc1909f0e53fbfe4649b8623d9_prof);

    }

    // line 22
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_de8160ae62eba0fc5488252c6a3df9a5c3c4de465ba2cca3a8a758c6376bd553 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_de8160ae62eba0fc5488252c6a3df9a5c3c4de465ba2cca3a8a758c6376bd553->enter($__internal_de8160ae62eba0fc5488252c6a3df9a5c3c4de465ba2cca3a8a758c6376bd553_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_321132a8f2a8d83a996fb36ef6214251284e92fa63c317f192e33486f00c743a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_321132a8f2a8d83a996fb36ef6214251284e92fa63c317f192e33486f00c743a->enter($__internal_321132a8f2a8d83a996fb36ef6214251284e92fa63c317f192e33486f00c743a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_321132a8f2a8d83a996fb36ef6214251284e92fa63c317f192e33486f00c743a->leave($__internal_321132a8f2a8d83a996fb36ef6214251284e92fa63c317f192e33486f00c743a_prof);

        
        $__internal_de8160ae62eba0fc5488252c6a3df9a5c3c4de465ba2cca3a8a758c6376bd553->leave($__internal_de8160ae62eba0fc5488252c6a3df9a5c3c4de465ba2cca3a8a758c6376bd553_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  125 => 22,  115 => 18,  106 => 17,  94 => 19,  92 => 17,  88 => 15,  81 => 13,  72 => 10,  67 => 9,  62 => 8,  57 => 7,  55 => 6,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '::base.html.twig' %}

{% block body %}


{% if app.request.hasPreviousSession %}
    {% for type, messages in app.session.flashbag.all() %}
        {% for message in messages %}
            <div class=\"flash-{{ type }}\">
                {{ message }}
            </div>
        {% endfor %}
    {% endfor %}
{% endif %}

<div>
    {% block fos_user_content %}
    {% endblock fos_user_content %}
</div>

{% endblock %}
{% block javascripts %}{% endblock %}
", "@FOSUser/layout.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\app\\Resources\\FOSUserBundle\\views\\layout.html.twig");
    }
}
