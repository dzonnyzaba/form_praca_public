<?php

/* @WebProfiler/Profiler/open.html.twig */
class __TwigTemplate_ac12c574132227140b686f2370e565f8f8810500cd69524bf391681d60e32a52 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/base.html.twig", "@WebProfiler/Profiler/open.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7e40614ba74b75b064425dfbbf5657643f32e6d784a4441ec807b45bd794608a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7e40614ba74b75b064425dfbbf5657643f32e6d784a4441ec807b45bd794608a->enter($__internal_7e40614ba74b75b064425dfbbf5657643f32e6d784a4441ec807b45bd794608a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/open.html.twig"));

        $__internal_8320aa089cfc21a18ff38e4076322b237d95be9f4c683efcfb0dda2e0af4ce53 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8320aa089cfc21a18ff38e4076322b237d95be9f4c683efcfb0dda2e0af4ce53->enter($__internal_8320aa089cfc21a18ff38e4076322b237d95be9f4c683efcfb0dda2e0af4ce53_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/open.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7e40614ba74b75b064425dfbbf5657643f32e6d784a4441ec807b45bd794608a->leave($__internal_7e40614ba74b75b064425dfbbf5657643f32e6d784a4441ec807b45bd794608a_prof);

        
        $__internal_8320aa089cfc21a18ff38e4076322b237d95be9f4c683efcfb0dda2e0af4ce53->leave($__internal_8320aa089cfc21a18ff38e4076322b237d95be9f4c683efcfb0dda2e0af4ce53_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_3312665e4c4c7af90e32b51736b19d84b3b888a83c36f4ee138f314f64ef681a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3312665e4c4c7af90e32b51736b19d84b3b888a83c36f4ee138f314f64ef681a->enter($__internal_3312665e4c4c7af90e32b51736b19d84b3b888a83c36f4ee138f314f64ef681a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_450f78d0bf014ed79c361f152d21a23285c45368e21b1e30aad29beb9eaf436b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_450f78d0bf014ed79c361f152d21a23285c45368e21b1e30aad29beb9eaf436b->enter($__internal_450f78d0bf014ed79c361f152d21a23285c45368e21b1e30aad29beb9eaf436b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        ";
        // line 5
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/open.css.twig");
        echo "
    </style>
";
        
        $__internal_450f78d0bf014ed79c361f152d21a23285c45368e21b1e30aad29beb9eaf436b->leave($__internal_450f78d0bf014ed79c361f152d21a23285c45368e21b1e30aad29beb9eaf436b_prof);

        
        $__internal_3312665e4c4c7af90e32b51736b19d84b3b888a83c36f4ee138f314f64ef681a->leave($__internal_3312665e4c4c7af90e32b51736b19d84b3b888a83c36f4ee138f314f64ef681a_prof);

    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        $__internal_ebd7e230930c075d04ab0890a7f06050738a05f4c8b0007e0f20dda09af1bbdb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ebd7e230930c075d04ab0890a7f06050738a05f4c8b0007e0f20dda09af1bbdb->enter($__internal_ebd7e230930c075d04ab0890a7f06050738a05f4c8b0007e0f20dda09af1bbdb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_12affd1af4be340a3cda64050868f460de50b081ed2d1dc5e81b30ab74145fa6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_12affd1af4be340a3cda64050868f460de50b081ed2d1dc5e81b30ab74145fa6->enter($__internal_12affd1af4be340a3cda64050868f460de50b081ed2d1dc5e81b30ab74145fa6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "<div class=\"header\">
    <h1>";
        // line 11
        echo twig_escape_filter($this->env, ($context["file"] ?? $this->getContext($context, "file")), "html", null, true);
        echo " <small>line ";
        echo twig_escape_filter($this->env, ($context["line"] ?? $this->getContext($context, "line")), "html", null, true);
        echo "</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/";
        // line 12
        echo twig_escape_filter($this->env, twig_constant("Symfony\\Component\\HttpKernel\\Kernel::VERSION"), "html", null, true);
        echo "/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    ";
        // line 15
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->fileExcerpt(($context["filename"] ?? $this->getContext($context, "filename")), ($context["line"] ?? $this->getContext($context, "line")),  -1);
        echo "
</div>
";
        
        $__internal_12affd1af4be340a3cda64050868f460de50b081ed2d1dc5e81b30ab74145fa6->leave($__internal_12affd1af4be340a3cda64050868f460de50b081ed2d1dc5e81b30ab74145fa6_prof);

        
        $__internal_ebd7e230930c075d04ab0890a7f06050738a05f4c8b0007e0f20dda09af1bbdb->leave($__internal_ebd7e230930c075d04ab0890a7f06050738a05f4c8b0007e0f20dda09af1bbdb_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/open.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 15,  84 => 12,  78 => 11,  75 => 10,  66 => 9,  53 => 5,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/base.html.twig' %}

{% block head %}
    <style>
        {{ include('@WebProfiler/Profiler/open.css.twig') }}
    </style>
{% endblock %}

{% block body %}
<div class=\"header\">
    <h1>{{ file }} <small>line {{ line }}</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/{{ constant('Symfony\\\\Component\\\\HttpKernel\\\\Kernel::VERSION') }}/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    {{ filename|file_excerpt(line, -1) }}
</div>
{% endblock %}
", "@WebProfiler/Profiler/open.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Profiler\\open.html.twig");
    }
}
