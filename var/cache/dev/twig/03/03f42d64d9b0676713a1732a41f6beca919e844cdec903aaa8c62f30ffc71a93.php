<?php

/* @WebProfiler/Profiler/ajax_layout.html.twig */
class __TwigTemplate_d820b52e09141a15cfa4ddd0827a37effef652934e33db8a7b5ff8de159bc968 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3589abfc6447672d223c9ca257ec04bad5c2db47a2736c7a0974dd778e6d09de = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3589abfc6447672d223c9ca257ec04bad5c2db47a2736c7a0974dd778e6d09de->enter($__internal_3589abfc6447672d223c9ca257ec04bad5c2db47a2736c7a0974dd778e6d09de_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/ajax_layout.html.twig"));

        $__internal_b27e4d2dee9e63a8eb2733995f28310548d01801a3b7d2e3a14e4f3b24d26916 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b27e4d2dee9e63a8eb2733995f28310548d01801a3b7d2e3a14e4f3b24d26916->enter($__internal_b27e4d2dee9e63a8eb2733995f28310548d01801a3b7d2e3a14e4f3b24d26916_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_3589abfc6447672d223c9ca257ec04bad5c2db47a2736c7a0974dd778e6d09de->leave($__internal_3589abfc6447672d223c9ca257ec04bad5c2db47a2736c7a0974dd778e6d09de_prof);

        
        $__internal_b27e4d2dee9e63a8eb2733995f28310548d01801a3b7d2e3a14e4f3b24d26916->leave($__internal_b27e4d2dee9e63a8eb2733995f28310548d01801a3b7d2e3a14e4f3b24d26916_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_e590a61194bd7cfa0b7d1193c5ef0bce4bdeaf7329e1ca6e6a764fa1302df2e4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e590a61194bd7cfa0b7d1193c5ef0bce4bdeaf7329e1ca6e6a764fa1302df2e4->enter($__internal_e590a61194bd7cfa0b7d1193c5ef0bce4bdeaf7329e1ca6e6a764fa1302df2e4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_8c8282f2759032d8d3d9c581f15261079d6d2072b11a09f8f57bf2a135948072 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8c8282f2759032d8d3d9c581f15261079d6d2072b11a09f8f57bf2a135948072->enter($__internal_8c8282f2759032d8d3d9c581f15261079d6d2072b11a09f8f57bf2a135948072_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_8c8282f2759032d8d3d9c581f15261079d6d2072b11a09f8f57bf2a135948072->leave($__internal_8c8282f2759032d8d3d9c581f15261079d6d2072b11a09f8f57bf2a135948072_prof);

        
        $__internal_e590a61194bd7cfa0b7d1193c5ef0bce4bdeaf7329e1ca6e6a764fa1302df2e4->leave($__internal_e590a61194bd7cfa0b7d1193c5ef0bce4bdeaf7329e1ca6e6a764fa1302df2e4_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% block panel '' %}
", "@WebProfiler/Profiler/ajax_layout.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Profiler\\ajax_layout.html.twig");
    }
}
