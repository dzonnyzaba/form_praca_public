<?php

/* @Framework/Form/url_widget.html.php */
class __TwigTemplate_6d6c06b96e364367f395c82483451daac5fae51592a7cdd84884e1d19a39a9a5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9a501a3047837b896fde34dfd978561929e908c91633e8d5247542e4480e6b0d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9a501a3047837b896fde34dfd978561929e908c91633e8d5247542e4480e6b0d->enter($__internal_9a501a3047837b896fde34dfd978561929e908c91633e8d5247542e4480e6b0d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        $__internal_e101bfbfb2d76048051506f46a8492302d38d754506e2b3c83c58b0806b63aea = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e101bfbfb2d76048051506f46a8492302d38d754506e2b3c83c58b0806b63aea->enter($__internal_e101bfbfb2d76048051506f46a8492302d38d754506e2b3c83c58b0806b63aea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'url')) ?>
";
        
        $__internal_9a501a3047837b896fde34dfd978561929e908c91633e8d5247542e4480e6b0d->leave($__internal_9a501a3047837b896fde34dfd978561929e908c91633e8d5247542e4480e6b0d_prof);

        
        $__internal_e101bfbfb2d76048051506f46a8492302d38d754506e2b3c83c58b0806b63aea->leave($__internal_e101bfbfb2d76048051506f46a8492302d38d754506e2b3c83c58b0806b63aea_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/url_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'url')) ?>
", "@Framework/Form/url_widget.html.php", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\url_widget.html.php");
    }
}
