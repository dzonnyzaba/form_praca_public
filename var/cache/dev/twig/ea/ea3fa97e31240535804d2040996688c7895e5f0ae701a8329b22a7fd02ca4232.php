<?php

/* @FOSUser/Group/new.html.twig */
class __TwigTemplate_518dfe9ac541d13a99cf4edb625eb89e6cb519b8aae5755467dae980e686f171 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Group/new.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_842af7c1db29c68cbfd077e45b6a36d5c23488c238a6558d0980b712ef294231 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_842af7c1db29c68cbfd077e45b6a36d5c23488c238a6558d0980b712ef294231->enter($__internal_842af7c1db29c68cbfd077e45b6a36d5c23488c238a6558d0980b712ef294231_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Group/new.html.twig"));

        $__internal_aeaa99bc083e4ee5ba52a20bff9ff8610663be7fdedfe4f11fc9fc2a7bd9c28e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_aeaa99bc083e4ee5ba52a20bff9ff8610663be7fdedfe4f11fc9fc2a7bd9c28e->enter($__internal_aeaa99bc083e4ee5ba52a20bff9ff8610663be7fdedfe4f11fc9fc2a7bd9c28e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Group/new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_842af7c1db29c68cbfd077e45b6a36d5c23488c238a6558d0980b712ef294231->leave($__internal_842af7c1db29c68cbfd077e45b6a36d5c23488c238a6558d0980b712ef294231_prof);

        
        $__internal_aeaa99bc083e4ee5ba52a20bff9ff8610663be7fdedfe4f11fc9fc2a7bd9c28e->leave($__internal_aeaa99bc083e4ee5ba52a20bff9ff8610663be7fdedfe4f11fc9fc2a7bd9c28e_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_869bd5948b924d60226f6257bff632f009285499498d73e57968268cee3bb3bb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_869bd5948b924d60226f6257bff632f009285499498d73e57968268cee3bb3bb->enter($__internal_869bd5948b924d60226f6257bff632f009285499498d73e57968268cee3bb3bb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_19de17d6c3bfadf784621500146715ef4ef311e68777cdc711201ddd313816dd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_19de17d6c3bfadf784621500146715ef4ef311e68777cdc711201ddd313816dd->enter($__internal_19de17d6c3bfadf784621500146715ef4ef311e68777cdc711201ddd313816dd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/new_content.html.twig", "@FOSUser/Group/new.html.twig", 4)->display($context);
        
        $__internal_19de17d6c3bfadf784621500146715ef4ef311e68777cdc711201ddd313816dd->leave($__internal_19de17d6c3bfadf784621500146715ef4ef311e68777cdc711201ddd313816dd_prof);

        
        $__internal_869bd5948b924d60226f6257bff632f009285499498d73e57968268cee3bb3bb->leave($__internal_869bd5948b924d60226f6257bff632f009285499498d73e57968268cee3bb3bb_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Group/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/new_content.html.twig\" %}
{% endblock fos_user_content %}
", "@FOSUser/Group/new.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\app\\Resources\\FOSUserBundle\\views\\Group\\new.html.twig");
    }
}
