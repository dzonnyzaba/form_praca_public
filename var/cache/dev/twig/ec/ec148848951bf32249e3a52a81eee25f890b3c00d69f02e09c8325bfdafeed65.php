<?php

/* @Framework/Form/integer_widget.html.php */
class __TwigTemplate_4ed3b32a2c8f1e1cb568fdee9d3edf3099294f4695be017a3bbb6b97b328ba4e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a5a500167933cb8bb7984a132eb2a41d08ac80f042cab21d97164516995e34b3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a5a500167933cb8bb7984a132eb2a41d08ac80f042cab21d97164516995e34b3->enter($__internal_a5a500167933cb8bb7984a132eb2a41d08ac80f042cab21d97164516995e34b3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        $__internal_96d3c12e9c2e22773012abb384be2e64a926519b329c754770248e91020e6235 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_96d3c12e9c2e22773012abb384be2e64a926519b329c754770248e91020e6235->enter($__internal_96d3c12e9c2e22773012abb384be2e64a926519b329c754770248e91020e6235_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
";
        
        $__internal_a5a500167933cb8bb7984a132eb2a41d08ac80f042cab21d97164516995e34b3->leave($__internal_a5a500167933cb8bb7984a132eb2a41d08ac80f042cab21d97164516995e34b3_prof);

        
        $__internal_96d3c12e9c2e22773012abb384be2e64a926519b329c754770248e91020e6235->leave($__internal_96d3c12e9c2e22773012abb384be2e64a926519b329c754770248e91020e6235_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/integer_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
", "@Framework/Form/integer_widget.html.php", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\integer_widget.html.php");
    }
}
