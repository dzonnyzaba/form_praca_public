<?php

/* FOSUserBundle:Security:login.html.twig */
class __TwigTemplate_a8766f8ad97ba714943c19ae7e83ef34ee29f5de36831996a90851cab62a26d0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Security:login.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_451426fafc4fc0935a38baaea684db9b448f70c84e81b91466ed00f5505e4d98 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_451426fafc4fc0935a38baaea684db9b448f70c84e81b91466ed00f5505e4d98->enter($__internal_451426fafc4fc0935a38baaea684db9b448f70c84e81b91466ed00f5505e4d98_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Security:login.html.twig"));

        $__internal_3dd7e02ce373b5816561a33bef37f3b1da72b8d8bc1c6c9f8d1ff5b64e8bf292 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3dd7e02ce373b5816561a33bef37f3b1da72b8d8bc1c6c9f8d1ff5b64e8bf292->enter($__internal_3dd7e02ce373b5816561a33bef37f3b1da72b8d8bc1c6c9f8d1ff5b64e8bf292_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Security:login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_451426fafc4fc0935a38baaea684db9b448f70c84e81b91466ed00f5505e4d98->leave($__internal_451426fafc4fc0935a38baaea684db9b448f70c84e81b91466ed00f5505e4d98_prof);

        
        $__internal_3dd7e02ce373b5816561a33bef37f3b1da72b8d8bc1c6c9f8d1ff5b64e8bf292->leave($__internal_3dd7e02ce373b5816561a33bef37f3b1da72b8d8bc1c6c9f8d1ff5b64e8bf292_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_349a802f573dd11dca1e88dbf4119894aa7599f16e4bbe8937094b12202d0849 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_349a802f573dd11dca1e88dbf4119894aa7599f16e4bbe8937094b12202d0849->enter($__internal_349a802f573dd11dca1e88dbf4119894aa7599f16e4bbe8937094b12202d0849_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_693531a3e5910549967fa6b50b53df5bef00a801818e9043e937a3dc326ed548 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_693531a3e5910549967fa6b50b53df5bef00a801818e9043e937a3dc326ed548->enter($__internal_693531a3e5910549967fa6b50b53df5bef00a801818e9043e937a3dc326ed548_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        echo "    ";
        echo twig_include($this->env, $context, "@FOSUser/Security/login_content.html.twig");
        echo "
";
        
        $__internal_693531a3e5910549967fa6b50b53df5bef00a801818e9043e937a3dc326ed548->leave($__internal_693531a3e5910549967fa6b50b53df5bef00a801818e9043e937a3dc326ed548_prof);

        
        $__internal_349a802f573dd11dca1e88dbf4119894aa7599f16e4bbe8937094b12202d0849->leave($__internal_349a802f573dd11dca1e88dbf4119894aa7599f16e4bbe8937094b12202d0849_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Security:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
    {{ include('@FOSUser/Security/login_content.html.twig') }}
{% endblock fos_user_content %}
", "FOSUserBundle:Security:login.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\app/Resources/FOSUserBundle/views/Security/login.html.twig");
    }
}
