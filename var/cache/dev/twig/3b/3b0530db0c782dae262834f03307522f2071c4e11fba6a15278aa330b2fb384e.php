<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_f32ad73c9bc804e7d7685ce925e27ce9c7daa98efc0b9a8c89227d7076a75563 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ac99b3a67a05bd3544ea259486654e5d0095c24a81a7f2acf67ed3136bd69ee4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ac99b3a67a05bd3544ea259486654e5d0095c24a81a7f2acf67ed3136bd69ee4->enter($__internal_ac99b3a67a05bd3544ea259486654e5d0095c24a81a7f2acf67ed3136bd69ee4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $__internal_ceded45acf54649631e6149349263b1f1a1218a2d5872cdbd320f15291b72b3f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ceded45acf54649631e6149349263b1f1a1218a2d5872cdbd320f15291b72b3f->enter($__internal_ceded45acf54649631e6149349263b1f1a1218a2d5872cdbd320f15291b72b3f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ac99b3a67a05bd3544ea259486654e5d0095c24a81a7f2acf67ed3136bd69ee4->leave($__internal_ac99b3a67a05bd3544ea259486654e5d0095c24a81a7f2acf67ed3136bd69ee4_prof);

        
        $__internal_ceded45acf54649631e6149349263b1f1a1218a2d5872cdbd320f15291b72b3f->leave($__internal_ceded45acf54649631e6149349263b1f1a1218a2d5872cdbd320f15291b72b3f_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_777d362e546f86ed3ce56ace6695ff428cf8b927d6857c88f6ebba79debd1bf1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_777d362e546f86ed3ce56ace6695ff428cf8b927d6857c88f6ebba79debd1bf1->enter($__internal_777d362e546f86ed3ce56ace6695ff428cf8b927d6857c88f6ebba79debd1bf1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_b5273f49ff42e77a60a27d268aac7d57050242b1016e10e47a86e67ff8aa69a5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b5273f49ff42e77a60a27d268aac7d57050242b1016e10e47a86e67ff8aa69a5->enter($__internal_b5273f49ff42e77a60a27d268aac7d57050242b1016e10e47a86e67ff8aa69a5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_b5273f49ff42e77a60a27d268aac7d57050242b1016e10e47a86e67ff8aa69a5->leave($__internal_b5273f49ff42e77a60a27d268aac7d57050242b1016e10e47a86e67ff8aa69a5_prof);

        
        $__internal_777d362e546f86ed3ce56ace6695ff428cf8b927d6857c88f6ebba79debd1bf1->leave($__internal_777d362e546f86ed3ce56ace6695ff428cf8b927d6857c88f6ebba79debd1bf1_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_492fa742b8eef3a401be63913e86e88dafc5975f515397ce996fb2c68ccc4f01 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_492fa742b8eef3a401be63913e86e88dafc5975f515397ce996fb2c68ccc4f01->enter($__internal_492fa742b8eef3a401be63913e86e88dafc5975f515397ce996fb2c68ccc4f01_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_23db1bc01e897e57956a92815b32c9243dc82ca4bf5a103feca6955ee64eaf95 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_23db1bc01e897e57956a92815b32c9243dc82ca4bf5a103feca6955ee64eaf95->enter($__internal_23db1bc01e897e57956a92815b32c9243dc82ca4bf5a103feca6955ee64eaf95_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_23db1bc01e897e57956a92815b32c9243dc82ca4bf5a103feca6955ee64eaf95->leave($__internal_23db1bc01e897e57956a92815b32c9243dc82ca4bf5a103feca6955ee64eaf95_prof);

        
        $__internal_492fa742b8eef3a401be63913e86e88dafc5975f515397ce996fb2c68ccc4f01->leave($__internal_492fa742b8eef3a401be63913e86e88dafc5975f515397ce996fb2c68ccc4f01_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_5f09310e1e1e7cdb5118eef29aa72c3d037a4f4e32a0f344fbbdf55e34dab946 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5f09310e1e1e7cdb5118eef29aa72c3d037a4f4e32a0f344fbbdf55e34dab946->enter($__internal_5f09310e1e1e7cdb5118eef29aa72c3d037a4f4e32a0f344fbbdf55e34dab946_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_86cdafc1c22b5a36e4ffe1a3df8aa39ba69c77199c8695a2be75c1f918baf7bd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_86cdafc1c22b5a36e4ffe1a3df8aa39ba69c77199c8695a2be75c1f918baf7bd->enter($__internal_86cdafc1c22b5a36e4ffe1a3df8aa39ba69c77199c8695a2be75c1f918baf7bd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_86cdafc1c22b5a36e4ffe1a3df8aa39ba69c77199c8695a2be75c1f918baf7bd->leave($__internal_86cdafc1c22b5a36e4ffe1a3df8aa39ba69c77199c8695a2be75c1f918baf7bd_prof);

        
        $__internal_5f09310e1e1e7cdb5118eef29aa72c3d037a4f4e32a0f344fbbdf55e34dab946->leave($__internal_5f09310e1e1e7cdb5118eef29aa72c3d037a4f4e32a0f344fbbdf55e34dab946_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "@WebProfiler/Collector/exception.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\exception.html.twig");
    }
}
