<?php

/* @Framework/Form/textarea_widget.html.php */
class __TwigTemplate_4b7edc73be1df84354e2b6fcc211065fce6f5b9fedf694944c129d535991b375 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_33ad5a2999200d85eed157004ce5991ccac7db04a7a2778515e7dc91016f2482 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_33ad5a2999200d85eed157004ce5991ccac7db04a7a2778515e7dc91016f2482->enter($__internal_33ad5a2999200d85eed157004ce5991ccac7db04a7a2778515e7dc91016f2482_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        $__internal_5ea5cc9d55724acc35444ca9df42659d8780bcd038604b637e2c3c6a1b6cb105 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5ea5cc9d55724acc35444ca9df42659d8780bcd038604b637e2c3c6a1b6cb105->enter($__internal_5ea5cc9d55724acc35444ca9df42659d8780bcd038604b637e2c3c6a1b6cb105_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        // line 1
        echo "<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
";
        
        $__internal_33ad5a2999200d85eed157004ce5991ccac7db04a7a2778515e7dc91016f2482->leave($__internal_33ad5a2999200d85eed157004ce5991ccac7db04a7a2778515e7dc91016f2482_prof);

        
        $__internal_5ea5cc9d55724acc35444ca9df42659d8780bcd038604b637e2c3c6a1b6cb105->leave($__internal_5ea5cc9d55724acc35444ca9df42659d8780bcd038604b637e2c3c6a1b6cb105_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/textarea_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
", "@Framework/Form/textarea_widget.html.php", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\textarea_widget.html.php");
    }
}
