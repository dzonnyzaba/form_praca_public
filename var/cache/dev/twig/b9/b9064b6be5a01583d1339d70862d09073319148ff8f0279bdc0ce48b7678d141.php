<?php

/* TwigBundle:Exception:exception.css.twig */
class __TwigTemplate_196fc751e8eff37053a59b16d65991cef582337b190b691419e76d75fd72247f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e578995dfa6b1a5a119347598f217a1e36a546af0adcd11a03a4967a2ce114e8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e578995dfa6b1a5a119347598f217a1e36a546af0adcd11a03a4967a2ce114e8->enter($__internal_e578995dfa6b1a5a119347598f217a1e36a546af0adcd11a03a4967a2ce114e8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.css.twig"));

        $__internal_af11b0d9ec05417a501cc0995d2171a3f15eb68ae158b9a0dd52fe95c46f4a12 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_af11b0d9ec05417a501cc0995d2171a3f15eb68ae158b9a0dd52fe95c46f4a12->enter($__internal_af11b0d9ec05417a501cc0995d2171a3f15eb68ae158b9a0dd52fe95c46f4a12_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_include($this->env, $context, "@Twig/Exception/exception.txt.twig", array("exception" => ($context["exception"] ?? $this->getContext($context, "exception"))));
        echo "
*/
";
        
        $__internal_e578995dfa6b1a5a119347598f217a1e36a546af0adcd11a03a4967a2ce114e8->leave($__internal_e578995dfa6b1a5a119347598f217a1e36a546af0adcd11a03a4967a2ce114e8_prof);

        
        $__internal_af11b0d9ec05417a501cc0995d2171a3f15eb68ae158b9a0dd52fe95c46f4a12->leave($__internal_af11b0d9ec05417a501cc0995d2171a3f15eb68ae158b9a0dd52fe95c46f4a12_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ include('@Twig/Exception/exception.txt.twig', { exception: exception }) }}
*/
", "TwigBundle:Exception:exception.css.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle/Resources/views/Exception/exception.css.twig");
    }
}
