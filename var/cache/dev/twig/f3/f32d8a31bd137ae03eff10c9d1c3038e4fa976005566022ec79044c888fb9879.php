<?php

/* @FOSUser/Profile/show.html.twig */
class __TwigTemplate_0d10ff10a0e5e4b3d93501aa9f4ffaec72fec04ed8e2180903152c183b3217af extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Profile/show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2af0f8b18154426acde5ca9baa0551fe4faaf3b2efc61007928f1f5dea3bfcb2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2af0f8b18154426acde5ca9baa0551fe4faaf3b2efc61007928f1f5dea3bfcb2->enter($__internal_2af0f8b18154426acde5ca9baa0551fe4faaf3b2efc61007928f1f5dea3bfcb2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Profile/show.html.twig"));

        $__internal_bcf9f918b421f7df859f141815872e11c494883e38815ef1bfe7da5665fe9ce5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bcf9f918b421f7df859f141815872e11c494883e38815ef1bfe7da5665fe9ce5->enter($__internal_bcf9f918b421f7df859f141815872e11c494883e38815ef1bfe7da5665fe9ce5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Profile/show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2af0f8b18154426acde5ca9baa0551fe4faaf3b2efc61007928f1f5dea3bfcb2->leave($__internal_2af0f8b18154426acde5ca9baa0551fe4faaf3b2efc61007928f1f5dea3bfcb2_prof);

        
        $__internal_bcf9f918b421f7df859f141815872e11c494883e38815ef1bfe7da5665fe9ce5->leave($__internal_bcf9f918b421f7df859f141815872e11c494883e38815ef1bfe7da5665fe9ce5_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_957434597977f6bb9d0c3bd0390af3e03f49ee9535c6920d13bec56e59792c01 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_957434597977f6bb9d0c3bd0390af3e03f49ee9535c6920d13bec56e59792c01->enter($__internal_957434597977f6bb9d0c3bd0390af3e03f49ee9535c6920d13bec56e59792c01_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_ad4f0fb7df37190bcc74d15402c753d4e5d50fa33ee3e6cfcfa5f715127255f2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ad4f0fb7df37190bcc74d15402c753d4e5d50fa33ee3e6cfcfa5f715127255f2->enter($__internal_ad4f0fb7df37190bcc74d15402c753d4e5d50fa33ee3e6cfcfa5f715127255f2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Profile/show_content.html.twig", "@FOSUser/Profile/show.html.twig", 4)->display($context);
        
        $__internal_ad4f0fb7df37190bcc74d15402c753d4e5d50fa33ee3e6cfcfa5f715127255f2->leave($__internal_ad4f0fb7df37190bcc74d15402c753d4e5d50fa33ee3e6cfcfa5f715127255f2_prof);

        
        $__internal_957434597977f6bb9d0c3bd0390af3e03f49ee9535c6920d13bec56e59792c01->leave($__internal_957434597977f6bb9d0c3bd0390af3e03f49ee9535c6920d13bec56e59792c01_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Profile/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Profile/show_content.html.twig\" %}
{% endblock fos_user_content %}
", "@FOSUser/Profile/show.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\app\\Resources\\FOSUserBundle\\views\\Profile\\show.html.twig");
    }
}
