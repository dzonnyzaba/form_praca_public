<?php

/* @Framework/Form/widget_container_attributes.html.php */
class __TwigTemplate_f1ad46441adaaa876ead5e4032cec652eb0eb235e9271099cffa72e8f8522253 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d262aba7563bd787086359d6f007c8778261f61ba2f1ddcbddbb845abddb27fc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d262aba7563bd787086359d6f007c8778261f61ba2f1ddcbddbb845abddb27fc->enter($__internal_d262aba7563bd787086359d6f007c8778261f61ba2f1ddcbddbb845abddb27fc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/widget_container_attributes.html.php"));

        $__internal_6715bad5822a49aa06fd45f1f5c012248a4398e993e2206e8df888e7498ce39a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6715bad5822a49aa06fd45f1f5c012248a4398e993e2206e8df888e7498ce39a->enter($__internal_6715bad5822a49aa06fd45f1f5c012248a4398e993e2206e8df888e7498ce39a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/widget_container_attributes.html.php"));

        // line 1
        echo "<?php if (!empty(\$id)): ?>id=\"<?php echo \$view->escape(\$id) ?>\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
";
        
        $__internal_d262aba7563bd787086359d6f007c8778261f61ba2f1ddcbddbb845abddb27fc->leave($__internal_d262aba7563bd787086359d6f007c8778261f61ba2f1ddcbddbb845abddb27fc_prof);

        
        $__internal_6715bad5822a49aa06fd45f1f5c012248a4398e993e2206e8df888e7498ce39a->leave($__internal_6715bad5822a49aa06fd45f1f5c012248a4398e993e2206e8df888e7498ce39a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/widget_container_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (!empty(\$id)): ?>id=\"<?php echo \$view->escape(\$id) ?>\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
", "@Framework/Form/widget_container_attributes.html.php", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\widget_container_attributes.html.php");
    }
}
