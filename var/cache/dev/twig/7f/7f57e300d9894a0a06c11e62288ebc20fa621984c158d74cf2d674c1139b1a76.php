<?php

/* @Twig/images/chevron-right.svg */
class __TwigTemplate_38a6c797aab9659831372987100a40392803c422a7b2171a144e8b5467094f9f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_73e07a4b4853f99f211980d6079091d0ffa8d9c62cbcdc2e63de126547e58491 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_73e07a4b4853f99f211980d6079091d0ffa8d9c62cbcdc2e63de126547e58491->enter($__internal_73e07a4b4853f99f211980d6079091d0ffa8d9c62cbcdc2e63de126547e58491_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/chevron-right.svg"));

        $__internal_6ba07e8b7e8c378db0dcf885535db067d938af40a3c9ca25f4218481945a695b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6ba07e8b7e8c378db0dcf885535db067d938af40a3c9ca25f4218481945a695b->enter($__internal_6ba07e8b7e8c378db0dcf885535db067d938af40a3c9ca25f4218481945a695b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/chevron-right.svg"));

        // line 1
        echo "<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path fill=\"#FFF\" d=\"M1363 877l-742 742q-19 19-45 19t-45-19l-166-166q-19-19-19-45t19-45l531-531-531-531q-19-19-19-45t19-45l166-166q19-19 45-19t45 19l742 742q19 19 19 45t-19 45z\"/></svg>
";
        
        $__internal_73e07a4b4853f99f211980d6079091d0ffa8d9c62cbcdc2e63de126547e58491->leave($__internal_73e07a4b4853f99f211980d6079091d0ffa8d9c62cbcdc2e63de126547e58491_prof);

        
        $__internal_6ba07e8b7e8c378db0dcf885535db067d938af40a3c9ca25f4218481945a695b->leave($__internal_6ba07e8b7e8c378db0dcf885535db067d938af40a3c9ca25f4218481945a695b_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/images/chevron-right.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path fill=\"#FFF\" d=\"M1363 877l-742 742q-19 19-45 19t-45-19l-166-166q-19-19-19-45t19-45l531-531-531-531q-19-19-19-45t19-45l166-166q19-19 45-19t45 19l742 742q19 19 19 45t-19 45z\"/></svg>
", "@Twig/images/chevron-right.svg", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\images\\chevron-right.svg");
    }
}
