<?php

/* TwigBundle:Exception:error.atom.twig */
class __TwigTemplate_6e7a8411032a9e7fc9fee18ab0fb54b26cfafc0b52b476d6a1e159fc6277a230 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_452657bb01ddbed50abd50796bec422da71a4f544254fa3fbbaaa67140cde813 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_452657bb01ddbed50abd50796bec422da71a4f544254fa3fbbaaa67140cde813->enter($__internal_452657bb01ddbed50abd50796bec422da71a4f544254fa3fbbaaa67140cde813_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.atom.twig"));

        $__internal_896b07d98779259a9c89d2913f2647e8db986fbcbdb793be8abffc22084e5a62 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_896b07d98779259a9c89d2913f2647e8db986fbcbdb793be8abffc22084e5a62->enter($__internal_896b07d98779259a9c89d2913f2647e8db986fbcbdb793be8abffc22084e5a62_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.atom.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/Exception/error.xml.twig");
        echo "
";
        
        $__internal_452657bb01ddbed50abd50796bec422da71a4f544254fa3fbbaaa67140cde813->leave($__internal_452657bb01ddbed50abd50796bec422da71a4f544254fa3fbbaaa67140cde813_prof);

        
        $__internal_896b07d98779259a9c89d2913f2647e8db986fbcbdb793be8abffc22084e5a62->leave($__internal_896b07d98779259a9c89d2913f2647e8db986fbcbdb793be8abffc22084e5a62_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/Exception/error.xml.twig') }}
", "TwigBundle:Exception:error.atom.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle/Resources/views/Exception/error.atom.twig");
    }
}
