<?php

/* @Twig/layout.html.twig */
class __TwigTemplate_0ef46ff3e00be850b375145c4de69a72941605b1b30816a42d1a36aa92bc080f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c922ba0d5ae43631e65597ac6a971816242947ba2e2dbe36e1e7de0df5d398d7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c922ba0d5ae43631e65597ac6a971816242947ba2e2dbe36e1e7de0df5d398d7->enter($__internal_c922ba0d5ae43631e65597ac6a971816242947ba2e2dbe36e1e7de0df5d398d7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/layout.html.twig"));

        $__internal_0a74eb919ea9e1297929af682fae1d12930ca6869b9539db3e751853a1ca3cb3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0a74eb919ea9e1297929af682fae1d12930ca6869b9539db3e751853a1ca3cb3->enter($__internal_0a74eb919ea9e1297929af682fae1d12930ca6869b9539db3e751853a1ca3cb3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 8
        echo twig_include($this->env, $context, "@Twig/images/favicon.png.base64");
        echo "\">
        <style>";
        // line 9
        echo twig_include($this->env, $context, "@Twig/exception.css.twig");
        echo "</style>
        ";
        // line 10
        $this->displayBlock('head', $context, $blocks);
        // line 11
        echo "    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">";
        // line 15
        echo twig_include($this->env, $context, "@Twig/images/symfony-logo.svg");
        echo " Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">";
        // line 19
        echo twig_include($this->env, $context, "@Twig/images/icon-book.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">";
        // line 26
        echo twig_include($this->env, $context, "@Twig/images/icon-support.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        ";
        // line 33
        $this->displayBlock('body', $context, $blocks);
        // line 34
        echo "        ";
        echo twig_include($this->env, $context, "@Twig/base_js.html.twig");
        echo "
    </body>
</html>
";
        
        $__internal_c922ba0d5ae43631e65597ac6a971816242947ba2e2dbe36e1e7de0df5d398d7->leave($__internal_c922ba0d5ae43631e65597ac6a971816242947ba2e2dbe36e1e7de0df5d398d7_prof);

        
        $__internal_0a74eb919ea9e1297929af682fae1d12930ca6869b9539db3e751853a1ca3cb3->leave($__internal_0a74eb919ea9e1297929af682fae1d12930ca6869b9539db3e751853a1ca3cb3_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_5d26212b8b9a65f483e9ac5c4c5ec31473745873ceec8e2d1e15c5faf714ac0d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5d26212b8b9a65f483e9ac5c4c5ec31473745873ceec8e2d1e15c5faf714ac0d->enter($__internal_5d26212b8b9a65f483e9ac5c4c5ec31473745873ceec8e2d1e15c5faf714ac0d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_bb90802c8ef4012f073576d3dd12fea2bc42794df2c257683ab82bb0d1cf887e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bb90802c8ef4012f073576d3dd12fea2bc42794df2c257683ab82bb0d1cf887e->enter($__internal_bb90802c8ef4012f073576d3dd12fea2bc42794df2c257683ab82bb0d1cf887e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_bb90802c8ef4012f073576d3dd12fea2bc42794df2c257683ab82bb0d1cf887e->leave($__internal_bb90802c8ef4012f073576d3dd12fea2bc42794df2c257683ab82bb0d1cf887e_prof);

        
        $__internal_5d26212b8b9a65f483e9ac5c4c5ec31473745873ceec8e2d1e15c5faf714ac0d->leave($__internal_5d26212b8b9a65f483e9ac5c4c5ec31473745873ceec8e2d1e15c5faf714ac0d_prof);

    }

    // line 10
    public function block_head($context, array $blocks = array())
    {
        $__internal_edfbe96d3db144edbbebcddb4c52b03f2da92adfef6cc2bc1d54f38309fb02a6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_edfbe96d3db144edbbebcddb4c52b03f2da92adfef6cc2bc1d54f38309fb02a6->enter($__internal_edfbe96d3db144edbbebcddb4c52b03f2da92adfef6cc2bc1d54f38309fb02a6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_906512640918e267df996cb77c04ae582f518482b9d986a0c8cae60374f2229f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_906512640918e267df996cb77c04ae582f518482b9d986a0c8cae60374f2229f->enter($__internal_906512640918e267df996cb77c04ae582f518482b9d986a0c8cae60374f2229f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        
        $__internal_906512640918e267df996cb77c04ae582f518482b9d986a0c8cae60374f2229f->leave($__internal_906512640918e267df996cb77c04ae582f518482b9d986a0c8cae60374f2229f_prof);

        
        $__internal_edfbe96d3db144edbbebcddb4c52b03f2da92adfef6cc2bc1d54f38309fb02a6->leave($__internal_edfbe96d3db144edbbebcddb4c52b03f2da92adfef6cc2bc1d54f38309fb02a6_prof);

    }

    // line 33
    public function block_body($context, array $blocks = array())
    {
        $__internal_503d01add82d6373ddc7858ec2651e1e03947c4a5ed2bacd98f4a8192f8ef1d0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_503d01add82d6373ddc7858ec2651e1e03947c4a5ed2bacd98f4a8192f8ef1d0->enter($__internal_503d01add82d6373ddc7858ec2651e1e03947c4a5ed2bacd98f4a8192f8ef1d0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_f5b56cd186885e6903ee0e3e225ced7be8b1393dd8083da7ee6a154c19272e4d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f5b56cd186885e6903ee0e3e225ced7be8b1393dd8083da7ee6a154c19272e4d->enter($__internal_f5b56cd186885e6903ee0e3e225ced7be8b1393dd8083da7ee6a154c19272e4d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_f5b56cd186885e6903ee0e3e225ced7be8b1393dd8083da7ee6a154c19272e4d->leave($__internal_f5b56cd186885e6903ee0e3e225ced7be8b1393dd8083da7ee6a154c19272e4d_prof);

        
        $__internal_503d01add82d6373ddc7858ec2651e1e03947c4a5ed2bacd98f4a8192f8ef1d0->leave($__internal_503d01add82d6373ddc7858ec2651e1e03947c4a5ed2bacd98f4a8192f8ef1d0_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 33,  120 => 10,  103 => 7,  88 => 34,  86 => 33,  76 => 26,  66 => 19,  59 => 15,  53 => 11,  51 => 10,  47 => 9,  43 => 8,  39 => 7,  33 => 4,  28 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"{{ _charset }}\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>{% block title %}{% endblock %}</title>
        <link rel=\"icon\" type=\"image/png\" href=\"{{ include('@Twig/images/favicon.png.base64') }}\">
        <style>{{ include('@Twig/exception.css.twig') }}</style>
        {% block head %}{% endblock %}
    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">{{ include('@Twig/images/symfony-logo.svg') }} Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-book.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-support.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        {% block body %}{% endblock %}
        {{ include('@Twig/base_js.html.twig') }}
    </body>
</html>
", "@Twig/layout.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\layout.html.twig");
    }
}
