<?php

/* @Framework/Form/hidden_widget.html.php */
class __TwigTemplate_12ceef6175538e89135020ff8cfbd7ecffadfea54fc0de7930b6cc798711c3a9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cabc939c1bd047128f799d222a33f0ab0484dd4fc4b9e300dde87d6895b1439b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cabc939c1bd047128f799d222a33f0ab0484dd4fc4b9e300dde87d6895b1439b->enter($__internal_cabc939c1bd047128f799d222a33f0ab0484dd4fc4b9e300dde87d6895b1439b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        $__internal_27f83090a5e1079af8fa59651a91385c889055a4942e1f29ba27b449680cff0a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_27f83090a5e1079af8fa59651a91385c889055a4942e1f29ba27b449680cff0a->enter($__internal_27f83090a5e1079af8fa59651a91385c889055a4942e1f29ba27b449680cff0a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
";
        
        $__internal_cabc939c1bd047128f799d222a33f0ab0484dd4fc4b9e300dde87d6895b1439b->leave($__internal_cabc939c1bd047128f799d222a33f0ab0484dd4fc4b9e300dde87d6895b1439b_prof);

        
        $__internal_27f83090a5e1079af8fa59651a91385c889055a4942e1f29ba27b449680cff0a->leave($__internal_27f83090a5e1079af8fa59651a91385c889055a4942e1f29ba27b449680cff0a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
", "@Framework/Form/hidden_widget.html.php", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\hidden_widget.html.php");
    }
}
