<?php

/* @FOSUser/Registration/check_email.html.twig */
class __TwigTemplate_0a66db1d453019ec933d9eb5adbb819f44a0ed72c48e08f4e8fd0533a96e0311 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Registration/check_email.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a8c409c9fd91c3b0305899c95e1e128898d021b5db7f84a683c3af28f0928a0e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a8c409c9fd91c3b0305899c95e1e128898d021b5db7f84a683c3af28f0928a0e->enter($__internal_a8c409c9fd91c3b0305899c95e1e128898d021b5db7f84a683c3af28f0928a0e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/check_email.html.twig"));

        $__internal_977253d74dcc3796b02e8cd373ffdfa934f05865034594151bd8c44e90e7e44e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_977253d74dcc3796b02e8cd373ffdfa934f05865034594151bd8c44e90e7e44e->enter($__internal_977253d74dcc3796b02e8cd373ffdfa934f05865034594151bd8c44e90e7e44e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/check_email.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a8c409c9fd91c3b0305899c95e1e128898d021b5db7f84a683c3af28f0928a0e->leave($__internal_a8c409c9fd91c3b0305899c95e1e128898d021b5db7f84a683c3af28f0928a0e_prof);

        
        $__internal_977253d74dcc3796b02e8cd373ffdfa934f05865034594151bd8c44e90e7e44e->leave($__internal_977253d74dcc3796b02e8cd373ffdfa934f05865034594151bd8c44e90e7e44e_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_b7f0e197b480887a0e7ba32168da3ba5c233f770f2164e295553f7159f38ec2f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b7f0e197b480887a0e7ba32168da3ba5c233f770f2164e295553f7159f38ec2f->enter($__internal_b7f0e197b480887a0e7ba32168da3ba5c233f770f2164e295553f7159f38ec2f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_42d23021296326c8b4294d73c062a1df349447b6166ab619b05d0a077753eee0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_42d23021296326c8b4294d73c062a1df349447b6166ab619b05d0a077753eee0->enter($__internal_42d23021296326c8b4294d73c062a1df349447b6166ab619b05d0a077753eee0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "    <p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.check_email", array("%email%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "email", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
";
        
        $__internal_42d23021296326c8b4294d73c062a1df349447b6166ab619b05d0a077753eee0->leave($__internal_42d23021296326c8b4294d73c062a1df349447b6166ab619b05d0a077753eee0_prof);

        
        $__internal_b7f0e197b480887a0e7ba32168da3ba5c233f770f2164e295553f7159f38ec2f->leave($__internal_b7f0e197b480887a0e7ba32168da3ba5c233f770f2164e295553f7159f38ec2f_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Registration/check_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 6,  40 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% trans_default_domain 'FOSUserBundle' %}

{% block fos_user_content %}
    <p>{{ 'registration.check_email'|trans({'%email%': user.email}) }}</p>
{% endblock fos_user_content %}
", "@FOSUser/Registration/check_email.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\app\\Resources\\FOSUserBundle\\views\\Registration\\check_email.html.twig");
    }
}
