<?php

/* @Twig/Exception/error.json.twig */
class __TwigTemplate_aaa33a75cf7a0988a5bf811895e3e5ef98796ae4f1bbec179c245af3c83f74b3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ea33113a67df743eeb0d0dad1a7ca8de5924be937befcaf54f8a53e8a038043a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ea33113a67df743eeb0d0dad1a7ca8de5924be937befcaf54f8a53e8a038043a->enter($__internal_ea33113a67df743eeb0d0dad1a7ca8de5924be937befcaf54f8a53e8a038043a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.json.twig"));

        $__internal_efce9abf217f75dae4dd7feb2ae618ef664c856c39732f064188216b10b153d0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_efce9abf217f75dae4dd7feb2ae618ef664c856c39732f064188216b10b153d0->enter($__internal_efce9abf217f75dae4dd7feb2ae618ef664c856c39732f064188216b10b153d0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => ($context["status_code"] ?? $this->getContext($context, "status_code")), "message" => ($context["status_text"] ?? $this->getContext($context, "status_text")))));
        echo "
";
        
        $__internal_ea33113a67df743eeb0d0dad1a7ca8de5924be937befcaf54f8a53e8a038043a->leave($__internal_ea33113a67df743eeb0d0dad1a7ca8de5924be937befcaf54f8a53e8a038043a_prof);

        
        $__internal_efce9abf217f75dae4dd7feb2ae618ef664c856c39732f064188216b10b153d0->leave($__internal_efce9abf217f75dae4dd7feb2ae618ef664c856c39732f064188216b10b153d0_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ { 'error': { 'code': status_code, 'message': status_text } }|json_encode|raw }}
", "@Twig/Exception/error.json.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\error.json.twig");
    }
}
