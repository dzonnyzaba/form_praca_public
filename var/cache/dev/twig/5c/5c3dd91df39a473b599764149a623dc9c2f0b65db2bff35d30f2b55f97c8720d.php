<?php

/* WebProfilerBundle:Profiler:toolbar_redirect.html.twig */
class __TwigTemplate_004ed2c035a0936893d03c557d3f66e4ffb22ea4677e43bb1b574f1910da27c6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4353de03646d382168d56afbba10da77285a6f55bb0a54449c852f98c0ffab0b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4353de03646d382168d56afbba10da77285a6f55bb0a54449c852f98c0ffab0b->enter($__internal_4353de03646d382168d56afbba10da77285a6f55bb0a54449c852f98c0ffab0b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $__internal_2076aae1758d083fb8a806df499be75f0033ce996a279b903b9e49c894b47840 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2076aae1758d083fb8a806df499be75f0033ce996a279b903b9e49c894b47840->enter($__internal_2076aae1758d083fb8a806df499be75f0033ce996a279b903b9e49c894b47840_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4353de03646d382168d56afbba10da77285a6f55bb0a54449c852f98c0ffab0b->leave($__internal_4353de03646d382168d56afbba10da77285a6f55bb0a54449c852f98c0ffab0b_prof);

        
        $__internal_2076aae1758d083fb8a806df499be75f0033ce996a279b903b9e49c894b47840->leave($__internal_2076aae1758d083fb8a806df499be75f0033ce996a279b903b9e49c894b47840_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_ce0dd546475e0ab745e4fc4cf495338a5fdd256e12c14c21dd683bc7107217ee = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ce0dd546475e0ab745e4fc4cf495338a5fdd256e12c14c21dd683bc7107217ee->enter($__internal_ce0dd546475e0ab745e4fc4cf495338a5fdd256e12c14c21dd683bc7107217ee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_500a2d2e27ee2d93e32af71fff86abc984e67cac878ce6b5e958c2c3125c5343 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_500a2d2e27ee2d93e32af71fff86abc984e67cac878ce6b5e958c2c3125c5343->enter($__internal_500a2d2e27ee2d93e32af71fff86abc984e67cac878ce6b5e958c2c3125c5343_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_500a2d2e27ee2d93e32af71fff86abc984e67cac878ce6b5e958c2c3125c5343->leave($__internal_500a2d2e27ee2d93e32af71fff86abc984e67cac878ce6b5e958c2c3125c5343_prof);

        
        $__internal_ce0dd546475e0ab745e4fc4cf495338a5fdd256e12c14c21dd683bc7107217ee->leave($__internal_ce0dd546475e0ab745e4fc4cf495338a5fdd256e12c14c21dd683bc7107217ee_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_d1bd9d88e1451d5dc6651e3943f6f3415cbfede494487129c18915b944124b07 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d1bd9d88e1451d5dc6651e3943f6f3415cbfede494487129c18915b944124b07->enter($__internal_d1bd9d88e1451d5dc6651e3943f6f3415cbfede494487129c18915b944124b07_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_d74866906b0846c09bc44797361cc151cf1ff5a519ad2aea9595b4bb74053bcc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d74866906b0846c09bc44797361cc151cf1ff5a519ad2aea9595b4bb74053bcc->enter($__internal_d74866906b0846c09bc44797361cc151cf1ff5a519ad2aea9595b4bb74053bcc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, ($context["location"] ?? $this->getContext($context, "location")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, ($context["location"] ?? $this->getContext($context, "location")), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_d74866906b0846c09bc44797361cc151cf1ff5a519ad2aea9595b4bb74053bcc->leave($__internal_d74866906b0846c09bc44797361cc151cf1ff5a519ad2aea9595b4bb74053bcc_prof);

        
        $__internal_d1bd9d88e1451d5dc6651e3943f6f3415cbfede494487129c18915b944124b07->leave($__internal_d1bd9d88e1451d5dc6651e3943f6f3415cbfede494487129c18915b944124b07_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 8,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block title 'Redirection Intercepted' %}

{% block body %}
    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"{{ location }}\">{{ location }}</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
{% endblock %}
", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle/Resources/views/Profiler/toolbar_redirect.html.twig");
    }
}
