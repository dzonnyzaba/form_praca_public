<?php

/* FOSUserBundle:Resetting:reset.html.twig */
class __TwigTemplate_a2cc721ae2032a5ae3d414f38791317a38f238fd1f420ef40a9c25e25484c03d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Resetting:reset.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_699a1dd9120e7e4aa5e2eeaa1dbb93524f9b3d2e31bdf662e121d8ec41962065 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_699a1dd9120e7e4aa5e2eeaa1dbb93524f9b3d2e31bdf662e121d8ec41962065->enter($__internal_699a1dd9120e7e4aa5e2eeaa1dbb93524f9b3d2e31bdf662e121d8ec41962065_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:reset.html.twig"));

        $__internal_d3083f9815dd9829342a1007c5b9c82115a5e511cc234a40affc5b9e88694cfc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d3083f9815dd9829342a1007c5b9c82115a5e511cc234a40affc5b9e88694cfc->enter($__internal_d3083f9815dd9829342a1007c5b9c82115a5e511cc234a40affc5b9e88694cfc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:reset.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_699a1dd9120e7e4aa5e2eeaa1dbb93524f9b3d2e31bdf662e121d8ec41962065->leave($__internal_699a1dd9120e7e4aa5e2eeaa1dbb93524f9b3d2e31bdf662e121d8ec41962065_prof);

        
        $__internal_d3083f9815dd9829342a1007c5b9c82115a5e511cc234a40affc5b9e88694cfc->leave($__internal_d3083f9815dd9829342a1007c5b9c82115a5e511cc234a40affc5b9e88694cfc_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_f9ad2116d19d8ee7a45e968899591f1f517177bb1c6258cde942b40cfcf2f3b4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f9ad2116d19d8ee7a45e968899591f1f517177bb1c6258cde942b40cfcf2f3b4->enter($__internal_f9ad2116d19d8ee7a45e968899591f1f517177bb1c6258cde942b40cfcf2f3b4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_aba35a53beb66d029c3ed9445c6832cbd2411e0cf4b6c63c813ea1bed4ce18c6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_aba35a53beb66d029c3ed9445c6832cbd2411e0cf4b6c63c813ea1bed4ce18c6->enter($__internal_aba35a53beb66d029c3ed9445c6832cbd2411e0cf4b6c63c813ea1bed4ce18c6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Resetting/reset_content.html.twig", "FOSUserBundle:Resetting:reset.html.twig", 4)->display($context);
        
        $__internal_aba35a53beb66d029c3ed9445c6832cbd2411e0cf4b6c63c813ea1bed4ce18c6->leave($__internal_aba35a53beb66d029c3ed9445c6832cbd2411e0cf4b6c63c813ea1bed4ce18c6_prof);

        
        $__internal_f9ad2116d19d8ee7a45e968899591f1f517177bb1c6258cde942b40cfcf2f3b4->leave($__internal_f9ad2116d19d8ee7a45e968899591f1f517177bb1c6258cde942b40cfcf2f3b4_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:reset.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Resetting/reset_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Resetting:reset.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\app/Resources/FOSUserBundle/views/Resetting/reset.html.twig");
    }
}
