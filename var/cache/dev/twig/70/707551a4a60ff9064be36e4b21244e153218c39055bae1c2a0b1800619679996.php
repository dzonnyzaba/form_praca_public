<?php

/* FOSUserBundle:Resetting:request.html.twig */
class __TwigTemplate_954a07f3d6af0864a952abc1eafffe01266ea69b8a1eb0834c1592b859fd812d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Resetting:request.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_09854ff155db69c9439496a7df0d2befb7e3a4413543ce1cf91816c7eb2fa55e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_09854ff155db69c9439496a7df0d2befb7e3a4413543ce1cf91816c7eb2fa55e->enter($__internal_09854ff155db69c9439496a7df0d2befb7e3a4413543ce1cf91816c7eb2fa55e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:request.html.twig"));

        $__internal_387c3fa36fc4c986945f0c4da47d5f3859c64cdf908aa78d329495936290480b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_387c3fa36fc4c986945f0c4da47d5f3859c64cdf908aa78d329495936290480b->enter($__internal_387c3fa36fc4c986945f0c4da47d5f3859c64cdf908aa78d329495936290480b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:request.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_09854ff155db69c9439496a7df0d2befb7e3a4413543ce1cf91816c7eb2fa55e->leave($__internal_09854ff155db69c9439496a7df0d2befb7e3a4413543ce1cf91816c7eb2fa55e_prof);

        
        $__internal_387c3fa36fc4c986945f0c4da47d5f3859c64cdf908aa78d329495936290480b->leave($__internal_387c3fa36fc4c986945f0c4da47d5f3859c64cdf908aa78d329495936290480b_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_4e500048b9a381959ed2968287184360df6d226d7eb0eeb1779cc5034af5065e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4e500048b9a381959ed2968287184360df6d226d7eb0eeb1779cc5034af5065e->enter($__internal_4e500048b9a381959ed2968287184360df6d226d7eb0eeb1779cc5034af5065e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_419abdcd253da86a30b21fe3d2eb8b7c7770a9de849f8cdafa5b1c805532a63d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_419abdcd253da86a30b21fe3d2eb8b7c7770a9de849f8cdafa5b1c805532a63d->enter($__internal_419abdcd253da86a30b21fe3d2eb8b7c7770a9de849f8cdafa5b1c805532a63d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Resetting/request_content.html.twig", "FOSUserBundle:Resetting:request.html.twig", 4)->display($context);
        
        $__internal_419abdcd253da86a30b21fe3d2eb8b7c7770a9de849f8cdafa5b1c805532a63d->leave($__internal_419abdcd253da86a30b21fe3d2eb8b7c7770a9de849f8cdafa5b1c805532a63d_prof);

        
        $__internal_4e500048b9a381959ed2968287184360df6d226d7eb0eeb1779cc5034af5065e->leave($__internal_4e500048b9a381959ed2968287184360df6d226d7eb0eeb1779cc5034af5065e_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:request.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Resetting/request_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Resetting:request.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\app/Resources/FOSUserBundle/views/Resetting/request.html.twig");
    }
}
