<?php

/* @FOSUser/Security/login.html.twig */
class __TwigTemplate_ce00d3f84e215a7dd5900504a734b124c8fa4d06957df84a4b0582a82a4aa33b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Security/login.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cf8d5fbe0ba09c7d5542994e52df3d329e9e5a1cb515284d95b3d1fda96dd517 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cf8d5fbe0ba09c7d5542994e52df3d329e9e5a1cb515284d95b3d1fda96dd517->enter($__internal_cf8d5fbe0ba09c7d5542994e52df3d329e9e5a1cb515284d95b3d1fda96dd517_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login.html.twig"));

        $__internal_4011b6513af68e5227e441284748121d8e68c8d347a188c8fe735d02bfcb3bee = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4011b6513af68e5227e441284748121d8e68c8d347a188c8fe735d02bfcb3bee->enter($__internal_4011b6513af68e5227e441284748121d8e68c8d347a188c8fe735d02bfcb3bee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_cf8d5fbe0ba09c7d5542994e52df3d329e9e5a1cb515284d95b3d1fda96dd517->leave($__internal_cf8d5fbe0ba09c7d5542994e52df3d329e9e5a1cb515284d95b3d1fda96dd517_prof);

        
        $__internal_4011b6513af68e5227e441284748121d8e68c8d347a188c8fe735d02bfcb3bee->leave($__internal_4011b6513af68e5227e441284748121d8e68c8d347a188c8fe735d02bfcb3bee_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_d2868b7eb19624d664d5670776b6f629b23b5d4d36002cc7e5a659fb874ded11 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d2868b7eb19624d664d5670776b6f629b23b5d4d36002cc7e5a659fb874ded11->enter($__internal_d2868b7eb19624d664d5670776b6f629b23b5d4d36002cc7e5a659fb874ded11_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_b908ba8503a07ea61f3eb672200672ded2a96d46d9febd2b600810b2b30933dd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b908ba8503a07ea61f3eb672200672ded2a96d46d9febd2b600810b2b30933dd->enter($__internal_b908ba8503a07ea61f3eb672200672ded2a96d46d9febd2b600810b2b30933dd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        echo "    ";
        echo twig_include($this->env, $context, "@FOSUser/Security/login_content.html.twig");
        echo "
";
        
        $__internal_b908ba8503a07ea61f3eb672200672ded2a96d46d9febd2b600810b2b30933dd->leave($__internal_b908ba8503a07ea61f3eb672200672ded2a96d46d9febd2b600810b2b30933dd_prof);

        
        $__internal_d2868b7eb19624d664d5670776b6f629b23b5d4d36002cc7e5a659fb874ded11->leave($__internal_d2868b7eb19624d664d5670776b6f629b23b5d4d36002cc7e5a659fb874ded11_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Security/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
    {{ include('@FOSUser/Security/login_content.html.twig') }}
{% endblock fos_user_content %}
", "@FOSUser/Security/login.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\app\\Resources\\FOSUserBundle\\views\\Security\\login.html.twig");
    }
}
