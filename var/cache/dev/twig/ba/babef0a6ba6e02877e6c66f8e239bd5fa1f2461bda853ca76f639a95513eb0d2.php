<?php

/* FOSUserBundle:Registration:email.txt.twig */
class __TwigTemplate_918c318b42fde38fa27073d87367b06668bd55a23e015b73b6b96071622f6069 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_903052266c6807ce0bcaa01b3ab8969587e7f03e29679119f3e011319f0b6ac3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_903052266c6807ce0bcaa01b3ab8969587e7f03e29679119f3e011319f0b6ac3->enter($__internal_903052266c6807ce0bcaa01b3ab8969587e7f03e29679119f3e011319f0b6ac3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:email.txt.twig"));

        $__internal_2644d1c3439311db6922a0b27c6abf55e281a7ab2117ca8a57b21ea19c6b304a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2644d1c3439311db6922a0b27c6abf55e281a7ab2117ca8a57b21ea19c6b304a->enter($__internal_2644d1c3439311db6922a0b27c6abf55e281a7ab2117ca8a57b21ea19c6b304a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        echo "
";
        // line 8
        $this->displayBlock('body_text', $context, $blocks);
        // line 13
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_903052266c6807ce0bcaa01b3ab8969587e7f03e29679119f3e011319f0b6ac3->leave($__internal_903052266c6807ce0bcaa01b3ab8969587e7f03e29679119f3e011319f0b6ac3_prof);

        
        $__internal_2644d1c3439311db6922a0b27c6abf55e281a7ab2117ca8a57b21ea19c6b304a->leave($__internal_2644d1c3439311db6922a0b27c6abf55e281a7ab2117ca8a57b21ea19c6b304a_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_1262a5d9d520108ae8e3f8cb1f28e014d82c9d3af94e310e10f4b901dca6ecf8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1262a5d9d520108ae8e3f8cb1f28e014d82c9d3af94e310e10f4b901dca6ecf8->enter($__internal_1262a5d9d520108ae8e3f8cb1f28e014d82c9d3af94e310e10f4b901dca6ecf8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        $__internal_a0b6c68dec335ee1f7c5eedc6121375436c90830036bd6d8a529543e43fa4694 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a0b6c68dec335ee1f7c5eedc6121375436c90830036bd6d8a529543e43fa4694->enter($__internal_a0b6c68dec335ee1f7c5eedc6121375436c90830036bd6d8a529543e43fa4694_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.email.subject", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => ($context["confirmationUrl"] ?? $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        
        $__internal_a0b6c68dec335ee1f7c5eedc6121375436c90830036bd6d8a529543e43fa4694->leave($__internal_a0b6c68dec335ee1f7c5eedc6121375436c90830036bd6d8a529543e43fa4694_prof);

        
        $__internal_1262a5d9d520108ae8e3f8cb1f28e014d82c9d3af94e310e10f4b901dca6ecf8->leave($__internal_1262a5d9d520108ae8e3f8cb1f28e014d82c9d3af94e310e10f4b901dca6ecf8_prof);

    }

    // line 8
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_f2bb62a5777c656e4c566bd7ad230ceb442b9e2e41f8af6664c240ed4004452f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f2bb62a5777c656e4c566bd7ad230ceb442b9e2e41f8af6664c240ed4004452f->enter($__internal_f2bb62a5777c656e4c566bd7ad230ceb442b9e2e41f8af6664c240ed4004452f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        $__internal_a18e93c892803c31653393b53d26e3e4f097d182fea1a7d9e8d508049ab5ad3b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a18e93c892803c31653393b53d26e3e4f097d182fea1a7d9e8d508049ab5ad3b->enter($__internal_a18e93c892803c31653393b53d26e3e4f097d182fea1a7d9e8d508049ab5ad3b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.email.message", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => ($context["confirmationUrl"] ?? $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        echo "
";
        
        $__internal_a18e93c892803c31653393b53d26e3e4f097d182fea1a7d9e8d508049ab5ad3b->leave($__internal_a18e93c892803c31653393b53d26e3e4f097d182fea1a7d9e8d508049ab5ad3b_prof);

        
        $__internal_f2bb62a5777c656e4c566bd7ad230ceb442b9e2e41f8af6664c240ed4004452f->leave($__internal_f2bb62a5777c656e4c566bd7ad230ceb442b9e2e41f8af6664c240ed4004452f_prof);

    }

    // line 13
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_0c7c522f1949f8983cd45ffba9f3653ee850e6d4228ab8ba58b3f44c449a5bda = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0c7c522f1949f8983cd45ffba9f3653ee850e6d4228ab8ba58b3f44c449a5bda->enter($__internal_0c7c522f1949f8983cd45ffba9f3653ee850e6d4228ab8ba58b3f44c449a5bda_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        $__internal_42a773e412ef4b99b2ee979b417648f64c9a68c606268ece524e6b208dbc1df6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_42a773e412ef4b99b2ee979b417648f64c9a68c606268ece524e6b208dbc1df6->enter($__internal_42a773e412ef4b99b2ee979b417648f64c9a68c606268ece524e6b208dbc1df6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_42a773e412ef4b99b2ee979b417648f64c9a68c606268ece524e6b208dbc1df6->leave($__internal_42a773e412ef4b99b2ee979b417648f64c9a68c606268ece524e6b208dbc1df6_prof);

        
        $__internal_0c7c522f1949f8983cd45ffba9f3653ee850e6d4228ab8ba58b3f44c449a5bda->leave($__internal_0c7c522f1949f8983cd45ffba9f3653ee850e6d4228ab8ba58b3f44c449a5bda_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  85 => 13,  73 => 10,  64 => 8,  54 => 4,  45 => 2,  35 => 13,  33 => 8,  30 => 7,  28 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}
{% block subject %}
{%- autoescape false -%}
{{ 'registration.email.subject'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{%- endautoescape -%}
{% endblock %}

{% block body_text %}
{% autoescape false %}
{{ 'registration.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{% endautoescape %}
{% endblock %}
{% block body_html %}{% endblock %}
", "FOSUserBundle:Registration:email.txt.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\app/Resources/FOSUserBundle/views/Registration/email.txt.twig");
    }
}
