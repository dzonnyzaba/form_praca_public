<?php

/* @FOSUser/ChangePassword/change_password.html.twig */
class __TwigTemplate_1c296dd8dbdb0234df6ca8393cee0a205d0203e37fb9440b28652b85c56d39ee extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/ChangePassword/change_password.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e39f2ab223f18c34452c35b1111552a74abadbfe3e4e185e337dbba47d3aa219 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e39f2ab223f18c34452c35b1111552a74abadbfe3e4e185e337dbba47d3aa219->enter($__internal_e39f2ab223f18c34452c35b1111552a74abadbfe3e4e185e337dbba47d3aa219_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/ChangePassword/change_password.html.twig"));

        $__internal_2183abfb0e4484472b2463e2d1b33e9816103cbc3763adef6daec5640cf0d023 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2183abfb0e4484472b2463e2d1b33e9816103cbc3763adef6daec5640cf0d023->enter($__internal_2183abfb0e4484472b2463e2d1b33e9816103cbc3763adef6daec5640cf0d023_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/ChangePassword/change_password.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e39f2ab223f18c34452c35b1111552a74abadbfe3e4e185e337dbba47d3aa219->leave($__internal_e39f2ab223f18c34452c35b1111552a74abadbfe3e4e185e337dbba47d3aa219_prof);

        
        $__internal_2183abfb0e4484472b2463e2d1b33e9816103cbc3763adef6daec5640cf0d023->leave($__internal_2183abfb0e4484472b2463e2d1b33e9816103cbc3763adef6daec5640cf0d023_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_2cf1f429d36bad9edc011423b3126d56792f7b9b64405823989ce7e663b374f8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2cf1f429d36bad9edc011423b3126d56792f7b9b64405823989ce7e663b374f8->enter($__internal_2cf1f429d36bad9edc011423b3126d56792f7b9b64405823989ce7e663b374f8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_b3f266cc66abe2bc3f439c54332943718f12261f9ea313057399e7bfd6b320c1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b3f266cc66abe2bc3f439c54332943718f12261f9ea313057399e7bfd6b320c1->enter($__internal_b3f266cc66abe2bc3f439c54332943718f12261f9ea313057399e7bfd6b320c1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/ChangePassword/change_password_content.html.twig", "@FOSUser/ChangePassword/change_password.html.twig", 4)->display($context);
        
        $__internal_b3f266cc66abe2bc3f439c54332943718f12261f9ea313057399e7bfd6b320c1->leave($__internal_b3f266cc66abe2bc3f439c54332943718f12261f9ea313057399e7bfd6b320c1_prof);

        
        $__internal_2cf1f429d36bad9edc011423b3126d56792f7b9b64405823989ce7e663b374f8->leave($__internal_2cf1f429d36bad9edc011423b3126d56792f7b9b64405823989ce7e663b374f8_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/ChangePassword/change_password.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/ChangePassword/change_password_content.html.twig\" %}
{% endblock fos_user_content %}
", "@FOSUser/ChangePassword/change_password.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\app\\Resources\\FOSUserBundle\\views\\ChangePassword\\change_password.html.twig");
    }
}
