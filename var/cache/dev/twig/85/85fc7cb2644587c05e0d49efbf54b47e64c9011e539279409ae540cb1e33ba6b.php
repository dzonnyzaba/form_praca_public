<?php

/* FOSUserBundle:Group:edit.html.twig */
class __TwigTemplate_d062b445c20abe69baf6a85aa7c9daef6e66deb746c21301a07d6ed8823cdc84 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3dda81be6344c40bf21c450690ea7a6a8209dca052d55484fc484aa8702a3ba6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3dda81be6344c40bf21c450690ea7a6a8209dca052d55484fc484aa8702a3ba6->enter($__internal_3dda81be6344c40bf21c450690ea7a6a8209dca052d55484fc484aa8702a3ba6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:edit.html.twig"));

        $__internal_1e2c78bdedf1ebfcdc26cf39fda08b5fd933577b9e8e9ebaf3079cbb929457c6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1e2c78bdedf1ebfcdc26cf39fda08b5fd933577b9e8e9ebaf3079cbb929457c6->enter($__internal_1e2c78bdedf1ebfcdc26cf39fda08b5fd933577b9e8e9ebaf3079cbb929457c6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3dda81be6344c40bf21c450690ea7a6a8209dca052d55484fc484aa8702a3ba6->leave($__internal_3dda81be6344c40bf21c450690ea7a6a8209dca052d55484fc484aa8702a3ba6_prof);

        
        $__internal_1e2c78bdedf1ebfcdc26cf39fda08b5fd933577b9e8e9ebaf3079cbb929457c6->leave($__internal_1e2c78bdedf1ebfcdc26cf39fda08b5fd933577b9e8e9ebaf3079cbb929457c6_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_76cfb37a078ad966929bf15ce52adda6783e2f2741587f737c00ac13e20738af = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_76cfb37a078ad966929bf15ce52adda6783e2f2741587f737c00ac13e20738af->enter($__internal_76cfb37a078ad966929bf15ce52adda6783e2f2741587f737c00ac13e20738af_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_e1042ac596c117b906e6f455bbf5c236d374fff9f340e4f38d787e1b0227819e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e1042ac596c117b906e6f455bbf5c236d374fff9f340e4f38d787e1b0227819e->enter($__internal_e1042ac596c117b906e6f455bbf5c236d374fff9f340e4f38d787e1b0227819e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/edit_content.html.twig", "FOSUserBundle:Group:edit.html.twig", 4)->display($context);
        
        $__internal_e1042ac596c117b906e6f455bbf5c236d374fff9f340e4f38d787e1b0227819e->leave($__internal_e1042ac596c117b906e6f455bbf5c236d374fff9f340e4f38d787e1b0227819e_prof);

        
        $__internal_76cfb37a078ad966929bf15ce52adda6783e2f2741587f737c00ac13e20738af->leave($__internal_76cfb37a078ad966929bf15ce52adda6783e2f2741587f737c00ac13e20738af_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/edit_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Group:edit.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\app/Resources/FOSUserBundle/views/Group/edit.html.twig");
    }
}
