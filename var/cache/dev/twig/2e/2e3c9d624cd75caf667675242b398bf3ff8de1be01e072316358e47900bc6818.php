<?php

/* FOSUserBundle:Profile:edit.html.twig */
class __TwigTemplate_d9a1c5760e8ab440660050033bac0e83c4329a3e05ef58e8ffe2e24e17714e79 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Profile:edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f355b2960deb1c0b4378af2087f9265cb52a900762ce0c2cf63a468f099b12f2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f355b2960deb1c0b4378af2087f9265cb52a900762ce0c2cf63a468f099b12f2->enter($__internal_f355b2960deb1c0b4378af2087f9265cb52a900762ce0c2cf63a468f099b12f2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:edit.html.twig"));

        $__internal_04e45a27fc7407d53e946aeafefed11f2a0d6e1096ecad3921f29dc679583c6e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_04e45a27fc7407d53e946aeafefed11f2a0d6e1096ecad3921f29dc679583c6e->enter($__internal_04e45a27fc7407d53e946aeafefed11f2a0d6e1096ecad3921f29dc679583c6e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f355b2960deb1c0b4378af2087f9265cb52a900762ce0c2cf63a468f099b12f2->leave($__internal_f355b2960deb1c0b4378af2087f9265cb52a900762ce0c2cf63a468f099b12f2_prof);

        
        $__internal_04e45a27fc7407d53e946aeafefed11f2a0d6e1096ecad3921f29dc679583c6e->leave($__internal_04e45a27fc7407d53e946aeafefed11f2a0d6e1096ecad3921f29dc679583c6e_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_ce4cad808cf0b801c7aece09503e213452f2c4f76d685a4e2ea57756713995c3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ce4cad808cf0b801c7aece09503e213452f2c4f76d685a4e2ea57756713995c3->enter($__internal_ce4cad808cf0b801c7aece09503e213452f2c4f76d685a4e2ea57756713995c3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_7dd79076ac183314f43f774f198888a8c238dab48024b15addf9c9f2e6442982 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7dd79076ac183314f43f774f198888a8c238dab48024b15addf9c9f2e6442982->enter($__internal_7dd79076ac183314f43f774f198888a8c238dab48024b15addf9c9f2e6442982_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Profile/edit_content.html.twig", "FOSUserBundle:Profile:edit.html.twig", 4)->display($context);
        
        $__internal_7dd79076ac183314f43f774f198888a8c238dab48024b15addf9c9f2e6442982->leave($__internal_7dd79076ac183314f43f774f198888a8c238dab48024b15addf9c9f2e6442982_prof);

        
        $__internal_ce4cad808cf0b801c7aece09503e213452f2c4f76d685a4e2ea57756713995c3->leave($__internal_ce4cad808cf0b801c7aece09503e213452f2c4f76d685a4e2ea57756713995c3_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Profile/edit_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Profile:edit.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\app/Resources/FOSUserBundle/views/Profile/edit.html.twig");
    }
}
