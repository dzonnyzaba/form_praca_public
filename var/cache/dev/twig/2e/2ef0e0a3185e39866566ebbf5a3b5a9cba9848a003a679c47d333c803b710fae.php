<?php

/* @FOSUser/Resetting/email.txt.twig */
class __TwigTemplate_6bed18124c9bdca4ae01d82739d5173c769ae941aac0fc8cf5458d61db72de5a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6bceb5a44bdc6ab684948a4d14ef8240dbe4e66878624878707961f085fb4a14 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6bceb5a44bdc6ab684948a4d14ef8240dbe4e66878624878707961f085fb4a14->enter($__internal_6bceb5a44bdc6ab684948a4d14ef8240dbe4e66878624878707961f085fb4a14_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Resetting/email.txt.twig"));

        $__internal_8b83885ebfb2eb6d91b72f71857a6d659e665748d0ff7fba8f5d1f23910b0c40 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8b83885ebfb2eb6d91b72f71857a6d659e665748d0ff7fba8f5d1f23910b0c40->enter($__internal_8b83885ebfb2eb6d91b72f71857a6d659e665748d0ff7fba8f5d1f23910b0c40_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Resetting/email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        echo "
";
        // line 8
        $this->displayBlock('body_text', $context, $blocks);
        // line 13
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_6bceb5a44bdc6ab684948a4d14ef8240dbe4e66878624878707961f085fb4a14->leave($__internal_6bceb5a44bdc6ab684948a4d14ef8240dbe4e66878624878707961f085fb4a14_prof);

        
        $__internal_8b83885ebfb2eb6d91b72f71857a6d659e665748d0ff7fba8f5d1f23910b0c40->leave($__internal_8b83885ebfb2eb6d91b72f71857a6d659e665748d0ff7fba8f5d1f23910b0c40_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_855323deb6e2f1102abc83610535781a4fcfbe8da8e9d518d5597f815a843358 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_855323deb6e2f1102abc83610535781a4fcfbe8da8e9d518d5597f815a843358->enter($__internal_855323deb6e2f1102abc83610535781a4fcfbe8da8e9d518d5597f815a843358_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        $__internal_7050764c57cc6745326e156f201231a12e95422d2876031f6ef045c1a2df8cc2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7050764c57cc6745326e156f201231a12e95422d2876031f6ef045c1a2df8cc2->enter($__internal_7050764c57cc6745326e156f201231a12e95422d2876031f6ef045c1a2df8cc2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.email.subject", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array())), "FOSUserBundle");
        
        $__internal_7050764c57cc6745326e156f201231a12e95422d2876031f6ef045c1a2df8cc2->leave($__internal_7050764c57cc6745326e156f201231a12e95422d2876031f6ef045c1a2df8cc2_prof);

        
        $__internal_855323deb6e2f1102abc83610535781a4fcfbe8da8e9d518d5597f815a843358->leave($__internal_855323deb6e2f1102abc83610535781a4fcfbe8da8e9d518d5597f815a843358_prof);

    }

    // line 8
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_b2e929f3ce57767da539d6434020211347a9ae2c85a7c0716622df2ff9e8c3d3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b2e929f3ce57767da539d6434020211347a9ae2c85a7c0716622df2ff9e8c3d3->enter($__internal_b2e929f3ce57767da539d6434020211347a9ae2c85a7c0716622df2ff9e8c3d3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        $__internal_4a0aaea527af8a12a836ce0be19c4002bb2bf107c0a058ccbff3ba60a3a99b3e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4a0aaea527af8a12a836ce0be19c4002bb2bf107c0a058ccbff3ba60a3a99b3e->enter($__internal_4a0aaea527af8a12a836ce0be19c4002bb2bf107c0a058ccbff3ba60a3a99b3e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.email.message", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => ($context["confirmationUrl"] ?? $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        echo "
";
        
        $__internal_4a0aaea527af8a12a836ce0be19c4002bb2bf107c0a058ccbff3ba60a3a99b3e->leave($__internal_4a0aaea527af8a12a836ce0be19c4002bb2bf107c0a058ccbff3ba60a3a99b3e_prof);

        
        $__internal_b2e929f3ce57767da539d6434020211347a9ae2c85a7c0716622df2ff9e8c3d3->leave($__internal_b2e929f3ce57767da539d6434020211347a9ae2c85a7c0716622df2ff9e8c3d3_prof);

    }

    // line 13
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_c3d267a19cc830b37abcdbe8f7643c46fbe9cf475807b00313752a3053c7f09e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c3d267a19cc830b37abcdbe8f7643c46fbe9cf475807b00313752a3053c7f09e->enter($__internal_c3d267a19cc830b37abcdbe8f7643c46fbe9cf475807b00313752a3053c7f09e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        $__internal_902de3b119bb5a3330b01318d85dc54eb8e52ef716d368dde5c367c8a5acc836 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_902de3b119bb5a3330b01318d85dc54eb8e52ef716d368dde5c367c8a5acc836->enter($__internal_902de3b119bb5a3330b01318d85dc54eb8e52ef716d368dde5c367c8a5acc836_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_902de3b119bb5a3330b01318d85dc54eb8e52ef716d368dde5c367c8a5acc836->leave($__internal_902de3b119bb5a3330b01318d85dc54eb8e52ef716d368dde5c367c8a5acc836_prof);

        
        $__internal_c3d267a19cc830b37abcdbe8f7643c46fbe9cf475807b00313752a3053c7f09e->leave($__internal_c3d267a19cc830b37abcdbe8f7643c46fbe9cf475807b00313752a3053c7f09e_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Resetting/email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  85 => 13,  73 => 10,  64 => 8,  54 => 4,  45 => 2,  35 => 13,  33 => 8,  30 => 7,  28 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}
{% block subject %}
{%- autoescape false -%}
{{ 'resetting.email.subject'|trans({'%username%': user.username}) }}
{%- endautoescape -%}
{% endblock %}

{% block body_text %}
{% autoescape false %}
{{ 'resetting.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{% endautoescape %}
{% endblock %}
{% block body_html %}{% endblock %}
", "@FOSUser/Resetting/email.txt.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\app\\Resources\\FOSUserBundle\\views\\Resetting\\email.txt.twig");
    }
}
