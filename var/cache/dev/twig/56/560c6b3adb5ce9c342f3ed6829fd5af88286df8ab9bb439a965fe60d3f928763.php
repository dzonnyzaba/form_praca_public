<?php

/* FOSUserBundle:Registration:register.html.twig */
class __TwigTemplate_e71f54c2974461908140e91dcf30474d652509a9cb441886853b450a8745d1c6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Registration:register.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2235cd13ad81637c502cf84d5852ab68fe2ab98d8fff6c5dc6739088e0e2e03c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2235cd13ad81637c502cf84d5852ab68fe2ab98d8fff6c5dc6739088e0e2e03c->enter($__internal_2235cd13ad81637c502cf84d5852ab68fe2ab98d8fff6c5dc6739088e0e2e03c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:register.html.twig"));

        $__internal_1963adc8f7feb254bc4cab14169c6e5e6977bd0548672a49cbc46873da684542 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1963adc8f7feb254bc4cab14169c6e5e6977bd0548672a49cbc46873da684542->enter($__internal_1963adc8f7feb254bc4cab14169c6e5e6977bd0548672a49cbc46873da684542_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:register.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2235cd13ad81637c502cf84d5852ab68fe2ab98d8fff6c5dc6739088e0e2e03c->leave($__internal_2235cd13ad81637c502cf84d5852ab68fe2ab98d8fff6c5dc6739088e0e2e03c_prof);

        
        $__internal_1963adc8f7feb254bc4cab14169c6e5e6977bd0548672a49cbc46873da684542->leave($__internal_1963adc8f7feb254bc4cab14169c6e5e6977bd0548672a49cbc46873da684542_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_5685d56fd791fcdad462f3b3c199f9a88282c9dbbbec09dfca62bc36baa53566 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5685d56fd791fcdad462f3b3c199f9a88282c9dbbbec09dfca62bc36baa53566->enter($__internal_5685d56fd791fcdad462f3b3c199f9a88282c9dbbbec09dfca62bc36baa53566_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_555c0ea658e1108129cc2bac599d67ca0fb25a713c3ecb5a2a3063c16d624197 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_555c0ea658e1108129cc2bac599d67ca0fb25a713c3ecb5a2a3063c16d624197->enter($__internal_555c0ea658e1108129cc2bac599d67ca0fb25a713c3ecb5a2a3063c16d624197_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Registration/register_content.html.twig", "FOSUserBundle:Registration:register.html.twig", 4)->display($context);
        
        $__internal_555c0ea658e1108129cc2bac599d67ca0fb25a713c3ecb5a2a3063c16d624197->leave($__internal_555c0ea658e1108129cc2bac599d67ca0fb25a713c3ecb5a2a3063c16d624197_prof);

        
        $__internal_5685d56fd791fcdad462f3b3c199f9a88282c9dbbbec09dfca62bc36baa53566->leave($__internal_5685d56fd791fcdad462f3b3c199f9a88282c9dbbbec09dfca62bc36baa53566_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Registration/register_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Registration:register.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\app/Resources/FOSUserBundle/views/Registration/register.html.twig");
    }
}
