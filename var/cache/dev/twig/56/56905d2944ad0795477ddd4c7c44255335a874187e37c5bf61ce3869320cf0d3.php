<?php

/* @Framework/Form/form_widget.html.php */
class __TwigTemplate_e80bd426376c58c1d5dca3b1cc12ff386d456005d8dcca2ea8a2fe02d8fa5d55 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_58feab6a0954ea04a9ec050def555c1f6f98707252e1fca786b2be1b9060b64f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_58feab6a0954ea04a9ec050def555c1f6f98707252e1fca786b2be1b9060b64f->enter($__internal_58feab6a0954ea04a9ec050def555c1f6f98707252e1fca786b2be1b9060b64f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        $__internal_f0c7fab6973f57b0cd0e3581b3bffbd7f69e65d7d7379ac4131dc7196617700e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f0c7fab6973f57b0cd0e3581b3bffbd7f69e65d7d7379ac4131dc7196617700e->enter($__internal_f0c7fab6973f57b0cd0e3581b3bffbd7f69e65d7d7379ac4131dc7196617700e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        // line 1
        echo "<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
";
        
        $__internal_58feab6a0954ea04a9ec050def555c1f6f98707252e1fca786b2be1b9060b64f->leave($__internal_58feab6a0954ea04a9ec050def555c1f6f98707252e1fca786b2be1b9060b64f_prof);

        
        $__internal_f0c7fab6973f57b0cd0e3581b3bffbd7f69e65d7d7379ac4131dc7196617700e->leave($__internal_f0c7fab6973f57b0cd0e3581b3bffbd7f69e65d7d7379ac4131dc7196617700e_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
", "@Framework/Form/form_widget.html.php", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\form_widget.html.php");
    }
}
