<?php

/* FOSUserBundle::layout.html.twig */
class __TwigTemplate_afabcaa289a78be0d6362e0d3a9fa74b7734b1209c3d08c48f48c2a56b87ee74 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "FOSUserBundle::layout.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a513e6c9f7791ca31302d84f8bfaeddca42a1685b8e64753829753c57a3c3532 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a513e6c9f7791ca31302d84f8bfaeddca42a1685b8e64753829753c57a3c3532->enter($__internal_a513e6c9f7791ca31302d84f8bfaeddca42a1685b8e64753829753c57a3c3532_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle::layout.html.twig"));

        $__internal_e76bba9556433c917b0db5b9074ddf20b595db3c4d3e43fb76dcfa77f5224dbc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e76bba9556433c917b0db5b9074ddf20b595db3c4d3e43fb76dcfa77f5224dbc->enter($__internal_e76bba9556433c917b0db5b9074ddf20b595db3c4d3e43fb76dcfa77f5224dbc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle::layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a513e6c9f7791ca31302d84f8bfaeddca42a1685b8e64753829753c57a3c3532->leave($__internal_a513e6c9f7791ca31302d84f8bfaeddca42a1685b8e64753829753c57a3c3532_prof);

        
        $__internal_e76bba9556433c917b0db5b9074ddf20b595db3c4d3e43fb76dcfa77f5224dbc->leave($__internal_e76bba9556433c917b0db5b9074ddf20b595db3c4d3e43fb76dcfa77f5224dbc_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_5896903d5f2e2005e75e67eb021d578cf55d2a0e5e4f1330b55468632e59effe = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5896903d5f2e2005e75e67eb021d578cf55d2a0e5e4f1330b55468632e59effe->enter($__internal_5896903d5f2e2005e75e67eb021d578cf55d2a0e5e4f1330b55468632e59effe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_0305acea73889a3e88c3fb6a0455b2b0d2c3b1feda27a12826661a5e8c16483c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0305acea73889a3e88c3fb6a0455b2b0d2c3b1feda27a12826661a5e8c16483c->enter($__internal_0305acea73889a3e88c3fb6a0455b2b0d2c3b1feda27a12826661a5e8c16483c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<div>
    ";
        // line 5
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 6
            echo "        ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("layout.logged_in_as", array("%username%" => $this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "user", array()), "username", array())), "FOSUserBundle"), "html", null, true);
            echo " |
        <a href=\"";
            // line 7
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_logout");
            echo "\">
            ";
            // line 8
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("layout.logout", array(), "FOSUserBundle"), "html", null, true);
            echo "
        </a>
    ";
        } else {
            // line 11
            echo "        <a href=\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_login");
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("layout.login", array(), "FOSUserBundle"), "html", null, true);
            echo "</a>
    ";
        }
        // line 13
        echo "</div>

";
        // line 15
        if ($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "hasPreviousSession", array())) {
            // line 16
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "session", array()), "flashbag", array()), "all", array(), "method"));
            foreach ($context['_seq'] as $context["type"] => $context["messages"]) {
                // line 17
                echo "        ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($context["messages"]);
                foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                    // line 18
                    echo "            <div class=\"flash-";
                    echo twig_escape_filter($this->env, $context["type"], "html", null, true);
                    echo "\">
                ";
                    // line 19
                    echo twig_escape_filter($this->env, $context["message"], "html", null, true);
                    echo "
            </div>
        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 22
                echo "    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['type'], $context['messages'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        // line 24
        echo "
<div>
    ";
        // line 26
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 28
        echo "</div>
";
        
        $__internal_0305acea73889a3e88c3fb6a0455b2b0d2c3b1feda27a12826661a5e8c16483c->leave($__internal_0305acea73889a3e88c3fb6a0455b2b0d2c3b1feda27a12826661a5e8c16483c_prof);

        
        $__internal_5896903d5f2e2005e75e67eb021d578cf55d2a0e5e4f1330b55468632e59effe->leave($__internal_5896903d5f2e2005e75e67eb021d578cf55d2a0e5e4f1330b55468632e59effe_prof);

    }

    // line 26
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_89424b2255fd61c431b9dc88a2c52481006dc109093dd727b813c674425f7177 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_89424b2255fd61c431b9dc88a2c52481006dc109093dd727b813c674425f7177->enter($__internal_89424b2255fd61c431b9dc88a2c52481006dc109093dd727b813c674425f7177_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_adace46a467ccc6aade30fd3f546d23bd363eb088aef22c81bae672e13e06822 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_adace46a467ccc6aade30fd3f546d23bd363eb088aef22c81bae672e13e06822->enter($__internal_adace46a467ccc6aade30fd3f546d23bd363eb088aef22c81bae672e13e06822_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 27
        echo "    ";
        
        $__internal_adace46a467ccc6aade30fd3f546d23bd363eb088aef22c81bae672e13e06822->leave($__internal_adace46a467ccc6aade30fd3f546d23bd363eb088aef22c81bae672e13e06822_prof);

        
        $__internal_89424b2255fd61c431b9dc88a2c52481006dc109093dd727b813c674425f7177->leave($__internal_89424b2255fd61c431b9dc88a2c52481006dc109093dd727b813c674425f7177_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  141 => 27,  132 => 26,  121 => 28,  119 => 26,  115 => 24,  108 => 22,  99 => 19,  94 => 18,  89 => 17,  84 => 16,  82 => 15,  78 => 13,  70 => 11,  64 => 8,  60 => 7,  55 => 6,  53 => 5,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '::base.html.twig' %}

{% block body %}
<div>
    {% if is_granted(\"IS_AUTHENTICATED_REMEMBERED\") %}
        {{ 'layout.logged_in_as'|trans({'%username%': app.user.username}, 'FOSUserBundle') }} |
        <a href=\"{{ path('fos_user_security_logout') }}\">
            {{ 'layout.logout'|trans({}, 'FOSUserBundle') }}
        </a>
    {% else %}
        <a href=\"{{ path('fos_user_security_login') }}\">{{ 'layout.login'|trans({}, 'FOSUserBundle') }}</a>
    {% endif %}
</div>

{% if app.request.hasPreviousSession %}
    {% for type, messages in app.session.flashbag.all() %}
        {% for message in messages %}
            <div class=\"flash-{{ type }}\">
                {{ message }}
            </div>
        {% endfor %}
    {% endfor %}
{% endif %}

<div>
    {% block fos_user_content %}
    {% endblock fos_user_content %}
</div>
{% endblock %}
", "FOSUserBundle::layout.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\app/Resources/FOSUserBundle/views/layout.html.twig");
    }
}
