<?php

/* TwigBundle:Exception:exception.js.twig */
class __TwigTemplate_57797d445e1ae7026835a8dbf36ce0a3d974979017a5af17014fbde7106a22c8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7c15b3ae739b1286fe1f6f13a9634e7b499cd4a259e1d81fbd0621e6959ea840 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7c15b3ae739b1286fe1f6f13a9634e7b499cd4a259e1d81fbd0621e6959ea840->enter($__internal_7c15b3ae739b1286fe1f6f13a9634e7b499cd4a259e1d81fbd0621e6959ea840_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.js.twig"));

        $__internal_b011dd724a746c05be0c49caec6ffcfd6687df3f03c937dd51afe471238459eb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b011dd724a746c05be0c49caec6ffcfd6687df3f03c937dd51afe471238459eb->enter($__internal_b011dd724a746c05be0c49caec6ffcfd6687df3f03c937dd51afe471238459eb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_include($this->env, $context, "@Twig/Exception/exception.txt.twig", array("exception" => ($context["exception"] ?? $this->getContext($context, "exception"))));
        echo "
*/
";
        
        $__internal_7c15b3ae739b1286fe1f6f13a9634e7b499cd4a259e1d81fbd0621e6959ea840->leave($__internal_7c15b3ae739b1286fe1f6f13a9634e7b499cd4a259e1d81fbd0621e6959ea840_prof);

        
        $__internal_b011dd724a746c05be0c49caec6ffcfd6687df3f03c937dd51afe471238459eb->leave($__internal_b011dd724a746c05be0c49caec6ffcfd6687df3f03c937dd51afe471238459eb_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ include('@Twig/Exception/exception.txt.twig', { exception: exception }) }}
*/
", "TwigBundle:Exception:exception.js.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle/Resources/views/Exception/exception.js.twig");
    }
}
