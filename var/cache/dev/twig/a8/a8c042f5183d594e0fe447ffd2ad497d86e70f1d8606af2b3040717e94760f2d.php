<?php

/* @Framework/Form/percent_widget.html.php */
class __TwigTemplate_6886cbef29787afff90251e9e84872c41856f254feaec4ade7c9a87ef4e92692 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_eb1197fe78b4e28b98e3641323e4da7e009eff6f5d4d6b901e89cf5686610f44 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_eb1197fe78b4e28b98e3641323e4da7e009eff6f5d4d6b901e89cf5686610f44->enter($__internal_eb1197fe78b4e28b98e3641323e4da7e009eff6f5d4d6b901e89cf5686610f44_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/percent_widget.html.php"));

        $__internal_36782389f5bb480f3f4fb97b07da9a9c39aabaff6c6b573487df7b89790ece37 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_36782389f5bb480f3f4fb97b07da9a9c39aabaff6c6b573487df7b89790ece37->enter($__internal_36782389f5bb480f3f4fb97b07da9a9c39aabaff6c6b573487df7b89790ece37_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/percent_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?> %
";
        
        $__internal_eb1197fe78b4e28b98e3641323e4da7e009eff6f5d4d6b901e89cf5686610f44->leave($__internal_eb1197fe78b4e28b98e3641323e4da7e009eff6f5d4d6b901e89cf5686610f44_prof);

        
        $__internal_36782389f5bb480f3f4fb97b07da9a9c39aabaff6c6b573487df7b89790ece37->leave($__internal_36782389f5bb480f3f4fb97b07da9a9c39aabaff6c6b573487df7b89790ece37_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/percent_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?> %
", "@Framework/Form/percent_widget.html.php", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\percent_widget.html.php");
    }
}
