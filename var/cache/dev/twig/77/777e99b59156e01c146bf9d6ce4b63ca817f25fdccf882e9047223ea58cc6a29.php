<?php

/* @FOSUser/Resetting/request.html.twig */
class __TwigTemplate_92ef3a66c04d7a213f539dfe6108cf2e87573b2c35c881611a8d33db4e48b3cc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Resetting/request.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c8572129dfa5fc93049ec368e3a3fe015b1ca7852e3153f982dd774adf7384bf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c8572129dfa5fc93049ec368e3a3fe015b1ca7852e3153f982dd774adf7384bf->enter($__internal_c8572129dfa5fc93049ec368e3a3fe015b1ca7852e3153f982dd774adf7384bf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Resetting/request.html.twig"));

        $__internal_5969945057d466f1fa23f44abdafba4bf1061ef0426d5376d1b1388709fc76e3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5969945057d466f1fa23f44abdafba4bf1061ef0426d5376d1b1388709fc76e3->enter($__internal_5969945057d466f1fa23f44abdafba4bf1061ef0426d5376d1b1388709fc76e3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Resetting/request.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c8572129dfa5fc93049ec368e3a3fe015b1ca7852e3153f982dd774adf7384bf->leave($__internal_c8572129dfa5fc93049ec368e3a3fe015b1ca7852e3153f982dd774adf7384bf_prof);

        
        $__internal_5969945057d466f1fa23f44abdafba4bf1061ef0426d5376d1b1388709fc76e3->leave($__internal_5969945057d466f1fa23f44abdafba4bf1061ef0426d5376d1b1388709fc76e3_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_08080450852d502f5fdde5993ba9290e54f9b8f867254ee3e3bfa0651c83084e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_08080450852d502f5fdde5993ba9290e54f9b8f867254ee3e3bfa0651c83084e->enter($__internal_08080450852d502f5fdde5993ba9290e54f9b8f867254ee3e3bfa0651c83084e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_30056fd7ff65fdf3d85ad818d165cd55eda68c06f53a43e7be17b041227452af = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_30056fd7ff65fdf3d85ad818d165cd55eda68c06f53a43e7be17b041227452af->enter($__internal_30056fd7ff65fdf3d85ad818d165cd55eda68c06f53a43e7be17b041227452af_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Resetting/request_content.html.twig", "@FOSUser/Resetting/request.html.twig", 4)->display($context);
        
        $__internal_30056fd7ff65fdf3d85ad818d165cd55eda68c06f53a43e7be17b041227452af->leave($__internal_30056fd7ff65fdf3d85ad818d165cd55eda68c06f53a43e7be17b041227452af_prof);

        
        $__internal_08080450852d502f5fdde5993ba9290e54f9b8f867254ee3e3bfa0651c83084e->leave($__internal_08080450852d502f5fdde5993ba9290e54f9b8f867254ee3e3bfa0651c83084e_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Resetting/request.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Resetting/request_content.html.twig\" %}
{% endblock fos_user_content %}
", "@FOSUser/Resetting/request.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\app\\Resources\\FOSUserBundle\\views\\Resetting\\request.html.twig");
    }
}
