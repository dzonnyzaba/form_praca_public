<?php

/* FOSUserBundle:Group:show.html.twig */
class __TwigTemplate_9e67c20553c2f629de5fe9eb5ed38b96a8fb84415f7d3109d2cbbaf5963d97c4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9a09e9de7d39bda34d53d1d6a442d9ef40ccb98d24595610604b3274236e5295 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9a09e9de7d39bda34d53d1d6a442d9ef40ccb98d24595610604b3274236e5295->enter($__internal_9a09e9de7d39bda34d53d1d6a442d9ef40ccb98d24595610604b3274236e5295_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show.html.twig"));

        $__internal_83509dfa179e7bb203fba7e8782803cd20cda3335b1e59b5b44e970a0479600a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_83509dfa179e7bb203fba7e8782803cd20cda3335b1e59b5b44e970a0479600a->enter($__internal_83509dfa179e7bb203fba7e8782803cd20cda3335b1e59b5b44e970a0479600a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9a09e9de7d39bda34d53d1d6a442d9ef40ccb98d24595610604b3274236e5295->leave($__internal_9a09e9de7d39bda34d53d1d6a442d9ef40ccb98d24595610604b3274236e5295_prof);

        
        $__internal_83509dfa179e7bb203fba7e8782803cd20cda3335b1e59b5b44e970a0479600a->leave($__internal_83509dfa179e7bb203fba7e8782803cd20cda3335b1e59b5b44e970a0479600a_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_f2e8eddfdcaf7e8da1d31ea83b639206e62dd60b4d47325847dacfd85a2044ee = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f2e8eddfdcaf7e8da1d31ea83b639206e62dd60b4d47325847dacfd85a2044ee->enter($__internal_f2e8eddfdcaf7e8da1d31ea83b639206e62dd60b4d47325847dacfd85a2044ee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_13868ebb3c28d826f9ff5de4ef8aab19538db4700e2b80452f88f1571da19867 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_13868ebb3c28d826f9ff5de4ef8aab19538db4700e2b80452f88f1571da19867->enter($__internal_13868ebb3c28d826f9ff5de4ef8aab19538db4700e2b80452f88f1571da19867_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/show_content.html.twig", "FOSUserBundle:Group:show.html.twig", 4)->display($context);
        
        $__internal_13868ebb3c28d826f9ff5de4ef8aab19538db4700e2b80452f88f1571da19867->leave($__internal_13868ebb3c28d826f9ff5de4ef8aab19538db4700e2b80452f88f1571da19867_prof);

        
        $__internal_f2e8eddfdcaf7e8da1d31ea83b639206e62dd60b4d47325847dacfd85a2044ee->leave($__internal_f2e8eddfdcaf7e8da1d31ea83b639206e62dd60b4d47325847dacfd85a2044ee_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/show_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Group:show.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\app/Resources/FOSUserBundle/views/Group/show.html.twig");
    }
}
