<?php

/* @FOSUser/Registration/register_content.html.twig */
class __TwigTemplate_dc1725cc0844e2cec9637c8a938e82f633abba5972d954deca5f2b49d1b95a0d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_58bb979a5d45df8a12fe1598e6171f0f01ca00f38c6c8ac78e61bb3a5b0b992d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_58bb979a5d45df8a12fe1598e6171f0f01ca00f38c6c8ac78e61bb3a5b0b992d->enter($__internal_58bb979a5d45df8a12fe1598e6171f0f01ca00f38c6c8ac78e61bb3a5b0b992d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/register_content.html.twig"));

        $__internal_83111c50d3b16d0ff2b4579e3587390992f5bd631e178410b7a72e3e7b454723 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_83111c50d3b16d0ff2b4579e3587390992f5bd631e178410b7a72e3e7b454723->enter($__internal_83111c50d3b16d0ff2b4579e3587390992f5bd631e178410b7a72e3e7b454723_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/register_content.html.twig"));

        // line 2
        echo "
";
        // line 3
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start', array("method" => "post", "action" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_registration_register"), "attr" => array("class" => "fos_user_registration_register")));
        echo "
        <div class=\"form-group\" id=\"fos_user_registration_form\">
         
        <div>
        ";
        // line 7
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "imie", array()), 'label', array("label_attr" => array("class" => "etykieta"), "label" => "Imię"));
        echo "
        ";
        // line 8
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "imie", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Imię", array(), "FOSUserBundle"))));
        echo "
        <span class=\"komunikat\"></span>
        ";
        // line 10
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "imie", array()), 'errors');
        echo "
        </div>
        <div>
        ";
        // line 13
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "nazwisko", array()), 'label', array("label_attr" => array("class" => "etykieta"), "label" => "Nazwisko"));
        echo "
        ";
        // line 14
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "nazwisko", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Nazwisko", array(), "FOSUserBundle"))));
        echo "
        <span class=\"komunikat\"></span>
        ";
        // line 16
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "nazwisko", array()), 'errors');
        echo "
        </div>
        <div>
        ";
        // line 19
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "username", array()), 'label', array("label_attr" => array("class" => "etykieta"), "label" => "Nazwa użytkownika"));
        echo "
        ";
        // line 20
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "username", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("form.username", array(), "FOSUserBundle"))));
        echo "
        <span class=\"komunikat\"></span>
        ";
        // line 22
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "username", array()), 'errors');
        echo "
        </div>
        <div>
        ";
        // line 25
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "plainPassword", array()), "first", array()), 'label', array("label_attr" => array("class" => "etykieta"), "label" => "Hasło"));
        echo "
        ";
        // line 26
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "plainPassword", array()), "first", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("form.password", array(), "FOSUserBundle"))));
        echo "
        <span class=\"komunikat\"></span>
        ";
        // line 28
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "plainPassword", array()), "first", array()), 'errors');
        echo "
        </div>
        <div>
        ";
        // line 31
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "plainPassword", array()), "second", array()), 'label', array("label_attr" => array("class" => "etykieta"), "label" => "Powtórz hasło"));
        echo "
        ";
        // line 32
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "plainPassword", array()), "second", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("form.password_confirmation", array(), "FOSUserBundle"))));
        echo "
        <span class=\"komunikat\"></span>
        ";
        // line 34
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "plainPassword", array()), "second", array()), 'errors');
        echo "
        </div>
        <div>
        ";
        // line 37
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "data_urodzenia", array()), 'label', array("label_attr" => array("class" => "etykieta"), "label" => "Data urodzenia"));
        echo "
        ";
        // line 38
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "data_urodzenia", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("data urodzenia", array(), "FOSUserBundle"))));
        echo "
        ";
        // line 39
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "data_urodzenia", array()), 'errors');
        echo "
        </div>
        <div>
        ";
        // line 42
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "email", array()), 'label', array("label_attr" => array("class" => "etykieta"), "label" => "Adres E-mail"));
        echo "
        ";
        // line 43
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "email", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("form.email", array(), "FOSUserBundle"))));
        echo "
        <span class=\"komunikat\"></span>
        ";
        // line 45
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "email", array()), 'errors');
        echo "
        </div>
        ";
        // line 47
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'rest');
        echo "

    </div>
    <div>
        <input type=\"submit\" id=\"form_save\" class=\"btn btn-primary\" value=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
    </div>
";
        // line 53
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
";
        
        $__internal_58bb979a5d45df8a12fe1598e6171f0f01ca00f38c6c8ac78e61bb3a5b0b992d->leave($__internal_58bb979a5d45df8a12fe1598e6171f0f01ca00f38c6c8ac78e61bb3a5b0b992d_prof);

        
        $__internal_83111c50d3b16d0ff2b4579e3587390992f5bd631e178410b7a72e3e7b454723->leave($__internal_83111c50d3b16d0ff2b4579e3587390992f5bd631e178410b7a72e3e7b454723_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Registration/register_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  150 => 53,  145 => 51,  138 => 47,  133 => 45,  128 => 43,  124 => 42,  118 => 39,  114 => 38,  110 => 37,  104 => 34,  99 => 32,  95 => 31,  89 => 28,  84 => 26,  80 => 25,  74 => 22,  69 => 20,  65 => 19,  59 => 16,  54 => 14,  50 => 13,  44 => 10,  39 => 8,  35 => 7,  28 => 3,  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}

{{ form_start(form, {'method': 'post', 'action': path('fos_user_registration_register'), 'attr': {'class': 'fos_user_registration_register'}}) }}
        <div class=\"form-group\" id=\"fos_user_registration_form\">
         
        <div>
        {{ form_label(form.imie, 'Imię', {'label_attr': {'class': 'etykieta'}}) }}
        {{ form_widget(form.imie, { 'attr': {'class': 'form-control', 'placeholder': 'Imię'|trans } }) }}
        <span class=\"komunikat\"></span>
        {{ form_errors(form.imie) }}
        </div>
        <div>
        {{ form_label(form.nazwisko, 'Nazwisko', {'label_attr': {'class': 'etykieta'}}) }}
        {{ form_widget(form.nazwisko, { 'attr': {'class': 'form-control', 'placeholder': 'Nazwisko'|trans } }) }}
        <span class=\"komunikat\"></span>
        {{ form_errors(form.nazwisko) }}
        </div>
        <div>
        {{ form_label(form.username, 'Nazwa użytkownika', {'label_attr': {'class': 'etykieta'}}) }}
        {{ form_widget(form.username, { 'attr': {'class': 'form-control', 'placeholder': 'form.username'|trans } }) }}
        <span class=\"komunikat\"></span>
        {{ form_errors(form.username) }}
        </div>
        <div>
        {{ form_label(form.plainPassword.first, 'Hasło', {'label_attr': {'class': 'etykieta'}}) }}
        {{ form_widget(form.plainPassword.first, { 'attr': {'class': 'form-control', 'placeholder': 'form.password'|trans } }) }}
        <span class=\"komunikat\"></span>
        {{ form_errors(form.plainPassword.first) }}
        </div>
        <div>
        {{ form_label(form.plainPassword.second, 'Powtórz hasło', {'label_attr': {'class': 'etykieta'}}) }}
        {{ form_widget(form.plainPassword.second, { 'attr': {'class': 'form-control', 'placeholder': 'form.password_confirmation'|trans } }) }}
        <span class=\"komunikat\"></span>
        {{ form_errors(form.plainPassword.second) }}
        </div>
        <div>
        {{ form_label(form.data_urodzenia, 'Data urodzenia', {'label_attr': {'class': 'etykieta'}}) }}
        {{ form_widget(form.data_urodzenia, { 'attr': {'class': 'form-control', 'placeholder': 'data urodzenia'|trans } }) }}
        {{ form_errors(form.data_urodzenia) }}
        </div>
        <div>
        {{ form_label(form.email, 'Adres E-mail', {'label_attr': {'class': 'etykieta'}}) }}
        {{ form_widget(form.email, { 'attr': {'class': 'form-control', 'placeholder': 'form.email'|trans } }) }}
        <span class=\"komunikat\"></span>
        {{ form_errors(form.email) }}
        </div>
        {{ form_rest(form) }}

    </div>
    <div>
        <input type=\"submit\" id=\"form_save\" class=\"btn btn-primary\" value=\"{{ 'registration.submit'|trans }}\" />
    </div>
{{ form_end(form) }}
", "@FOSUser/Registration/register_content.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\app\\Resources\\FOSUserBundle\\views\\Registration\\register_content.html.twig");
    }
}
