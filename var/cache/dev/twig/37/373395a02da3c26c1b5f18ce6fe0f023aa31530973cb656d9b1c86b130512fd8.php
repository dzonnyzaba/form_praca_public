<?php

/* @Twig/Exception/exception.atom.twig */
class __TwigTemplate_c1e0b93695c65a4be7240a0661dad4b1f7d3ad14833b5a373cc0d1d5785b6f25 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_177129a9f55e4c2959e2d1da1270832135be47b0fbb02d1eb465fe13fd17f375 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_177129a9f55e4c2959e2d1da1270832135be47b0fbb02d1eb465fe13fd17f375->enter($__internal_177129a9f55e4c2959e2d1da1270832135be47b0fbb02d1eb465fe13fd17f375_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.atom.twig"));

        $__internal_c56aec8a5e5cbae55b78aae9838fc689f35a1baf5685a695efa81c42eb5fb268 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c56aec8a5e5cbae55b78aae9838fc689f35a1baf5685a695efa81c42eb5fb268->enter($__internal_c56aec8a5e5cbae55b78aae9838fc689f35a1baf5685a695efa81c42eb5fb268_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.atom.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/Exception/exception.xml.twig", array("exception" => ($context["exception"] ?? $this->getContext($context, "exception"))));
        echo "
";
        
        $__internal_177129a9f55e4c2959e2d1da1270832135be47b0fbb02d1eb465fe13fd17f375->leave($__internal_177129a9f55e4c2959e2d1da1270832135be47b0fbb02d1eb465fe13fd17f375_prof);

        
        $__internal_c56aec8a5e5cbae55b78aae9838fc689f35a1baf5685a695efa81c42eb5fb268->leave($__internal_c56aec8a5e5cbae55b78aae9838fc689f35a1baf5685a695efa81c42eb5fb268_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/Exception/exception.xml.twig', { exception: exception }) }}
", "@Twig/Exception/exception.atom.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\exception.atom.twig");
    }
}
