<?php

/* ::base.html.twig */
class __TwigTemplate_93e2499d03c6402ea1e3906495568257ed0120721f7f5badb0e11600e82927c5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_87391a7342d6cadd35a3d3acd492f7745bd648af61c8194d1dcd2360c1e75af9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_87391a7342d6cadd35a3d3acd492f7745bd648af61c8194d1dcd2360c1e75af9->enter($__internal_87391a7342d6cadd35a3d3acd492f7745bd648af61c8194d1dcd2360c1e75af9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        $__internal_38e1cc208613cbaf710462242127d6096cc9d61b7f1e380fbfe02f6f49588f1e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_38e1cc208613cbaf710462242127d6096cc9d61b7f1e380fbfe02f6f49588f1e->enter($__internal_38e1cc208613cbaf710462242127d6096cc9d61b7f1e380fbfe02f6f49588f1e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 9
        echo "        <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">
        
        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
        <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\">
        
    </head>
    <body>
    <div>
    ";
        // line 17
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 18
            echo "        ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("layout.logged_in_as", array("%username%" => $this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "user", array()), "username", array())), "FOSUserBundle"), "html", null, true);
            echo " |
        <a href=\"";
            // line 19
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_logout");
            echo "\">
            ";
            // line 20
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("layout.logout", array(), "FOSUserBundle"), "html", null, true);
            echo "
        </a>
    ";
        } else {
            // line 23
            echo "        <a href=\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_login");
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("layout.login", array(), "FOSUserBundle"), "html", null, true);
            echo "</a><br>
        <a href=\"";
            // line 24
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_registration_register");
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("layout.register", array(), "FOSUserBundle"), "html", null, true);
            echo "</a>
    ";
        }
        // line 26
        echo "    </div>
        ";
        // line 27
        $this->displayBlock('body', $context, $blocks);
        // line 28
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 29
        echo "    </body>
</html>
";
        
        $__internal_87391a7342d6cadd35a3d3acd492f7745bd648af61c8194d1dcd2360c1e75af9->leave($__internal_87391a7342d6cadd35a3d3acd492f7745bd648af61c8194d1dcd2360c1e75af9_prof);

        
        $__internal_38e1cc208613cbaf710462242127d6096cc9d61b7f1e380fbfe02f6f49588f1e->leave($__internal_38e1cc208613cbaf710462242127d6096cc9d61b7f1e380fbfe02f6f49588f1e_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_9aeaf49209db3c3d6a36dcdb32b94a1abc3fe8bcf8026224a6c414f55f0c1b99 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9aeaf49209db3c3d6a36dcdb32b94a1abc3fe8bcf8026224a6c414f55f0c1b99->enter($__internal_9aeaf49209db3c3d6a36dcdb32b94a1abc3fe8bcf8026224a6c414f55f0c1b99_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_299f88a8f4215cd617629e0bc87a1eb9e25bf1b471f87e915bc76fe5a6251228 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_299f88a8f4215cd617629e0bc87a1eb9e25bf1b471f87e915bc76fe5a6251228->enter($__internal_299f88a8f4215cd617629e0bc87a1eb9e25bf1b471f87e915bc76fe5a6251228_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_299f88a8f4215cd617629e0bc87a1eb9e25bf1b471f87e915bc76fe5a6251228->leave($__internal_299f88a8f4215cd617629e0bc87a1eb9e25bf1b471f87e915bc76fe5a6251228_prof);

        
        $__internal_9aeaf49209db3c3d6a36dcdb32b94a1abc3fe8bcf8026224a6c414f55f0c1b99->leave($__internal_9aeaf49209db3c3d6a36dcdb32b94a1abc3fe8bcf8026224a6c414f55f0c1b99_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_7db30f1831941599778554551a789db71bea93bfce9cf7c3b7c6c9bf7399ac5a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7db30f1831941599778554551a789db71bea93bfce9cf7c3b7c6c9bf7399ac5a->enter($__internal_7db30f1831941599778554551a789db71bea93bfce9cf7c3b7c6c9bf7399ac5a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_322a003d87180f188135b760429bdb2c4146ad1ef2e9cbeb179d1c88b91ff5c5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_322a003d87180f188135b760429bdb2c4146ad1ef2e9cbeb179d1c88b91ff5c5->enter($__internal_322a003d87180f188135b760429bdb2c4146ad1ef2e9cbeb179d1c88b91ff5c5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 7
        echo "
        ";
        
        $__internal_322a003d87180f188135b760429bdb2c4146ad1ef2e9cbeb179d1c88b91ff5c5->leave($__internal_322a003d87180f188135b760429bdb2c4146ad1ef2e9cbeb179d1c88b91ff5c5_prof);

        
        $__internal_7db30f1831941599778554551a789db71bea93bfce9cf7c3b7c6c9bf7399ac5a->leave($__internal_7db30f1831941599778554551a789db71bea93bfce9cf7c3b7c6c9bf7399ac5a_prof);

    }

    // line 27
    public function block_body($context, array $blocks = array())
    {
        $__internal_4fd2a4cf55f89a50e3b9b33589a03f09907a6d6315f1aa03e4038e1803e462f1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4fd2a4cf55f89a50e3b9b33589a03f09907a6d6315f1aa03e4038e1803e462f1->enter($__internal_4fd2a4cf55f89a50e3b9b33589a03f09907a6d6315f1aa03e4038e1803e462f1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_14e3e31e1bc41af0a7d15aacfede94e3f75969b09b16fca2a2675bbee87e5db3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_14e3e31e1bc41af0a7d15aacfede94e3f75969b09b16fca2a2675bbee87e5db3->enter($__internal_14e3e31e1bc41af0a7d15aacfede94e3f75969b09b16fca2a2675bbee87e5db3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_14e3e31e1bc41af0a7d15aacfede94e3f75969b09b16fca2a2675bbee87e5db3->leave($__internal_14e3e31e1bc41af0a7d15aacfede94e3f75969b09b16fca2a2675bbee87e5db3_prof);

        
        $__internal_4fd2a4cf55f89a50e3b9b33589a03f09907a6d6315f1aa03e4038e1803e462f1->leave($__internal_4fd2a4cf55f89a50e3b9b33589a03f09907a6d6315f1aa03e4038e1803e462f1_prof);

    }

    // line 28
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_ab3ef918345f1dd82fd2f92a0d0a6e4d3162b3a8a09aaae108112e942727ddb3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ab3ef918345f1dd82fd2f92a0d0a6e4d3162b3a8a09aaae108112e942727ddb3->enter($__internal_ab3ef918345f1dd82fd2f92a0d0a6e4d3162b3a8a09aaae108112e942727ddb3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_3ff979c817cf041960a8493daa76777ecf51e0ad8ce70a2e5319a8ad7225cae9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3ff979c817cf041960a8493daa76777ecf51e0ad8ce70a2e5319a8ad7225cae9->enter($__internal_3ff979c817cf041960a8493daa76777ecf51e0ad8ce70a2e5319a8ad7225cae9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_3ff979c817cf041960a8493daa76777ecf51e0ad8ce70a2e5319a8ad7225cae9->leave($__internal_3ff979c817cf041960a8493daa76777ecf51e0ad8ce70a2e5319a8ad7225cae9_prof);

        
        $__internal_ab3ef918345f1dd82fd2f92a0d0a6e4d3162b3a8a09aaae108112e942727ddb3->leave($__internal_ab3ef918345f1dd82fd2f92a0d0a6e4d3162b3a8a09aaae108112e942727ddb3_prof);

    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  163 => 28,  146 => 27,  135 => 7,  126 => 6,  108 => 5,  96 => 29,  93 => 28,  91 => 27,  88 => 26,  81 => 24,  74 => 23,  68 => 20,  64 => 19,  59 => 18,  57 => 17,  49 => 12,  45 => 11,  41 => 9,  39 => 6,  35 => 5,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        {% block stylesheets %}

        {% endblock %}
        <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">
        
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
        <link rel=\"stylesheet\" type=\"text/css\" href=\"{{ asset('css/style.css') }}\">
        
    </head>
    <body>
    <div>
    {% if is_granted(\"IS_AUTHENTICATED_REMEMBERED\") %}
        {{ 'layout.logged_in_as'|trans({'%username%': app.user.username}, 'FOSUserBundle') }} |
        <a href=\"{{ path('fos_user_security_logout') }}\">
            {{ 'layout.logout'|trans({}, 'FOSUserBundle') }}
        </a>
    {% else %}
        <a href=\"{{ path('fos_user_security_login') }}\">{{ 'layout.login'|trans({}, 'FOSUserBundle') }}</a><br>
        <a href=\"{{ path('fos_user_registration_register') }}\">{{ 'layout.register'|trans({}, 'FOSUserBundle') }}</a>
    {% endif %}
    </div>
        {% block body %}{% endblock %}
        {% block javascripts %}{% endblock %}
    </body>
</html>
", "::base.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\app\\Resources\\views\\base.html.twig");
    }
}
