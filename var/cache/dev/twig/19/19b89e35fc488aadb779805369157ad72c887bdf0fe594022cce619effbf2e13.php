<?php

/* TwigBundle:Exception:error.txt.twig */
class __TwigTemplate_bee41b5fcd1a34ba2ad29b481cff02b28acd654d51a18d067d1adc47889e574c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5238855819f2f014cb9200e88ef105a6a2d9e336e99dd5936c2eed53ec5abb09 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5238855819f2f014cb9200e88ef105a6a2d9e336e99dd5936c2eed53ec5abb09->enter($__internal_5238855819f2f014cb9200e88ef105a6a2d9e336e99dd5936c2eed53ec5abb09_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.txt.twig"));

        $__internal_baa9e4f7ddd4293fde21a17c499cf1cfd2bb0c2220543963a77d8c6d5f64869f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_baa9e4f7ddd4293fde21a17c499cf1cfd2bb0c2220543963a77d8c6d5f64869f->enter($__internal_baa9e4f7ddd4293fde21a17c499cf1cfd2bb0c2220543963a77d8c6d5f64869f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.txt.twig"));

        // line 1
        echo "Oops! An Error Occurred
=======================

The server returned a \"";
        // line 4
        echo ($context["status_code"] ?? $this->getContext($context, "status_code"));
        echo " ";
        echo ($context["status_text"] ?? $this->getContext($context, "status_text"));
        echo "\".

Something is broken. Please let us know what you were doing when this error occurred.
We will fix it as soon as possible. Sorry for any inconvenience caused.
";
        
        $__internal_5238855819f2f014cb9200e88ef105a6a2d9e336e99dd5936c2eed53ec5abb09->leave($__internal_5238855819f2f014cb9200e88ef105a6a2d9e336e99dd5936c2eed53ec5abb09_prof);

        
        $__internal_baa9e4f7ddd4293fde21a17c499cf1cfd2bb0c2220543963a77d8c6d5f64869f->leave($__internal_baa9e4f7ddd4293fde21a17c499cf1cfd2bb0c2220543963a77d8c6d5f64869f_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.txt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 4,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("Oops! An Error Occurred
=======================

The server returned a \"{{ status_code }} {{ status_text }}\".

Something is broken. Please let us know what you were doing when this error occurred.
We will fix it as soon as possible. Sorry for any inconvenience caused.
", "TwigBundle:Exception:error.txt.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle/Resources/views/Exception/error.txt.twig");
    }
}
