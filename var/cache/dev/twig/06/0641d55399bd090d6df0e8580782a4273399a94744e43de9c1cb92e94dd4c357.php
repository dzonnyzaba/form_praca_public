<?php

/* @Framework/Form/form_enctype.html.php */
class __TwigTemplate_3d5e943cc8f8fed9d099f1bc78b4627410a2d022699f6b6f147ae1065ccb6f2f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e6b3760d4a989aa43944b10a6b25866793b4bc3a16ef3f4b4ba9cbb2a0c21e78 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e6b3760d4a989aa43944b10a6b25866793b4bc3a16ef3f4b4ba9cbb2a0c21e78->enter($__internal_e6b3760d4a989aa43944b10a6b25866793b4bc3a16ef3f4b4ba9cbb2a0c21e78_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        $__internal_4174cf22939889e8b9d0e170e93e83816b5351025f32ad5abc2f7a62822a7aed = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4174cf22939889e8b9d0e170e93e83816b5351025f32ad5abc2f7a62822a7aed->enter($__internal_4174cf22939889e8b9d0e170e93e83816b5351025f32ad5abc2f7a62822a7aed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        // line 1
        echo "<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
";
        
        $__internal_e6b3760d4a989aa43944b10a6b25866793b4bc3a16ef3f4b4ba9cbb2a0c21e78->leave($__internal_e6b3760d4a989aa43944b10a6b25866793b4bc3a16ef3f4b4ba9cbb2a0c21e78_prof);

        
        $__internal_4174cf22939889e8b9d0e170e93e83816b5351025f32ad5abc2f7a62822a7aed->leave($__internal_4174cf22939889e8b9d0e170e93e83816b5351025f32ad5abc2f7a62822a7aed_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_enctype.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
", "@Framework/Form/form_enctype.html.php", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\form_enctype.html.php");
    }
}
