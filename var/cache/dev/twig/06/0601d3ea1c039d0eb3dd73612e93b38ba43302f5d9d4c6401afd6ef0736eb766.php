<?php

/* @Framework/FormTable/button_row.html.php */
class __TwigTemplate_88f11b1e857296202947b18f268b2cc539470e8047fba297f0948838878d304b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d83e68660a9e77c78ba9c46c84b8fb89d2ddcdc983d2276024f112dbabf22da0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d83e68660a9e77c78ba9c46c84b8fb89d2ddcdc983d2276024f112dbabf22da0->enter($__internal_d83e68660a9e77c78ba9c46c84b8fb89d2ddcdc983d2276024f112dbabf22da0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/button_row.html.php"));

        $__internal_effde5a66df8892e584ffd2c151d3ddf6cbd63ecca76142242af22bbf1f9f65f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_effde5a66df8892e584ffd2c151d3ddf6cbd63ecca76142242af22bbf1f9f65f->enter($__internal_effde5a66df8892e584ffd2c151d3ddf6cbd63ecca76142242af22bbf1f9f65f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/button_row.html.php"));

        // line 1
        echo "<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_d83e68660a9e77c78ba9c46c84b8fb89d2ddcdc983d2276024f112dbabf22da0->leave($__internal_d83e68660a9e77c78ba9c46c84b8fb89d2ddcdc983d2276024f112dbabf22da0_prof);

        
        $__internal_effde5a66df8892e584ffd2c151d3ddf6cbd63ecca76142242af22bbf1f9f65f->leave($__internal_effde5a66df8892e584ffd2c151d3ddf6cbd63ecca76142242af22bbf1f9f65f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
", "@Framework/FormTable/button_row.html.php", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\FormTable\\button_row.html.php");
    }
}
