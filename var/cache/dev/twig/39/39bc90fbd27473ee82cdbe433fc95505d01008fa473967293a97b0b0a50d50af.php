<?php

/* @FOSUser/Group/list.html.twig */
class __TwigTemplate_0fafe7d7e232de4605d49e4efc0f6a4ae9cf74ad303a9e6f0ecb0f941466959a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Group/list.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b56e475bc36f1bd14a5de0f9c88040c6e858ca5c43a79d2a7aadf155cfb08dbf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b56e475bc36f1bd14a5de0f9c88040c6e858ca5c43a79d2a7aadf155cfb08dbf->enter($__internal_b56e475bc36f1bd14a5de0f9c88040c6e858ca5c43a79d2a7aadf155cfb08dbf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Group/list.html.twig"));

        $__internal_a39f166facc0e493d09ed3e96e713f80ab35186b9fff92b7f16fa81af82bf323 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a39f166facc0e493d09ed3e96e713f80ab35186b9fff92b7f16fa81af82bf323->enter($__internal_a39f166facc0e493d09ed3e96e713f80ab35186b9fff92b7f16fa81af82bf323_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Group/list.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b56e475bc36f1bd14a5de0f9c88040c6e858ca5c43a79d2a7aadf155cfb08dbf->leave($__internal_b56e475bc36f1bd14a5de0f9c88040c6e858ca5c43a79d2a7aadf155cfb08dbf_prof);

        
        $__internal_a39f166facc0e493d09ed3e96e713f80ab35186b9fff92b7f16fa81af82bf323->leave($__internal_a39f166facc0e493d09ed3e96e713f80ab35186b9fff92b7f16fa81af82bf323_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_17245f5fd611a7f2c58b27e2758113339e41ec6e3f63c81b5601e793470d0f41 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_17245f5fd611a7f2c58b27e2758113339e41ec6e3f63c81b5601e793470d0f41->enter($__internal_17245f5fd611a7f2c58b27e2758113339e41ec6e3f63c81b5601e793470d0f41_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_1dee5a19907ffe451f6bfcbf88f31da0eba41a5a4c10a815ff847ae2cc188db5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1dee5a19907ffe451f6bfcbf88f31da0eba41a5a4c10a815ff847ae2cc188db5->enter($__internal_1dee5a19907ffe451f6bfcbf88f31da0eba41a5a4c10a815ff847ae2cc188db5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/list_content.html.twig", "@FOSUser/Group/list.html.twig", 4)->display($context);
        
        $__internal_1dee5a19907ffe451f6bfcbf88f31da0eba41a5a4c10a815ff847ae2cc188db5->leave($__internal_1dee5a19907ffe451f6bfcbf88f31da0eba41a5a4c10a815ff847ae2cc188db5_prof);

        
        $__internal_17245f5fd611a7f2c58b27e2758113339e41ec6e3f63c81b5601e793470d0f41->leave($__internal_17245f5fd611a7f2c58b27e2758113339e41ec6e3f63c81b5601e793470d0f41_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Group/list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/list_content.html.twig\" %}
{% endblock fos_user_content %}
", "@FOSUser/Group/list.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\app\\Resources\\FOSUserBundle\\views\\Group\\list.html.twig");
    }
}
