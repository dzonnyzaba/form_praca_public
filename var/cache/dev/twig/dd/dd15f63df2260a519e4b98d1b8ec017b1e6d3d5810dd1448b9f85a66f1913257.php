<?php

/* @FOSUser/Group/edit.html.twig */
class __TwigTemplate_0035f0a4f7fbe9dc77fbe7f7c2891f9f7d5f77e310f837f43f35c5b04cdf1d99 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Group/edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2e3cfe96ec180c8f51bac59cab585eecb67871c3909cab64c7ae8e3fc67bdb66 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2e3cfe96ec180c8f51bac59cab585eecb67871c3909cab64c7ae8e3fc67bdb66->enter($__internal_2e3cfe96ec180c8f51bac59cab585eecb67871c3909cab64c7ae8e3fc67bdb66_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Group/edit.html.twig"));

        $__internal_ebef8f8393130fe3238d5a56c24a088f8a1da481ea69e5101e7d65b758a8e5f4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ebef8f8393130fe3238d5a56c24a088f8a1da481ea69e5101e7d65b758a8e5f4->enter($__internal_ebef8f8393130fe3238d5a56c24a088f8a1da481ea69e5101e7d65b758a8e5f4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Group/edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2e3cfe96ec180c8f51bac59cab585eecb67871c3909cab64c7ae8e3fc67bdb66->leave($__internal_2e3cfe96ec180c8f51bac59cab585eecb67871c3909cab64c7ae8e3fc67bdb66_prof);

        
        $__internal_ebef8f8393130fe3238d5a56c24a088f8a1da481ea69e5101e7d65b758a8e5f4->leave($__internal_ebef8f8393130fe3238d5a56c24a088f8a1da481ea69e5101e7d65b758a8e5f4_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_e2f9c5ee9e0558a7b6738b505e84317f743856ab9666347d0c593c59a021cd72 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e2f9c5ee9e0558a7b6738b505e84317f743856ab9666347d0c593c59a021cd72->enter($__internal_e2f9c5ee9e0558a7b6738b505e84317f743856ab9666347d0c593c59a021cd72_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_36c55bd5b94100f4fab25884122e61f0ea194cfdd68537ae90b86cbc2dad7d9a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_36c55bd5b94100f4fab25884122e61f0ea194cfdd68537ae90b86cbc2dad7d9a->enter($__internal_36c55bd5b94100f4fab25884122e61f0ea194cfdd68537ae90b86cbc2dad7d9a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/edit_content.html.twig", "@FOSUser/Group/edit.html.twig", 4)->display($context);
        
        $__internal_36c55bd5b94100f4fab25884122e61f0ea194cfdd68537ae90b86cbc2dad7d9a->leave($__internal_36c55bd5b94100f4fab25884122e61f0ea194cfdd68537ae90b86cbc2dad7d9a_prof);

        
        $__internal_e2f9c5ee9e0558a7b6738b505e84317f743856ab9666347d0c593c59a021cd72->leave($__internal_e2f9c5ee9e0558a7b6738b505e84317f743856ab9666347d0c593c59a021cd72_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Group/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/edit_content.html.twig\" %}
{% endblock fos_user_content %}
", "@FOSUser/Group/edit.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\app\\Resources\\FOSUserBundle\\views\\Group\\edit.html.twig");
    }
}
