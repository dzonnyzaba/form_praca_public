<?php

/* @Framework/Form/email_widget.html.php */
class __TwigTemplate_f794bac01edc92477b475ede6c7c781008181ff6bbc724cf8c87ab652aee584c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_34d4d47a2de5ce854f5653bc1f18efccb6b69fdc54e1016d385d95c3376f9343 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_34d4d47a2de5ce854f5653bc1f18efccb6b69fdc54e1016d385d95c3376f9343->enter($__internal_34d4d47a2de5ce854f5653bc1f18efccb6b69fdc54e1016d385d95c3376f9343_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        $__internal_d0eb8025e874c9a1a1199f13b9d338cf0e3f731e7ef488b9078e72fed4cccb8b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d0eb8025e874c9a1a1199f13b9d338cf0e3f731e7ef488b9078e72fed4cccb8b->enter($__internal_d0eb8025e874c9a1a1199f13b9d338cf0e3f731e7ef488b9078e72fed4cccb8b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
";
        
        $__internal_34d4d47a2de5ce854f5653bc1f18efccb6b69fdc54e1016d385d95c3376f9343->leave($__internal_34d4d47a2de5ce854f5653bc1f18efccb6b69fdc54e1016d385d95c3376f9343_prof);

        
        $__internal_d0eb8025e874c9a1a1199f13b9d338cf0e3f731e7ef488b9078e72fed4cccb8b->leave($__internal_d0eb8025e874c9a1a1199f13b9d338cf0e3f731e7ef488b9078e72fed4cccb8b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/email_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
", "@Framework/Form/email_widget.html.php", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\email_widget.html.php");
    }
}
