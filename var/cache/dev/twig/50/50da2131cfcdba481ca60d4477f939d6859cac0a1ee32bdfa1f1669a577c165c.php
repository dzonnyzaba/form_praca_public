<?php

/* @FOSUser/Profile/edit.html.twig */
class __TwigTemplate_99b9e4703e43c91c969313a817e7b2bc364f5fb80750ff4ab02f10a2ebd0a9c7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Profile/edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4702ac6246e67e7f3f1d1a0e66da9ef5733b1079a5f054f56014a1e76860c7fe = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4702ac6246e67e7f3f1d1a0e66da9ef5733b1079a5f054f56014a1e76860c7fe->enter($__internal_4702ac6246e67e7f3f1d1a0e66da9ef5733b1079a5f054f56014a1e76860c7fe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Profile/edit.html.twig"));

        $__internal_0f0878445eed8c4aedd9a135b2439a1edfcce6659e4e18a219432114eca90c2f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0f0878445eed8c4aedd9a135b2439a1edfcce6659e4e18a219432114eca90c2f->enter($__internal_0f0878445eed8c4aedd9a135b2439a1edfcce6659e4e18a219432114eca90c2f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Profile/edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4702ac6246e67e7f3f1d1a0e66da9ef5733b1079a5f054f56014a1e76860c7fe->leave($__internal_4702ac6246e67e7f3f1d1a0e66da9ef5733b1079a5f054f56014a1e76860c7fe_prof);

        
        $__internal_0f0878445eed8c4aedd9a135b2439a1edfcce6659e4e18a219432114eca90c2f->leave($__internal_0f0878445eed8c4aedd9a135b2439a1edfcce6659e4e18a219432114eca90c2f_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_2b15842d6e749590a7c13b56952d88341a08ff428b4da6530f8ba892956cca50 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2b15842d6e749590a7c13b56952d88341a08ff428b4da6530f8ba892956cca50->enter($__internal_2b15842d6e749590a7c13b56952d88341a08ff428b4da6530f8ba892956cca50_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_22fabbeb82f46b731cec5e41e4ac3bd9aa0019de285de0d678735c98b35d6f10 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_22fabbeb82f46b731cec5e41e4ac3bd9aa0019de285de0d678735c98b35d6f10->enter($__internal_22fabbeb82f46b731cec5e41e4ac3bd9aa0019de285de0d678735c98b35d6f10_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Profile/edit_content.html.twig", "@FOSUser/Profile/edit.html.twig", 4)->display($context);
        
        $__internal_22fabbeb82f46b731cec5e41e4ac3bd9aa0019de285de0d678735c98b35d6f10->leave($__internal_22fabbeb82f46b731cec5e41e4ac3bd9aa0019de285de0d678735c98b35d6f10_prof);

        
        $__internal_2b15842d6e749590a7c13b56952d88341a08ff428b4da6530f8ba892956cca50->leave($__internal_2b15842d6e749590a7c13b56952d88341a08ff428b4da6530f8ba892956cca50_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Profile/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Profile/edit_content.html.twig\" %}
{% endblock fos_user_content %}
", "@FOSUser/Profile/edit.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\app\\Resources\\FOSUserBundle\\views\\Profile\\edit.html.twig");
    }
}
