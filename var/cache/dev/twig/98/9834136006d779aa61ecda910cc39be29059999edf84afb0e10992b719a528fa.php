<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_09886c37796d81d574b2448cf2a22ea32da8dad546b69f22c376ce19a869ce97 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_749f67ff23030f7fb9e7411ffccdcf49ab15f58469ee2ea8135e139b23a36a17 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_749f67ff23030f7fb9e7411ffccdcf49ab15f58469ee2ea8135e139b23a36a17->enter($__internal_749f67ff23030f7fb9e7411ffccdcf49ab15f58469ee2ea8135e139b23a36a17_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $__internal_f8c8e7c6e5cbe23c3b6dd5362a0397f4d5530f98a73e7ff2c61cb7f200865cdd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f8c8e7c6e5cbe23c3b6dd5362a0397f4d5530f98a73e7ff2c61cb7f200865cdd->enter($__internal_f8c8e7c6e5cbe23c3b6dd5362a0397f4d5530f98a73e7ff2c61cb7f200865cdd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_749f67ff23030f7fb9e7411ffccdcf49ab15f58469ee2ea8135e139b23a36a17->leave($__internal_749f67ff23030f7fb9e7411ffccdcf49ab15f58469ee2ea8135e139b23a36a17_prof);

        
        $__internal_f8c8e7c6e5cbe23c3b6dd5362a0397f4d5530f98a73e7ff2c61cb7f200865cdd->leave($__internal_f8c8e7c6e5cbe23c3b6dd5362a0397f4d5530f98a73e7ff2c61cb7f200865cdd_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_9ea0eb9f1d22836f794264ff703f75ebb171677e21c5d98f34ea97dc8b058824 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9ea0eb9f1d22836f794264ff703f75ebb171677e21c5d98f34ea97dc8b058824->enter($__internal_9ea0eb9f1d22836f794264ff703f75ebb171677e21c5d98f34ea97dc8b058824_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_0178b4533cc6a05bfc260212fb40519af0ed6727d39d32ae48eccf675bdb45ed = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0178b4533cc6a05bfc260212fb40519af0ed6727d39d32ae48eccf675bdb45ed->enter($__internal_0178b4533cc6a05bfc260212fb40519af0ed6727d39d32ae48eccf675bdb45ed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_0178b4533cc6a05bfc260212fb40519af0ed6727d39d32ae48eccf675bdb45ed->leave($__internal_0178b4533cc6a05bfc260212fb40519af0ed6727d39d32ae48eccf675bdb45ed_prof);

        
        $__internal_9ea0eb9f1d22836f794264ff703f75ebb171677e21c5d98f34ea97dc8b058824->leave($__internal_9ea0eb9f1d22836f794264ff703f75ebb171677e21c5d98f34ea97dc8b058824_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_d71f3015a516088a7b1fd0ec7851fc3ee7837eb2a9d6e5d50c419e94f5c1face = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d71f3015a516088a7b1fd0ec7851fc3ee7837eb2a9d6e5d50c419e94f5c1face->enter($__internal_d71f3015a516088a7b1fd0ec7851fc3ee7837eb2a9d6e5d50c419e94f5c1face_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_5fa71d8b41beb08ad037753aedbd75b04346ced426f086c5756b44d58f9fe031 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5fa71d8b41beb08ad037753aedbd75b04346ced426f086c5756b44d58f9fe031->enter($__internal_5fa71d8b41beb08ad037753aedbd75b04346ced426f086c5756b44d58f9fe031_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_5fa71d8b41beb08ad037753aedbd75b04346ced426f086c5756b44d58f9fe031->leave($__internal_5fa71d8b41beb08ad037753aedbd75b04346ced426f086c5756b44d58f9fe031_prof);

        
        $__internal_d71f3015a516088a7b1fd0ec7851fc3ee7837eb2a9d6e5d50c419e94f5c1face->leave($__internal_d71f3015a516088a7b1fd0ec7851fc3ee7837eb2a9d6e5d50c419e94f5c1face_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_67e389007b19644d2afd5b7536848d3dad809ada7a91e3f0bb3f736a59e31e32 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_67e389007b19644d2afd5b7536848d3dad809ada7a91e3f0bb3f736a59e31e32->enter($__internal_67e389007b19644d2afd5b7536848d3dad809ada7a91e3f0bb3f736a59e31e32_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_b792bf09e503b8f23cf3d55ae5055c17b5e1d1624edae6f83fd8e497a0c75f80 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b792bf09e503b8f23cf3d55ae5055c17b5e1d1624edae6f83fd8e497a0c75f80->enter($__internal_b792bf09e503b8f23cf3d55ae5055c17b5e1d1624edae6f83fd8e497a0c75f80_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_b792bf09e503b8f23cf3d55ae5055c17b5e1d1624edae6f83fd8e497a0c75f80->leave($__internal_b792bf09e503b8f23cf3d55ae5055c17b5e1d1624edae6f83fd8e497a0c75f80_prof);

        
        $__internal_67e389007b19644d2afd5b7536848d3dad809ada7a91e3f0bb3f736a59e31e32->leave($__internal_67e389007b19644d2afd5b7536848d3dad809ada7a91e3f0bb3f736a59e31e32_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\router.html.twig");
    }
}
