<?php

/* @FOSUser/Resetting/reset.html.twig */
class __TwigTemplate_5758c5bd860fccc6802e71649cc34c9ca224c2825c6a6c61d5c6d9d8177431fe extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Resetting/reset.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_aaf5957e021a03991815279088dcea99e408ffa6240dcd5a8200e511c1947268 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_aaf5957e021a03991815279088dcea99e408ffa6240dcd5a8200e511c1947268->enter($__internal_aaf5957e021a03991815279088dcea99e408ffa6240dcd5a8200e511c1947268_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Resetting/reset.html.twig"));

        $__internal_5da12607a71db9e83fc5ff61488e87dd2533c6db0d8bee847b7f513330230b47 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5da12607a71db9e83fc5ff61488e87dd2533c6db0d8bee847b7f513330230b47->enter($__internal_5da12607a71db9e83fc5ff61488e87dd2533c6db0d8bee847b7f513330230b47_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Resetting/reset.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_aaf5957e021a03991815279088dcea99e408ffa6240dcd5a8200e511c1947268->leave($__internal_aaf5957e021a03991815279088dcea99e408ffa6240dcd5a8200e511c1947268_prof);

        
        $__internal_5da12607a71db9e83fc5ff61488e87dd2533c6db0d8bee847b7f513330230b47->leave($__internal_5da12607a71db9e83fc5ff61488e87dd2533c6db0d8bee847b7f513330230b47_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_2f29aad6180c5a4bf30c196a43d1367f085757d7c3259850e9302e18492aa4bc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2f29aad6180c5a4bf30c196a43d1367f085757d7c3259850e9302e18492aa4bc->enter($__internal_2f29aad6180c5a4bf30c196a43d1367f085757d7c3259850e9302e18492aa4bc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_0fdc91015d5541c7d72ecf75da069d6d57eacf08a4b88e9b9e65098f66ac104c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0fdc91015d5541c7d72ecf75da069d6d57eacf08a4b88e9b9e65098f66ac104c->enter($__internal_0fdc91015d5541c7d72ecf75da069d6d57eacf08a4b88e9b9e65098f66ac104c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Resetting/reset_content.html.twig", "@FOSUser/Resetting/reset.html.twig", 4)->display($context);
        
        $__internal_0fdc91015d5541c7d72ecf75da069d6d57eacf08a4b88e9b9e65098f66ac104c->leave($__internal_0fdc91015d5541c7d72ecf75da069d6d57eacf08a4b88e9b9e65098f66ac104c_prof);

        
        $__internal_2f29aad6180c5a4bf30c196a43d1367f085757d7c3259850e9302e18492aa4bc->leave($__internal_2f29aad6180c5a4bf30c196a43d1367f085757d7c3259850e9302e18492aa4bc_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Resetting/reset.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Resetting/reset_content.html.twig\" %}
{% endblock fos_user_content %}
", "@FOSUser/Resetting/reset.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\app\\Resources\\FOSUserBundle\\views\\Resetting\\reset.html.twig");
    }
}
