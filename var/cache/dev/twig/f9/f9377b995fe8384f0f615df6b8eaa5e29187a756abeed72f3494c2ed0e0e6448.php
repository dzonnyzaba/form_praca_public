<?php

/* @Twig/Exception/exception.rdf.twig */
class __TwigTemplate_a578328802c4b404def3be14a096784cbfc79376723443982e53544f6003a9cc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_223e6780779abdb9e9aeb78b3ff7f7a0da4a0f1c87083120dd882242557fde4d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_223e6780779abdb9e9aeb78b3ff7f7a0da4a0f1c87083120dd882242557fde4d->enter($__internal_223e6780779abdb9e9aeb78b3ff7f7a0da4a0f1c87083120dd882242557fde4d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.rdf.twig"));

        $__internal_75c5c16166d8440c1bf2abfac093430fc694934d8a6316b1f11cc1ecf44d3532 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_75c5c16166d8440c1bf2abfac093430fc694934d8a6316b1f11cc1ecf44d3532->enter($__internal_75c5c16166d8440c1bf2abfac093430fc694934d8a6316b1f11cc1ecf44d3532_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.rdf.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/Exception/exception.xml.twig", array("exception" => ($context["exception"] ?? $this->getContext($context, "exception"))));
        echo "
";
        
        $__internal_223e6780779abdb9e9aeb78b3ff7f7a0da4a0f1c87083120dd882242557fde4d->leave($__internal_223e6780779abdb9e9aeb78b3ff7f7a0da4a0f1c87083120dd882242557fde4d_prof);

        
        $__internal_75c5c16166d8440c1bf2abfac093430fc694934d8a6316b1f11cc1ecf44d3532->leave($__internal_75c5c16166d8440c1bf2abfac093430fc694934d8a6316b1f11cc1ecf44d3532_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/Exception/exception.xml.twig', { exception: exception }) }}
", "@Twig/Exception/exception.rdf.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\exception.rdf.twig");
    }
}
