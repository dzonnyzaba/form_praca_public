<?php

/* @Framework/Form/container_attributes.html.php */
class __TwigTemplate_018a74265dafed84df1e4c8cbdb2309779e71d80b055a85859bf080568e3c50c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_84231913161e53c1bb7361d715ebf8dc8ea39bdc901dac430fa6327a77a7e957 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_84231913161e53c1bb7361d715ebf8dc8ea39bdc901dac430fa6327a77a7e957->enter($__internal_84231913161e53c1bb7361d715ebf8dc8ea39bdc901dac430fa6327a77a7e957_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        $__internal_49b01a3e2f24e42a90c5550f2c991952c0f5c212e46366228244691ffe421d12 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_49b01a3e2f24e42a90c5550f2c991952c0f5c212e46366228244691ffe421d12->enter($__internal_49b01a3e2f24e42a90c5550f2c991952c0f5c212e46366228244691ffe421d12_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
";
        
        $__internal_84231913161e53c1bb7361d715ebf8dc8ea39bdc901dac430fa6327a77a7e957->leave($__internal_84231913161e53c1bb7361d715ebf8dc8ea39bdc901dac430fa6327a77a7e957_prof);

        
        $__internal_49b01a3e2f24e42a90c5550f2c991952c0f5c212e46366228244691ffe421d12->leave($__internal_49b01a3e2f24e42a90c5550f2c991952c0f5c212e46366228244691ffe421d12_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/container_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
", "@Framework/Form/container_attributes.html.php", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\container_attributes.html.php");
    }
}
