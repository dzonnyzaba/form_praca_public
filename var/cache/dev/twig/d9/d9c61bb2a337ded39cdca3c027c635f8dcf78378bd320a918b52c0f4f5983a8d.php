<?php

/* @Framework/Form/form_row.html.php */
class __TwigTemplate_2e228051c03e77a1b82ee3800c4acabc8fe02ca2c249317a943d16608030b111 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_67e369d0ecad0b72b6090f10a0368a034c7271481b41832945c1a03495bc94c8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_67e369d0ecad0b72b6090f10a0368a034c7271481b41832945c1a03495bc94c8->enter($__internal_67e369d0ecad0b72b6090f10a0368a034c7271481b41832945c1a03495bc94c8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_row.html.php"));

        $__internal_1117e7377539e0a675640a840668c9bd8a769146bf091b114b58c4ee06260d6a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1117e7377539e0a675640a840668c9bd8a769146bf091b114b58c4ee06260d6a->enter($__internal_1117e7377539e0a675640a840668c9bd8a769146bf091b114b58c4ee06260d6a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_67e369d0ecad0b72b6090f10a0368a034c7271481b41832945c1a03495bc94c8->leave($__internal_67e369d0ecad0b72b6090f10a0368a034c7271481b41832945c1a03495bc94c8_prof);

        
        $__internal_1117e7377539e0a675640a840668c9bd8a769146bf091b114b58c4ee06260d6a->leave($__internal_1117e7377539e0a675640a840668c9bd8a769146bf091b114b58c4ee06260d6a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
", "@Framework/Form/form_row.html.php", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\form_row.html.php");
    }
}
