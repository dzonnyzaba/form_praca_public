<?php

/* @Framework/Form/form_rows.html.php */
class __TwigTemplate_9f6224f034cc85d529136e173070785fa907214a81106382bce53c67852f8111 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_eb46a016078387a68fef55d49cfc5b8772a0edf8ff4cc7aecd4f3d7818a7c518 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_eb46a016078387a68fef55d49cfc5b8772a0edf8ff4cc7aecd4f3d7818a7c518->enter($__internal_eb46a016078387a68fef55d49cfc5b8772a0edf8ff4cc7aecd4f3d7818a7c518_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rows.html.php"));

        $__internal_7020e438124f7539dbe999e610964fed9b16348b6c693289f59c2d64357da569 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7020e438124f7539dbe999e610964fed9b16348b6c693289f59c2d64357da569->enter($__internal_7020e438124f7539dbe999e610964fed9b16348b6c693289f59c2d64357da569_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rows.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
";
        
        $__internal_eb46a016078387a68fef55d49cfc5b8772a0edf8ff4cc7aecd4f3d7818a7c518->leave($__internal_eb46a016078387a68fef55d49cfc5b8772a0edf8ff4cc7aecd4f3d7818a7c518_prof);

        
        $__internal_7020e438124f7539dbe999e610964fed9b16348b6c693289f59c2d64357da569->leave($__internal_7020e438124f7539dbe999e610964fed9b16348b6c693289f59c2d64357da569_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rows.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
", "@Framework/Form/form_rows.html.php", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\form_rows.html.php");
    }
}
