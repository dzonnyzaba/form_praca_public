<?php

/* FOSUserBundle:Resetting:email.txt.twig */
class __TwigTemplate_2153b48e9b29951af96fd6129eddda53b8a9ce08b35bc6332096dad49a0c1ad5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_600de2b45674013263bafa86bf7f859f3dc05c625488d03ce35a14abf1f7d56c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_600de2b45674013263bafa86bf7f859f3dc05c625488d03ce35a14abf1f7d56c->enter($__internal_600de2b45674013263bafa86bf7f859f3dc05c625488d03ce35a14abf1f7d56c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:email.txt.twig"));

        $__internal_7732a52e3f9702fd351fc84a42b4a13922e4c1ff8134d9691d0ee86eb42b457e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7732a52e3f9702fd351fc84a42b4a13922e4c1ff8134d9691d0ee86eb42b457e->enter($__internal_7732a52e3f9702fd351fc84a42b4a13922e4c1ff8134d9691d0ee86eb42b457e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        echo "
";
        // line 8
        $this->displayBlock('body_text', $context, $blocks);
        // line 13
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_600de2b45674013263bafa86bf7f859f3dc05c625488d03ce35a14abf1f7d56c->leave($__internal_600de2b45674013263bafa86bf7f859f3dc05c625488d03ce35a14abf1f7d56c_prof);

        
        $__internal_7732a52e3f9702fd351fc84a42b4a13922e4c1ff8134d9691d0ee86eb42b457e->leave($__internal_7732a52e3f9702fd351fc84a42b4a13922e4c1ff8134d9691d0ee86eb42b457e_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_45afaef466863612170c929c561183209ab0c72fb09f434578706edcb4c33974 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_45afaef466863612170c929c561183209ab0c72fb09f434578706edcb4c33974->enter($__internal_45afaef466863612170c929c561183209ab0c72fb09f434578706edcb4c33974_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        $__internal_ff2fcacf3846fcf762e4aa771948f93f0d6fd017015f323e0c4a881f72ad4abb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ff2fcacf3846fcf762e4aa771948f93f0d6fd017015f323e0c4a881f72ad4abb->enter($__internal_ff2fcacf3846fcf762e4aa771948f93f0d6fd017015f323e0c4a881f72ad4abb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.email.subject", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array())), "FOSUserBundle");
        
        $__internal_ff2fcacf3846fcf762e4aa771948f93f0d6fd017015f323e0c4a881f72ad4abb->leave($__internal_ff2fcacf3846fcf762e4aa771948f93f0d6fd017015f323e0c4a881f72ad4abb_prof);

        
        $__internal_45afaef466863612170c929c561183209ab0c72fb09f434578706edcb4c33974->leave($__internal_45afaef466863612170c929c561183209ab0c72fb09f434578706edcb4c33974_prof);

    }

    // line 8
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_9718c94d74bea95f1834e4db644bf573ac5518c0aa8f4a9a0837b6add2a71708 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9718c94d74bea95f1834e4db644bf573ac5518c0aa8f4a9a0837b6add2a71708->enter($__internal_9718c94d74bea95f1834e4db644bf573ac5518c0aa8f4a9a0837b6add2a71708_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        $__internal_6406ad13403a804ad9a2de14af5596c582586eb81b3de669ac50ebbd6d2915dd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6406ad13403a804ad9a2de14af5596c582586eb81b3de669ac50ebbd6d2915dd->enter($__internal_6406ad13403a804ad9a2de14af5596c582586eb81b3de669ac50ebbd6d2915dd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.email.message", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => ($context["confirmationUrl"] ?? $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        echo "
";
        
        $__internal_6406ad13403a804ad9a2de14af5596c582586eb81b3de669ac50ebbd6d2915dd->leave($__internal_6406ad13403a804ad9a2de14af5596c582586eb81b3de669ac50ebbd6d2915dd_prof);

        
        $__internal_9718c94d74bea95f1834e4db644bf573ac5518c0aa8f4a9a0837b6add2a71708->leave($__internal_9718c94d74bea95f1834e4db644bf573ac5518c0aa8f4a9a0837b6add2a71708_prof);

    }

    // line 13
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_9cfe3eb6bbcdd92b87e330f47939f09fb466c174f016a08882d07954a8309beb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9cfe3eb6bbcdd92b87e330f47939f09fb466c174f016a08882d07954a8309beb->enter($__internal_9cfe3eb6bbcdd92b87e330f47939f09fb466c174f016a08882d07954a8309beb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        $__internal_2cc6dabb5f8ec96f5a9dbd58fd07531925f48fd17256b0b1deecb380378f0c81 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2cc6dabb5f8ec96f5a9dbd58fd07531925f48fd17256b0b1deecb380378f0c81->enter($__internal_2cc6dabb5f8ec96f5a9dbd58fd07531925f48fd17256b0b1deecb380378f0c81_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_2cc6dabb5f8ec96f5a9dbd58fd07531925f48fd17256b0b1deecb380378f0c81->leave($__internal_2cc6dabb5f8ec96f5a9dbd58fd07531925f48fd17256b0b1deecb380378f0c81_prof);

        
        $__internal_9cfe3eb6bbcdd92b87e330f47939f09fb466c174f016a08882d07954a8309beb->leave($__internal_9cfe3eb6bbcdd92b87e330f47939f09fb466c174f016a08882d07954a8309beb_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  85 => 13,  73 => 10,  64 => 8,  54 => 4,  45 => 2,  35 => 13,  33 => 8,  30 => 7,  28 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}
{% block subject %}
{%- autoescape false -%}
{{ 'resetting.email.subject'|trans({'%username%': user.username}) }}
{%- endautoescape -%}
{% endblock %}

{% block body_text %}
{% autoescape false %}
{{ 'resetting.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{% endautoescape %}
{% endblock %}
{% block body_html %}{% endblock %}
", "FOSUserBundle:Resetting:email.txt.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\app/Resources/FOSUserBundle/views/Resetting/email.txt.twig");
    }
}
