<?php

/* @Framework/Form/hidden_row.html.php */
class __TwigTemplate_cab5a67889bd3a426ce6a939f99ae35d294977358932f5d4e077defbbb342f18 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7971d2b56d6a3f8705bab3070e2d6f692726f0db35ae950e939c1b8f8a535b0e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7971d2b56d6a3f8705bab3070e2d6f692726f0db35ae950e939c1b8f8a535b0e->enter($__internal_7971d2b56d6a3f8705bab3070e2d6f692726f0db35ae950e939c1b8f8a535b0e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_row.html.php"));

        $__internal_e40ec9798cbce5fede10069d7c0417dfad7d09d515edb7a5978ab809ac321bd4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e40ec9798cbce5fede10069d7c0417dfad7d09d515edb7a5978ab809ac321bd4->enter($__internal_e40ec9798cbce5fede10069d7c0417dfad7d09d515edb7a5978ab809ac321bd4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->widget(\$form) ?>
";
        
        $__internal_7971d2b56d6a3f8705bab3070e2d6f692726f0db35ae950e939c1b8f8a535b0e->leave($__internal_7971d2b56d6a3f8705bab3070e2d6f692726f0db35ae950e939c1b8f8a535b0e_prof);

        
        $__internal_e40ec9798cbce5fede10069d7c0417dfad7d09d515edb7a5978ab809ac321bd4->leave($__internal_e40ec9798cbce5fede10069d7c0417dfad7d09d515edb7a5978ab809ac321bd4_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->widget(\$form) ?>
", "@Framework/Form/hidden_row.html.php", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\hidden_row.html.php");
    }
}
