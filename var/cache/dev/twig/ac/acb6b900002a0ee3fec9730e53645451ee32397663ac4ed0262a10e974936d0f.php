<?php

/* @Framework/Form/money_widget.html.php */
class __TwigTemplate_318816ca07eee82e6969e1299dd2c3bc57c76f2be658898ca129199b24b1f7a6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5320940b4cee6b3d43fd4b3acff50096652eebbbbdb87107a325673a8722f282 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5320940b4cee6b3d43fd4b3acff50096652eebbbbdb87107a325673a8722f282->enter($__internal_5320940b4cee6b3d43fd4b3acff50096652eebbbbdb87107a325673a8722f282_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/money_widget.html.php"));

        $__internal_b3adf78bda30b17546bf20a1f0d154e6acc74dbeee05aea5d4dbbc83ab9b1c23 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b3adf78bda30b17546bf20a1f0d154e6acc74dbeee05aea5d4dbbc83ab9b1c23->enter($__internal_b3adf78bda30b17546bf20a1f0d154e6acc74dbeee05aea5d4dbbc83ab9b1c23_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/money_widget.html.php"));

        // line 1
        echo "<?php echo str_replace('";
        echo twig_escape_filter($this->env, ($context["widget"] ?? $this->getContext($context, "widget")), "html", null, true);
        echo "', \$view['form']->block(\$form, 'form_widget_simple'), \$money_pattern) ?>
";
        
        $__internal_5320940b4cee6b3d43fd4b3acff50096652eebbbbdb87107a325673a8722f282->leave($__internal_5320940b4cee6b3d43fd4b3acff50096652eebbbbdb87107a325673a8722f282_prof);

        
        $__internal_b3adf78bda30b17546bf20a1f0d154e6acc74dbeee05aea5d4dbbc83ab9b1c23->leave($__internal_b3adf78bda30b17546bf20a1f0d154e6acc74dbeee05aea5d4dbbc83ab9b1c23_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/money_widget.html.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo str_replace('{{ widget }}', \$view['form']->block(\$form, 'form_widget_simple'), \$money_pattern) ?>
", "@Framework/Form/money_widget.html.php", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\money_widget.html.php");
    }
}
