<?php

/* TwigBundle::layout.html.twig */
class __TwigTemplate_26ce063fe9be192b956753213ac612bba2e41dc305aa519593807e55e9632f55 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a87e8d0ad4914d438ff48000a62b8076e76c66d6f211c6b220c18dee314098c6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a87e8d0ad4914d438ff48000a62b8076e76c66d6f211c6b220c18dee314098c6->enter($__internal_a87e8d0ad4914d438ff48000a62b8076e76c66d6f211c6b220c18dee314098c6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle::layout.html.twig"));

        $__internal_3bd64f0dbc4cf74a6ddbba7d502c6e16cfd1ac3dd4a91ce376777b16d7e5ca3d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3bd64f0dbc4cf74a6ddbba7d502c6e16cfd1ac3dd4a91ce376777b16d7e5ca3d->enter($__internal_3bd64f0dbc4cf74a6ddbba7d502c6e16cfd1ac3dd4a91ce376777b16d7e5ca3d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle::layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 8
        echo twig_include($this->env, $context, "@Twig/images/favicon.png.base64");
        echo "\">
        <style>";
        // line 9
        echo twig_include($this->env, $context, "@Twig/exception.css.twig");
        echo "</style>
        ";
        // line 10
        $this->displayBlock('head', $context, $blocks);
        // line 11
        echo "    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">";
        // line 15
        echo twig_include($this->env, $context, "@Twig/images/symfony-logo.svg");
        echo " Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">";
        // line 19
        echo twig_include($this->env, $context, "@Twig/images/icon-book.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">";
        // line 26
        echo twig_include($this->env, $context, "@Twig/images/icon-support.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        ";
        // line 33
        $this->displayBlock('body', $context, $blocks);
        // line 34
        echo "        ";
        echo twig_include($this->env, $context, "@Twig/base_js.html.twig");
        echo "
    </body>
</html>
";
        
        $__internal_a87e8d0ad4914d438ff48000a62b8076e76c66d6f211c6b220c18dee314098c6->leave($__internal_a87e8d0ad4914d438ff48000a62b8076e76c66d6f211c6b220c18dee314098c6_prof);

        
        $__internal_3bd64f0dbc4cf74a6ddbba7d502c6e16cfd1ac3dd4a91ce376777b16d7e5ca3d->leave($__internal_3bd64f0dbc4cf74a6ddbba7d502c6e16cfd1ac3dd4a91ce376777b16d7e5ca3d_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_cf554549cf16430f4778a430b3c2f0cf3de7e631dd7db652802a0723508e2315 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cf554549cf16430f4778a430b3c2f0cf3de7e631dd7db652802a0723508e2315->enter($__internal_cf554549cf16430f4778a430b3c2f0cf3de7e631dd7db652802a0723508e2315_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_23f8bc4b4c8d4c4d6164be1334a8ff461db849396819b6682be033608e4026b6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_23f8bc4b4c8d4c4d6164be1334a8ff461db849396819b6682be033608e4026b6->enter($__internal_23f8bc4b4c8d4c4d6164be1334a8ff461db849396819b6682be033608e4026b6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_23f8bc4b4c8d4c4d6164be1334a8ff461db849396819b6682be033608e4026b6->leave($__internal_23f8bc4b4c8d4c4d6164be1334a8ff461db849396819b6682be033608e4026b6_prof);

        
        $__internal_cf554549cf16430f4778a430b3c2f0cf3de7e631dd7db652802a0723508e2315->leave($__internal_cf554549cf16430f4778a430b3c2f0cf3de7e631dd7db652802a0723508e2315_prof);

    }

    // line 10
    public function block_head($context, array $blocks = array())
    {
        $__internal_a404c6bf5cb1a7704495febd3bacb715ec3d4c286cb0b6a2c469a060208b2ee6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a404c6bf5cb1a7704495febd3bacb715ec3d4c286cb0b6a2c469a060208b2ee6->enter($__internal_a404c6bf5cb1a7704495febd3bacb715ec3d4c286cb0b6a2c469a060208b2ee6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_3a6fc33c44809c672ba40e24b835981354b795cf12c58b63de3dc771f466bc5f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3a6fc33c44809c672ba40e24b835981354b795cf12c58b63de3dc771f466bc5f->enter($__internal_3a6fc33c44809c672ba40e24b835981354b795cf12c58b63de3dc771f466bc5f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        
        $__internal_3a6fc33c44809c672ba40e24b835981354b795cf12c58b63de3dc771f466bc5f->leave($__internal_3a6fc33c44809c672ba40e24b835981354b795cf12c58b63de3dc771f466bc5f_prof);

        
        $__internal_a404c6bf5cb1a7704495febd3bacb715ec3d4c286cb0b6a2c469a060208b2ee6->leave($__internal_a404c6bf5cb1a7704495febd3bacb715ec3d4c286cb0b6a2c469a060208b2ee6_prof);

    }

    // line 33
    public function block_body($context, array $blocks = array())
    {
        $__internal_d0d56ea93f375546d20fff314c5df80c51611d081dd3e6b45510e8e5ef3b3728 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d0d56ea93f375546d20fff314c5df80c51611d081dd3e6b45510e8e5ef3b3728->enter($__internal_d0d56ea93f375546d20fff314c5df80c51611d081dd3e6b45510e8e5ef3b3728_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_1ba381c8486fbea0a868cd34cf52c8c9ac97c63fe943d5a0d8eb69180273b913 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1ba381c8486fbea0a868cd34cf52c8c9ac97c63fe943d5a0d8eb69180273b913->enter($__internal_1ba381c8486fbea0a868cd34cf52c8c9ac97c63fe943d5a0d8eb69180273b913_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_1ba381c8486fbea0a868cd34cf52c8c9ac97c63fe943d5a0d8eb69180273b913->leave($__internal_1ba381c8486fbea0a868cd34cf52c8c9ac97c63fe943d5a0d8eb69180273b913_prof);

        
        $__internal_d0d56ea93f375546d20fff314c5df80c51611d081dd3e6b45510e8e5ef3b3728->leave($__internal_d0d56ea93f375546d20fff314c5df80c51611d081dd3e6b45510e8e5ef3b3728_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 33,  120 => 10,  103 => 7,  88 => 34,  86 => 33,  76 => 26,  66 => 19,  59 => 15,  53 => 11,  51 => 10,  47 => 9,  43 => 8,  39 => 7,  33 => 4,  28 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"{{ _charset }}\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>{% block title %}{% endblock %}</title>
        <link rel=\"icon\" type=\"image/png\" href=\"{{ include('@Twig/images/favicon.png.base64') }}\">
        <style>{{ include('@Twig/exception.css.twig') }}</style>
        {% block head %}{% endblock %}
    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">{{ include('@Twig/images/symfony-logo.svg') }} Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-book.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-support.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        {% block body %}{% endblock %}
        {{ include('@Twig/base_js.html.twig') }}
    </body>
</html>
", "TwigBundle::layout.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle/Resources/views/layout.html.twig");
    }
}
