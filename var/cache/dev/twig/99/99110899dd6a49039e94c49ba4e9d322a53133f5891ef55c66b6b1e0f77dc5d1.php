<?php

/* WebProfilerBundle:Profiler:ajax_layout.html.twig */
class __TwigTemplate_0447a5b26578c982271d3959bc31fe1d29de4baea38b3240c66ed77af9056adc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_15154d1fa6aac02a170410d4e99e0d7b207d0582a9094e5df35a39da4213f592 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_15154d1fa6aac02a170410d4e99e0d7b207d0582a9094e5df35a39da4213f592->enter($__internal_15154d1fa6aac02a170410d4e99e0d7b207d0582a9094e5df35a39da4213f592_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        $__internal_15636b6307cfe272fa7f53e5798beb18a885cbd135ec73f0966353f5711f50a8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_15636b6307cfe272fa7f53e5798beb18a885cbd135ec73f0966353f5711f50a8->enter($__internal_15636b6307cfe272fa7f53e5798beb18a885cbd135ec73f0966353f5711f50a8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_15154d1fa6aac02a170410d4e99e0d7b207d0582a9094e5df35a39da4213f592->leave($__internal_15154d1fa6aac02a170410d4e99e0d7b207d0582a9094e5df35a39da4213f592_prof);

        
        $__internal_15636b6307cfe272fa7f53e5798beb18a885cbd135ec73f0966353f5711f50a8->leave($__internal_15636b6307cfe272fa7f53e5798beb18a885cbd135ec73f0966353f5711f50a8_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_60f636bb6e27516dea756a9ae26a69511583d456ee57e3e07631cfef7bdaf58f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_60f636bb6e27516dea756a9ae26a69511583d456ee57e3e07631cfef7bdaf58f->enter($__internal_60f636bb6e27516dea756a9ae26a69511583d456ee57e3e07631cfef7bdaf58f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_7bc2673c05547b5f532ed143ba98f862785ec0092e094f6927ad09fc8f7bbe0e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7bc2673c05547b5f532ed143ba98f862785ec0092e094f6927ad09fc8f7bbe0e->enter($__internal_7bc2673c05547b5f532ed143ba98f862785ec0092e094f6927ad09fc8f7bbe0e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_7bc2673c05547b5f532ed143ba98f862785ec0092e094f6927ad09fc8f7bbe0e->leave($__internal_7bc2673c05547b5f532ed143ba98f862785ec0092e094f6927ad09fc8f7bbe0e_prof);

        
        $__internal_60f636bb6e27516dea756a9ae26a69511583d456ee57e3e07631cfef7bdaf58f->leave($__internal_60f636bb6e27516dea756a9ae26a69511583d456ee57e3e07631cfef7bdaf58f_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% block panel '' %}
", "WebProfilerBundle:Profiler:ajax_layout.html.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle/Resources/views/Profiler/ajax_layout.html.twig");
    }
}
