<?php

/* @Framework/Form/reset_widget.html.php */
class __TwigTemplate_7b1d2c5b49c16962cad41052fea34be7b55498728da807bb8af97ae522d4da5d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7a79eb46875c73a44f9ed6b2866b37298740cc0dae40e56c81f49258b92db0d3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7a79eb46875c73a44f9ed6b2866b37298740cc0dae40e56c81f49258b92db0d3->enter($__internal_7a79eb46875c73a44f9ed6b2866b37298740cc0dae40e56c81f49258b92db0d3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        $__internal_27fdfb261707bced5ce89e8bb78bb62e1d7ef759c7f7727321faff7606589acf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_27fdfb261707bced5ce89e8bb78bb62e1d7ef759c7f7727321faff7606589acf->enter($__internal_27fdfb261707bced5ce89e8bb78bb62e1d7ef759c7f7727321faff7606589acf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'reset')) ?>
";
        
        $__internal_7a79eb46875c73a44f9ed6b2866b37298740cc0dae40e56c81f49258b92db0d3->leave($__internal_7a79eb46875c73a44f9ed6b2866b37298740cc0dae40e56c81f49258b92db0d3_prof);

        
        $__internal_27fdfb261707bced5ce89e8bb78bb62e1d7ef759c7f7727321faff7606589acf->leave($__internal_27fdfb261707bced5ce89e8bb78bb62e1d7ef759c7f7727321faff7606589acf_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/reset_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'reset')) ?>
", "@Framework/Form/reset_widget.html.php", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\reset_widget.html.php");
    }
}
