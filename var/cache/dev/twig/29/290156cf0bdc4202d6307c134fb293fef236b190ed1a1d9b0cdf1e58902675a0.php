<?php

/* @Twig/Exception/error.txt.twig */
class __TwigTemplate_13d6abe4db688889006e99abf49b08cb6f14902caa17f0ae6661938f4a855ade extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d81b178ce99288ccd8d39439d00cba7ae7fa5b1e859904f3fb4dc3298cfe754a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d81b178ce99288ccd8d39439d00cba7ae7fa5b1e859904f3fb4dc3298cfe754a->enter($__internal_d81b178ce99288ccd8d39439d00cba7ae7fa5b1e859904f3fb4dc3298cfe754a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.txt.twig"));

        $__internal_95b788a1446f99c1641ae6958e1424ab1f07c1413a6d83894d96c23d0fc66051 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_95b788a1446f99c1641ae6958e1424ab1f07c1413a6d83894d96c23d0fc66051->enter($__internal_95b788a1446f99c1641ae6958e1424ab1f07c1413a6d83894d96c23d0fc66051_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.txt.twig"));

        // line 1
        echo "Oops! An Error Occurred
=======================

The server returned a \"";
        // line 4
        echo ($context["status_code"] ?? $this->getContext($context, "status_code"));
        echo " ";
        echo ($context["status_text"] ?? $this->getContext($context, "status_text"));
        echo "\".

Something is broken. Please let us know what you were doing when this error occurred.
We will fix it as soon as possible. Sorry for any inconvenience caused.
";
        
        $__internal_d81b178ce99288ccd8d39439d00cba7ae7fa5b1e859904f3fb4dc3298cfe754a->leave($__internal_d81b178ce99288ccd8d39439d00cba7ae7fa5b1e859904f3fb4dc3298cfe754a_prof);

        
        $__internal_95b788a1446f99c1641ae6958e1424ab1f07c1413a6d83894d96c23d0fc66051->leave($__internal_95b788a1446f99c1641ae6958e1424ab1f07c1413a6d83894d96c23d0fc66051_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.txt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 4,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("Oops! An Error Occurred
=======================

The server returned a \"{{ status_code }} {{ status_text }}\".

Something is broken. Please let us know what you were doing when this error occurred.
We will fix it as soon as possible. Sorry for any inconvenience caused.
", "@Twig/Exception/error.txt.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\error.txt.twig");
    }
}
