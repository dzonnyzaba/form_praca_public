<?php

/* @Twig/Exception/error.js.twig */
class __TwigTemplate_776cd615b8759ed72317ae086378acc489295d2714037f9e3fc9a0e7d4ba460c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4266c8aaaee3d40b4b5aa21ed70192fbe9f5f7f0d5eb37bf5f142a6608b1de4e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4266c8aaaee3d40b4b5aa21ed70192fbe9f5f7f0d5eb37bf5f142a6608b1de4e->enter($__internal_4266c8aaaee3d40b4b5aa21ed70192fbe9f5f7f0d5eb37bf5f142a6608b1de4e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.js.twig"));

        $__internal_76e1a59b325dcf87edddbd4c3123b4604ba02b5945d9b244ab93f1f5cf945da8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_76e1a59b325dcf87edddbd4c3123b4604ba02b5945d9b244ab93f1f5cf945da8->enter($__internal_76e1a59b325dcf87edddbd4c3123b4604ba02b5945d9b244ab93f1f5cf945da8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "js", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "js", null, true);
        echo "

*/
";
        
        $__internal_4266c8aaaee3d40b4b5aa21ed70192fbe9f5f7f0d5eb37bf5f142a6608b1de4e->leave($__internal_4266c8aaaee3d40b4b5aa21ed70192fbe9f5f7f0d5eb37bf5f142a6608b1de4e_prof);

        
        $__internal_76e1a59b325dcf87edddbd4c3123b4604ba02b5945d9b244ab93f1f5cf945da8->leave($__internal_76e1a59b325dcf87edddbd4c3123b4604ba02b5945d9b244ab93f1f5cf945da8_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ status_code }} {{ status_text }}

*/
", "@Twig/Exception/error.js.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\error.js.twig");
    }
}
