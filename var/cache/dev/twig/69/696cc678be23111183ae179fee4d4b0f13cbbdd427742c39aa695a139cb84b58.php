<?php

/* TwigBundle:Exception:exception.rdf.twig */
class __TwigTemplate_bc002d44418fb8c0d656c09dc978a8bb939752af74fb4075f3ebda5498ebbe2f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cd590cc08f391cb4a52cf9a79d94247808b7bd0fb766c3569de2edc2a8b16e01 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cd590cc08f391cb4a52cf9a79d94247808b7bd0fb766c3569de2edc2a8b16e01->enter($__internal_cd590cc08f391cb4a52cf9a79d94247808b7bd0fb766c3569de2edc2a8b16e01_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.rdf.twig"));

        $__internal_6d5bcd5e20dc97996913b6b3b7b10e2864f6ac6f1c0ffc957e24bf2cf92901a5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6d5bcd5e20dc97996913b6b3b7b10e2864f6ac6f1c0ffc957e24bf2cf92901a5->enter($__internal_6d5bcd5e20dc97996913b6b3b7b10e2864f6ac6f1c0ffc957e24bf2cf92901a5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.rdf.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/Exception/exception.xml.twig", array("exception" => ($context["exception"] ?? $this->getContext($context, "exception"))));
        echo "
";
        
        $__internal_cd590cc08f391cb4a52cf9a79d94247808b7bd0fb766c3569de2edc2a8b16e01->leave($__internal_cd590cc08f391cb4a52cf9a79d94247808b7bd0fb766c3569de2edc2a8b16e01_prof);

        
        $__internal_6d5bcd5e20dc97996913b6b3b7b10e2864f6ac6f1c0ffc957e24bf2cf92901a5->leave($__internal_6d5bcd5e20dc97996913b6b3b7b10e2864f6ac6f1c0ffc957e24bf2cf92901a5_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/Exception/exception.xml.twig', { exception: exception }) }}
", "TwigBundle:Exception:exception.rdf.twig", "C:\\xampp\\htdocs\\symfony_gajda\\formularz_praca\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle/Resources/views/Exception/exception.rdf.twig");
    }
}
